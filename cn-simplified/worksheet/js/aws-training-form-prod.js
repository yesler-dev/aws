(function($) {
    console.log('form script');

    MktoForms2.loadForm("//app-sj23.marketo.com", "112-TZM-766", 13879, function(form) {
        // form.vals({
        //     "FirstName":"Lisa",
        //     "LastName":"Simpson",
        //     "Email":"lisa@testing.com"
        // });
        form.onSuccess(function(values, followUpUrl) {
            form.getFormElem().hide();
            return false;
        });
    });

    MktoForms2.whenReady(function(form) {
        var $mktoForm = $('form');
        var $inputs = $('form :input');
        var $quiz = $('#question');

        // console.log($inputs);
        // console.log($mktoForm);

        $quiz.on('change', 'input[name=choices]', function() {
            console.log($(this));
            console.log($(this).attr('data-answer'));
            var answer = $(this).attr('data-answer');
            for(var i = 0; i < $inputs.length; i++) {
                if($inputs[i].value == answer && $($inputs[i]).prop('checked')) {
                    $($inputs[i]).prop('checked', false);
                } else if($inputs[i].value == answer) {
                    $($inputs[i]).prop('checked', true);
                } else {
                    // console.log('answer false');
                }
            }
        });
    });

    function submitMkto() {
        MktoForms2.whenReady(function(form) {
            form.submit();
        });
    }

    // Function to read the value of a cookie
    function readCookie(name) {
        var cookiename = name + "=";
        var ca = document.cookie.split(';');
        for(var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while(c.charAt(0) == ' ') c = c.substring(1, c.length);
            if(c.indexOf(cookiename) == 0) return c.substring(cookiename.length, c.length);
        }
        return null;
    }

    $(document).ready(function() {
        $('#testSubmit').on('click', function() {
            MktoForms2.whenReady(function(form) {
                form.submit();
            });
        });
    });




})(jQuery);




// <script src="//app-sj23.marketo.com/js/forms2/js/forms2.min.js"></script>
// <form id="mktoForm_13871"></form>
// <script>MktoForms2.loadForm("//app-sj23.marketo.com", "539-TIB-603", 13871);</script>

