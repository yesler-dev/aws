(function($) {

// new categories
// hands
// class
// digital
// exam

var all_results = [{
    type: "choices-class",
    text: {
        headline: "课堂培训",
        link: "https://aws.amazon.com/training/course-descriptions/",
        desc: "由具有深厚技术专业知识的认证讲师指导的面对面培训、虚拟培训和私人现场培训",
        long: "实际课堂或虚拟课堂可以为希望深化技术技能的人员提供更深入的培训。课堂内容包括在相应领域专家指导下进行的演示、动手实验和小组讨论。"
    }
}, {
    type: "choices-digital",
    text: {
        headline: "数字化培训",
        link: "https://aws.amazon.com/training/course-descriptions/",
        desc: "免费的按需数字化课程可以帮助您按照自己的进度加深知识",
        long: "只需一点时间和主动性，您就可以通过按需在线培训来强化自己的实用云知识。这些在线学习课程的时长从一小时到一天不等，可以帮助您拓宽对特定主题的理解。"
    }
}, {
    type: "choices-hands",
    text: {
        headline: "实践",
        link: "https://aws.amazon.com/training/course-descriptions/cloud-practitioner-essentials/",
        desc: "我们建议您在在真实的云环境中进行 3 到 6 个月的实践工作，打下知识基础",
        long: "团队培训的效果最好，您可以获得在真实云环境中进行开发和操作所需的实用知识。三到六个月的实践可以让您获得良好的基础应用知识。"
    }
}, {
    type: "choices-exam",
    text: {
        headline: "自学",
        link: "https://www.youtube.com/user/AWSwebinars/featured",
        desc: "独立学习让您可以按照自己的进度填补知识空白并学习新主题",
        long: "独立学习让您可以按照自己的进度填补知识空白并学习新主题。我们有各种白皮书、博客文章、视频、网络研讨会、使用案例和同行资源可供希望深入了解特定技术主题的 IT 专业人员使用。"
    }
}];

var all_questions = [{

    question_string: "您在云计算方面有多少经验？请选择所有正确选项。",
    choices: {
        q1: {
            class: "我有 AWS 云使用经验"
        },
        q2: {
            digital: "我是 IT 专业人员，有本地设备使用经验"
        },
        q3: {
            hands: "我不是 IT 专业人员，没有云使用经验"
        },
        q4: {
            class: "我有其他云提供商使用经验"
        },
        q5: {
            digital: "我不是 IT 专业人员，但有云使用经验"
        },
        q6: {
            digital: "我刚刚完成学业，第一次进入就业市场"
        }
    },
    type: "checkbox"

}, {
    question_string: "您是需要快速接受培训以便实现工作目标，还是在按照自己的进度推动职业发展？",
    choices: {
        q1: {
            digital: "我需要快速接受培训"
        },
        q2: {
            digital: "我的时间安排很灵活"
        }
    },
    type: "radio"
}, {
    question_string: "每个人都不一样。您希望通过哪种方式来学习？请选择所有正确选项。",
    choices: {
        q1: {
            class: "我在有组织的课堂上学习效果最好"
        },
        q2: {
            class: "我想要向 AWS 云专家提出问题"
        },
        q3: {
            digital: "我喜欢实验，尝试和失败对我来说不是问题"
        },
        q4: {
            digital: "我愿意自学，希望有丰富的选择"
        },
        q5: {
            hands: "我希望通过阅读的方式学习"
        },
        q6: {
            digital: "我喜欢视频和看得到的演示"
        },
        q7: {
            hands: "我喜欢在实践中学习，需要实际的云操作经验"
        }
    },
    type: "checkbox"
}, {
    question_string: "您是个人接受培训还是集体接受培训？",
    choices: {
        q1: {
            class: "我与一个小组或小团队共同接受培训"
        },
        q2: {
            class: "我与整个团队（或部门或大群体）共同接受培训"
        },
        q3: {
            digital: "我主要关注个人发展"
        }
    },
    type: "radio"
}, {
    question_string: "谁支付您的培训费用？",
    choices: {
        q1: {
            class: "我的公司支付"
        },
        q2: {
            digital: "我几乎没有预算"
        },
        q3: {
            class: "我自己支付自己的所有培训费用"
        },
        q4: {
            digital: "我支付自己的部分培训费用"
        },
        q5: {
            hands: "我没有预先批准的预算，但我所在的小组可能有兴趣进一步接受培训"
        },
        q6: {
            class: "我有预先批准的预算"
        }
    },
    type: "radio"
}];

var Quiz = function(quiz_name) {
    this.quiz_name = quiz_name;
    this.questions = [];
}

// Function to add a question to the Quiz object
Quiz.prototype.add_question = function(question) {
    // Randomly choose where to add question
    // var index_to_add_question = Math.floor(Math.random() * this.questions.length);

    var index_to_add_question = this.questions.length;
    this.questions.splice(index_to_add_question, 0, question);
}

Quiz.prototype.render = function(container) {
    var self = this;
    var $submit = $('#submit-button'), $prev = $('#prev-question-button'), $next = $('#next-question-button');

    $('#quiz-results').hide();

    var question_container = $('<div>').attr('id', 'question').insertAfter('#quiz-name');
    function change_question(direction) {
        var dir = direction;
        self.questions[current_question_index].render(question_container, dir);
        moveIn();
        $prev.prop('disabled', current_question_index === 0)
        $prev.toggleClass('btnHidden', current_question_index === 0);
        $next.prop('disabled', self.questions[current_question_index].user_choice_index === null);

        // Determine if all questions have been answered
        var all_questions_answered = true;
        for(var i = 0; i < self.questions.length; i++) {
            if(self.questions[i].user_choice_index === null) {
                all_questions_answered = false;
                break;
            }
        }
        if(all_questions_answered && current_question_index == self.questions.length - 1) {
            $next.toggleClass('btnHidden');
            $submit.prop('enable')
            $submit.toggleClass('btnHidden');
        } else {
            $next.removeClass('btnHidden');
            $submit.prop('disabled');
            $submit.addClass('btnHidden');
        }
    }

    function change_page_number() {
        var curPage = parseInt(self.questions.indexOf(self.questions[current_question_index])) + 1;
        var totalPages = self.questions.length;
        var numbers = curPage + "/" + totalPages;
        $('#pageNumber').text(numbers);
    }

    // Render the first question
    var current_question_index = 0;
    change_question();
    change_page_number();
    // Add listener for the previous question button
    $prev.on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        if(current_question_index > 0) {
            current_question_index--;
            userScore.pop();
            change_question('prev');
            change_page_number();
        }
    });
    // Add listener for the next question button
    $next.on('click', function() {
        if(current_question_index < self.questions.length - 1 && current_question_index !== undefined) {
            current_question_index++;
            userScore.push(pushScore($('#question input[name=choices]:checked')));
            // console.log(userScore);
            moveOut();
            change_page_number();
        }
    });

    function pushScore(vals) {
        var curScore, checkScore = [], $this = vals;
        if($this.attr('type') == 'radio') {
            curScore = $this.val();
            //console.log(curScore);
            return curScore;
        } else {
            for(var i = 0; i < $this.length; i++) {
                checkScore.push($this[i].value);
            }
            //console.log(checkScore);
            return curScore = checkScore;
        }

    }

    function submitMkto() {
        MktoForms2.whenReady(function(form) {
            form.submit();
        });
    }

    // Add listener for the submit button
    $submit.on('click', function() {
        userScore.push(pushScore($('#question input[name=choices]:checked')));
        var disp = resultsDisplay(occurrence(userScore));
        $('#quiz-results').prepend(disp);
        // submitMkto();
        $('body').toggleClass('bodyResults');
        $('.questionsHold').toggleClass('btnHidden');
        $('.resultsFooter').toggleClass('btnHidden');
        $('#quiz-results').css('opacity', 0).slideDown(1000).animate({ opacity: 1}, { queue: false, duration: 2000});
        $('#quiz button').slideUp();
    });

    // Add a listener on the questions container to listen for user select changes
    // This is for determining whether we can submit answers of not.
    question_container.bind('user-select-change', function() {
        var all_questions_answered = true;
        for(var i = 0; i < self.questions.length; i++) {
            if(self.questions[i].user_choice_index === null) {
                all_questions_answered = false;
                break;
            }
        }

        if(all_questions_answered && current_question_index == self.questions.length - 1) {
            //console.log('the end');
            $next.addClass('btnHidden');
            $submit.prop('enable')
            $submit.removeClass('btnHidden');
        } else {
            //console.log('where am I');
            $next.removeClass('btnHidden');
            $submit.prop('disabled');
            $submit.addClass('btnHidden');
        }
    });

    function moveOut() {
        var $container = $('#question').children();
        var setback = ($container.length) * 30;
        $container.each(function(index, element) {
            setTimeout(function() {
                $(element).removeClass(function (index, css) {
                    return (css.match (/\bmoveIn-\S+/g) || []).join(' ');
                });
                $(element).addClass('moveOut');
            }, ( index * 30 ));
        });

        setTimeout(function() {
            $('.buttonHolder').removeClass(function (index, css) {
                    return (css.match (/\bmoveIn-\S+/g) || []).join(' ');
                }).addClass('moveOut');
        }, (setback));
        setTimeout(function() {
            change_question();
        }, (setback + 445));
    }

    function moveIn() {
        var $container = $('#question').children();
        var timer = ($container.length) / 2 * 30;
        var setback = ($container.length + 1) / 2;

        $('#question h3').addClass('moveIn-header');
        setTimeout(function() {
            $('.buttonHolder').removeClass('moveOut');
            $('.buttonHolder button, .buttonHolder a').css({ opacity : 1 });
            $('.buttonHolder').addClass('moveIn-q' + setback);
        }, timer);
    }
}// end Quiz.prototype.render

var ScoreKeeper = function(title, score, answer) {
    this.title  = title,
    this.score  = score,
    this.answer = answer
}
var userScore = [];
var occurrence = function(score) {
    var result, item = [], tempCount = 0, suggestion, flat;
    flat = [].concat.apply([], score);

    result = flat.reduce(function(prev, next) {
        prev[next] = (prev[next] + 1) || 1;
        return prev;
    },{});
    for(var k in result) {
        if(result[k] > tempCount) {
            item.pop();
            tempCount = result[k];
            item.unshift(k);
        }
    }
    suggestion = item[0];
    return suggestion;
};

var resultsDisplay = function(choice) {
    var classInfo = all_results, display = "", mainClass = "", otherClasses = "";
    //console.log('choice is: ' + choice);

    for(var i = 0; i < classInfo.length; i++) {
        //console.log(classInfo[i].type);
        if(choice == classInfo[i].type) {
            display += "<h2>根据您个人的偏好和因素，我们建议您接受以下类型的培训 <span>" + classInfo[i].text.headline + "</span></h2>";
            mainClass += "<div class='mainClass'>";
            mainClass += "<img src='./img/aws-training-icon-" + classInfo[i].type + ".svg' />";
            mainClass += "<div class='mainText'>"
            mainClass += "<h3>" + classInfo[i].text.headline + "</h3>";
            mainClass += "<p>" + classInfo[i].text.long + "</p>";
            mainClass += "<a href='" + classInfo[i].text.link + "' target='_blank'>了解更多</a>";
            mainClass += "</div>";
            mainClass += "</div>";
        } else {
            otherClasses += "<article class='otherClass'>";
            otherClasses += "<img src='./img/aws-training-icon-" + classInfo[i].type + ".svg' />";
            otherClasses += "<div><h5>" + classInfo[i].text.headline + "</h5><p>" + classInfo[i].text.desc + "</p><a href='" + classInfo[i].text.link + "' target='_blank'>了解更多</a>";
            otherClasses += "</article>";
        }
    }
    display += mainClass;
    display += "<div class='otherClassesContainer'><h4>通过其他类型的培训让您受到的教育更加完善：</h4>" + otherClasses + "</div>";
    return display;
};

// object for a Question. Constructor
var Question = function(question_string, choices, type) {
    this.question_string   = question_string;
    this.choices           = [];
    this.user_choice_index = null; //Index of the user's choice selection
    this.type              = type;


    // TODO - fill choices[] with choices and do something with it
    var number_of_choices = choices;
    for(var key in number_of_choices) {
        var obj = number_of_choices[key];
        for(var prop in obj) {
            var quest = new ScoreKeeper(key, prop, obj[prop]);
            this.choices.push(quest);
        }
    }
}// end Question

// function that will render the questions inside the container
Question.prototype.render = function(container, direction) {
    var self = this;
    var dir = direction;
    // Fill out the question label
    var question_string_h3;
    if(container.children('h3').length === 0) {
        question_string_h3 = $('<h3>').appendTo(container);
    } else {
        question_string_h3 = container.children('h3').first();
    }
    question_string_h3.text(this.question_string);
    // console.log(question_string_h3);
    // Clear any radio buttons and create new ones
    if(container.children('input[type=radio]').length > 0) {
        container.children('input[type=radio]').each(function() {
            var radio_button_id = $(this).attr('id');
            $(this).remove();
            container.children('label[for=' + radio_button_id + ']').remove();
        });
    }
    if(container.children('input[type=checkbox]').length > 0 ) {
        container.children('input[type=checkbox]').each(function() {
            var checkbox_id = $(this).attr('id');
            $(this).remove();
            container.children('label[for=' + checkbox_id + ']').remove();
        });
    }
    for(var i = 0; i < this.choices.length; i++) {
        //console.log(this.user_choice_index);
        // console.log(this.choices[i].answer);
        if(this.type == "checkbox") {
            // Create the checkbox
            var choice_radio_button = $('<input>')
                .attr('id', 'choices-' + this.choices[i].title)
                .attr('type', 'checkbox')
                .attr('name', 'choices')
                .attr('data-answer', this.choices[i].answer)
                .attr('value', 'choices-' + this.choices[i].score);

                //.appendTo(container);
            if(this.user_choice_index !== null) {
                // console.log("i : " + i);
                // console.log("userchoiceindex: " + this.user_choice_index);
                choice_radio_button.attr('checked', this.user_choice_index.includes(i + 1))
                    .appendTo(container);
            } else {
                choice_radio_button.appendTo(container);
            }
        } else {
            // Create the radio button
            var choice_radio_button = $('<input>')
                .attr('id', 'choices-' + this.choices[i].title)
                .attr('type', 'radio')
                .attr('name', 'choices')
                .attr('data-answer', this.choices[i].answer)
                .attr('value', 'choices-' + this.choices[i].score)
                .attr('checked', i === (this.user_choice_index - 1))
                .appendTo(container);
        }
        // Create the lable
        if(dir == 'prev') {
            var choice_label = $('<label>')
                .text(this.choices[i].answer)
                .attr('for', 'choices-' + this.choices[i].title)
                .attr('class', 'moveIn-place')
                .appendTo(container);
        } else {
            var choice_label = $('<label>')
                .text(this.choices[i].answer)
                .attr('for', 'choices-' + this.choices[i].title)
                .attr('class', 'moveIn-' + this.choices[i].title)
                .appendTo(container);
        }

    }
    var checkboxAnswerIndex = [];
    // Add a listener for the radio button to change which on the user has clicked on
    $('input[name=choices]').change(function(index) {
        //var selected_radio_button_value = $('input[name=choices]:checked').val();
        // var answerIndex = $('input[name=choices]:checked').attr('id');
        var answerIndex = $(this).attr('id');
        if($(this).attr('type') == 'checkbox') {
            //console.log("answerIndex : " + answerIndex);
            // console.log($(this).prop('checked'));
            if(checkboxAnswerIndex.includes(parseInt(answerIndex.substr(answerIndex.length - 1, 1)))) {

                var i = checkboxAnswerIndex.indexOf(parseInt(answerIndex.substr(answerIndex.length - 1, 1)));
                if(i != -1) {
                    checkboxAnswerIndex.splice(i, 1);
                }
                // checkboxAnswerIndex = checkboxAnswerIndex.filter(function(item) {
                //     return item !== answerIndex;
                // });
            } else {
                checkboxAnswerIndex.push(parseInt(answerIndex.substr(answerIndex.length - 1, 1)));
            }
            //console.log('checkboxindes: ' + checkboxAnswerIndex);
        }
        // console.log(checkboxAnswerIndex);
        // console.log($(this).attr('id'));
        // console.log(answerIndex);
        if($('#next-question-button').prop('disabled')) {
            $('#next-question-button').prop('disabled', false);
        }
        // Change the user choice index
        if($(this).attr('type') == 'checkbox') {
            self.user_choice_index = checkboxAnswerIndex;
        } else {
            self.user_choice_index = parseInt(answerIndex.substr(answerIndex.length - 1, 1));
        }


        // Trigger a user-select-change
        container.trigger('user-select-change');
    });
}// end Question.prototype.render


$(document).ready(function() {
    // Create an instance of the Quiz object
    var quiz = new Quiz('Modality');
    // Create the Question objects from all_questions
    for(var i = 0; i < all_questions.length; i++) {
        var question = new Question(all_questions[i].question_string, all_questions[i].choices, all_questions[i].type);
        // Add the question to the instance of the Quiz object
        quiz.add_question(question);
    }

    // Render the quiz
    var quiz_container = $('.quizContainer');
    quiz.render(quiz_container);

    $('.pageStart').on('click', 'a', function() {
        $('.pageStart').addClass('noHeight');
        var $container = $('.pageStart').children();
        //$('#pageNumber').fadeToggle();
        $container.each(function(index, element) {
            setTimeout(function() {
                $(element).addClass('moveOut');
            }, 100 + ( index * 100 ));
        });

        $('.quizContainer').toggleClass('btnHidden');
        $('.questionsHold').addClass('show');
    });
});


})(jQuery);
























