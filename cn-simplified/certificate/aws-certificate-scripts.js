(function($) {

// answers
// cloud - intro
// arch - assoc or pro
// archassoc - assoc
// archpro - pro
// dev - assoc
// sysops - assoc
// devops - pro
// pro - only recommend pro level
// assoc - only recommend assoc level
// none - auto cloud rec
// all - special message
// spec - one of the add ons
// data
// security
// networking


var all_results = [{
    type: "choices-cloud",
    text: {
        headline: "AWS Certified Cloud Practitioner",
        link: "https://aws.amazon.com/certification/certified-cloud-practitioner/",
        desc: "这项基础认证将检验您对 AWS 云的实践、原则和特征的核心理解。此认证适用于在 AWS 方面有六个月以上经验的人士。",
        long: "这项基础认证将检验您对 AWS 云的实践、原则和特征的核心理解。此认证适用于在 AWS 方面有六个月以上经验的人士。"
    }
}, {
    type: "choices-archassoc",
    text: {
        headline: "AWS Certified Solutions Architect – Associate",
        link: "https://aws.amazon.com/certification/certified-solutions-architect-associate/",
        desc: "这项 Associate 认证将检验您在设计基于 AWS 的分布式应用程序和系统方面的的知识和技能。此认证适用于在 AWS 设计方面有一年或一年以上经验的专业人士。",
        long: "这项 Associate 认证将检验您在设计基于 AWS 的分布式应用程序和系统方面的的知识和技能。此认证适用于在 AWS 设计方面有一年或一年以上经验的专业人士。"
    }
}, {
    type: "choices-archpro",
    text: {
        headline: "AWS Certified Solutions Architect – Professional",
        link: "https://aws.amazon.com/certification/certified-solutions-architect-professional/",
        desc: "这项 Professional 认证将检验您在设计基于 AWS 的分布式应用程序和系统方面的高级技术技能和经验。此认证适用于已取得 AWS Certified Solutions Architect – Associate 认证的经验丰富的架构师。",
        long: "这项 Professional 认证将检验您在设计基于 AWS 的分布式应用程序和系统方面的高级技术技能和经验。此认证适用于已取得 AWS Certified Solutions Architect – Associate 认证的经验丰富的架构师。"
    }
}, {
    type: "choices-dev",
    text: {
        headline: "AWS Certified Developer – Associate",
        link: "https://aws.amazon.com/certification/certified-developer-associate/",
        desc: "这项 Associate 认证将检验您在开发和维护基于 AWS 的应用程序方面的专业技术能力。此认证适用于在设计和维护基于 AWS 的应用程序方面具有一年或一年以上实践经验的开发人员。",
        long: "这项 Associate 认证将检验您在开发和维护基于 AWS 的应用程序方面的专业技术能力。此认证适用于在设计和维护基于 AWS 的应用程序方面具有一年或一年以上实践经验的开发人员。"
    }
}, {
    type: "choices-sysops",
    text: {
        headline: "AWS Certified SysOps Administrator – Associate",
        link: "https://aws.amazon.com/certification/certified-sysops-admin-associate/",
        desc: "这项 Associate 认证将检验您在 AWS 上进行部署、管理和运维的专业技术能力。此认证适用于在基于 AWS 的应用程序的运维方面具有一年或一年以上实践经验的管理员。",
        long: "这项 Associate 认证将检验您在 AWS 上进行部署、管理和运维的专业技术能力。此认证适用于在基于 AWS 的应用程序的运维方面具有一年或一年以上实践经验的管理员。"
    }
}, {
    type: "choices-devops",
    text: {
        headline: "AWS Certified DevOps Engineer – Professional",
        link: "https://aws.amazon.com/certification/certified-devops-engineer-professional/",
        desc: "这项 Professional 认证将检验您在预置、运维和管理基于 AWS 的分布式应用程序和系统方面的专业技术能力。此认证适用于已获得 AWS Certified Developer – Associate 或 AWS Certified SysOps Administrator – Associate 认证的经验丰富的架构师。",
        long: "这项 Professional 认证将检验您在预置、运维和管理基于 AWS 的分布式应用程序和系统方面的专业技术能力。此认证适用于已获得 AWS Certified Developer – Associate 或 AWS Certified SysOps Administrator – Associate 认证的经验丰富的架构师。"
    }
}, {
    type: "choices-networking",
    text: {
        headline: "AWS Certified Advanced Networking – Specialty",
        link: "https://aws.amazon.com/certification/",
        desc: "这项 Specialty 认证将检验您在大规模设计和实施 AWS 及混合 IT 网络架构方面的高级技术技能。",
        long: "这项 Specialty 认证将检验您在大规模设计和实施 AWS 及混合 IT 网络架构方面的高级技术技能。"
    }
}, {
    type: "choices-data",
    text: {
        headline: "AWS Certified Big Data – Specialty",
        link: "https://aws.amazon.com/certification/",
        desc: "这项 Specialty 认证将检验您在设计和实施 AWS 服务，以便从数据中发掘价值方面的技术技能和经验。",
        long: "这项 Specialty 认证将检验您在设计和实施 AWS 服务，以便从数据中发掘价值方面的技术技能和经验。"
    }
}, {
    type: "choices-security",
    text: {
        headline: "AWS Certified Security – Specialty",
        link: "https://aws.amazon.com/certification/",
        desc: "这项 Specialty 认证将检验您在 AWS 安全保护方面的知识，以及与事件响应、日志记录和监控、基础设施安全、身份和访问管理及数据保护相关的技术技能。",
        long: "这项 Specialty 认证将检验您在 AWS 安全保护方面的知识，以及与事件响应、日志记录和监控、基础设施安全、身份和访问管理及数据保护相关的技术技能。"
    }
}];

var all_questions = [{

    question_string: "请告诉我们您关于AWS的经验",
    choices: {
        q1: {
            cloud: "一年之内，我曾经在管理、领导、测试或其他非编程功能方面参与过AWS项目。"
        },
        q2: {
            archassoc: "我曾经作为架构师/设计师参与过多个AWS项目。"
        },
        q3: {
            sysops: "我曾经参与过AWS上的多个项目部署"
        },
        q4: {
            dev: "我参与过多个项目在 AWS 上的构建和开发"
        },
        q5: {
            none: "我没有接触过 AWS"
        }
    },
    type: "checkbox"

}, {
    question_string: "您想担任哪种角色？",
    choices: {
        q1: {
            archassoc: "云安全工程师"
        },
        q2: {
            dev: "网络工程师"
        },
        q3: {
            cloud: "项目经理"
        },
        q4: {
            archassoc: "数据科学家"
        },
        q5: {
            dev: "初级 IT 人员"
        },
        q6: {
            cloud: "领导者/管理人员"
        },
        q7: {
            archassoc: "解决方案架构师"
        },
        q8: {
            archassoc: "企业架构师"
        },
        q9: {
            archassoc: "技术架构师"
        },
        q10: {
            archassoc: "应用程序架构师"
        },
        q11: {
            dev: "开发人员"
        },
        q12: {
            dev: "软件工程师"
        },
        q13: {
            dev: "编码人员"
        },
        q14: {
            sysops: "系统运维人员"
        },
        q15: {
            sysops: "安全运维人员"
        },
        q16: {
            devops: "开发运维人员"
        },
        q17: {
            sysops: "系统操作员"
        },
        q18: {
            sysops: "系统管理员"
        }
    },
    type: "checkbox"
}, {
    question_string: "您已取得哪些 AWS Certification？",
    choices: {
        q1: {
            archassoc: "AWS Certified Cloud Practitioner"
        },
        q2: {
            archpro: "AWS Certified Solutions Architect – Associate"
        },
        q3: {
            devops: "AWS Certified Developer – Associate"
        },
        q4: {
            devops: "AWS Certified SysOps Administrator – Associate"
        },
        q5: {
            proSpec1: "AWS Certified Solutions Architect – Professional"
        },
        q6: {
            proSpec2: "AWS Certified DevOps Engineer – Professional"
        },
        q7: {
            spec: "AWS Certified Advanced Networking – Specialty"
        },
        q8: {
            spec: "AWS Certified Big Data – Specialty"
        },
        q9: {
            spec: "AWS Certified Security – Specialty"
        },
        q10: {
            all: "以上皆是"
        },
        q11: {
            none: "无 "
        }
    },
    type: "checkbox"
}, {
    question_string: "您在云知识方面的目标是什么？",
    choices: {
        q1: {
            dev: "我想在云中构建稳健的创新应用程序。"
        },
        q2: {
            archassoc: "我想构建并部署可以根据不同负载和客户需求进行扩展的系统。"
        },
        q3: {
            archassoc: "我想确保我构建和部署的系统永远不会发生故障。"
        },
        q4: {
            archassoc: "我想知道如何部署冗余的生产应用程序。"
        },
        q5: {
            archassoc: "我想成为所在组织的安全问题权威。"
        },
        q6: {
            spec: "我想了解如何从数据中得出明智的结论。"
        },
        q7: {
            spec: "我想了解如何解读访问日志并更好地控制对敏感数据的访问。"
        },
        q8: {
            sysops: "我希望能够将本地应用程序迁移到云中。"
        },
        q9: {
            sysops: "我想有效地设置 CI/CD 开发环境。"
        },
        q10: {
            spec: "我想配置系统以提供 DDoS 和 EDoS 保护。"
        },
        q11: {
            archassoc: "我想了解基本云架构和核心服务，以便做出更明智的业务决策。"
        },
        q12: {
            other: "其他"
        }
    },
    type: "checkbox"
}, {
    question_string: "您对哪些 AWS 主题感兴趣？",
    choices: {
        q1: {
            spec: "我想部署结合本地数据中心元素和 AWS 组件的混合系统。"
        },
        q2: {
            archassoc: "我想拓展关于使用主要 AWS 服务（如 AWS 数据库、通知、存储和工作流服务，以及变更管理服务）的应用程序的实用知识。"
        },
        q3: {
            dev: "我想了解如何设计、开发、部署和维护基于 AWS 的安全可靠的应用程序。"
        },
        q4: {
            spec: "我想了解如何配置 NACL/ACL、防火墙、网关、VPN、密钥管理、SSL、HTTPS、SAML、令牌和证书。"
        },
        q5: {
            blank: "以上皆非"
        }
    },
    type: "checkbox"
}];

var Quiz = function(quiz_name) {
    this.quiz_name = quiz_name;
    this.questions = [];
}

// Function to add a question to the Quiz object
Quiz.prototype.add_question = function(question) {
    // Randomly choose where to add question
    // var index_to_add_question = Math.floor(Math.random() * this.questions.length);

    var index_to_add_question = this.questions.length;
    this.questions.splice(index_to_add_question, 0, question);
}

Quiz.prototype.render = function(container) {
    var self = this;
    var $submit = $('#submit-button'), $prev = $('#prev-question-button'), $next = $('#next-question-button');

    $('#quiz-results').hide();

    var question_container = $('<div>').attr('id', 'question').insertAfter('#quiz-name');
    function change_question(direction) {
        var dir = direction;
        self.questions[current_question_index].render(question_container, dir);
        // console.log(self);
        moveIn();
        $prev.prop('disabled', current_question_index === 0)
        $prev.toggleClass('btnHidden', current_question_index === 0);
        $next.prop('disabled', self.questions[current_question_index].user_choice_index === null);

        // Determine if all questions have been answered
        var all_questions_answered = true;
        for(var i = 0; i < self.questions.length; i++) {
            if(self.questions[i].user_choice_index === null) {
                all_questions_answered = false;
                break;
            }
        }
        if(all_questions_answered && current_question_index == self.questions.length - 1) {
            $next.toggleClass('btnHidden');
            $submit.prop('enable')
            $submit.toggleClass('btnHidden');
        } else {
            $next.removeClass('btnHidden');
            $submit.prop('disabled');
            $submit.addClass('btnHidden');
        }
    }

    function change_page_number() {
        var curPage = parseInt(self.questions.indexOf(self.questions[current_question_index])) + 1;
        var totalPages = self.questions.length;
        var numbers = curPage + "/" + totalPages;
        $('#pageNumber').text(numbers);
    }

    // Render the first question
    var current_question_index = 0;
    change_question();
    change_page_number();
    // Add listener for the previous question button
    $prev.on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        if(current_question_index > 0) {
            current_question_index--;
            userScore.pop();
            change_question('prev');
            change_page_number();
        }
    });
    // Add listener for the next question button
    $next.on('click', function() {
        var body = $(document).height();

        //console.log(body);
        if(current_question_index < self.questions.length - 1 && current_question_index !== undefined) {
            current_question_index++;
            userScore.push(pushScore($('#question input[name=choices]:checked')));
            console.log(userScore);

            moveOut();

            change_page_number();
            // $('#body').height(body);
        }
    });

    function pushScore(vals) {
        var curScore, checkScore = [], $this = vals;
        if($this.attr('type') == 'radio') {
            curScore = $this.val();
            // console.log(curScore);
            return curScore;
        } else {
            for(var i = 0; i < $this.length; i++) {
                if($this[i].value == "choices-other") {
                    break;
                }
                if($this[i].value == "choices-blank") {
                    break;
                }
                checkScore.push($this[i].value);
            }
            console.log(checkScore);
            return curScore = checkScore;
        }

    }

    function submitMkto() {
        MktoForms2.whenReady(function(form) {
            form.submit();
        });
    }

    // Add listener for the submit button
    $submit.on('click', function() {
        userScore.push(pushScore($('#question input[name=choices]:checked')));
        // console.log(userScore);
        var disp = resultsDisplay(occurrence(userScore));
        $('#quiz-results').prepend(disp);
        // submitMkto();
        $('body').toggleClass('bodyResults');
        $('.questionsHold').toggleClass('btnHidden');
        $('.resultsFooter').toggleClass('btnHidden');
        $('#quiz-results').css('opacity', 0).slideDown(1000).animate({ opacity: 1}, { queue: false, duration: 2000});
        $('#quiz button').slideUp();
    });

    // Add a listener on the questions container to listen for user select changes
    // This is for determining whether we can submit answers of not.
    question_container.bind('user-select-change', function() {
        var all_questions_answered = true;
        for(var i = 0; i < self.questions.length; i++) {
            if(self.questions[i].user_choice_index === null) {
                all_questions_answered = false;
                break;
            }
            //console.log(self);
        }

        if(all_questions_answered && current_question_index == self.questions.length - 1) {
            //console.log('the end');
            $next.addClass('btnHidden');
            $submit.prop('enable')
            $submit.removeClass('btnHidden');
        } else {
            //console.log('where am I');
            $next.removeClass('btnHidden');
            $submit.prop('disabled');
            $submit.addClass('btnHidden');
        }
    });

    function moveOut() {
        var $container = $('#question').children();
        var setback = ($container.length) * 30;
        $container.each(function(index, element) {
            setTimeout(function() {
                $(element).removeClass(function (index, css) {
                    return (css.match (/\bmoveIn-\S+/g) || []).join(' ');
                });
                $(element).addClass('moveOut');
            }, ( index * 30 ));
        });

        setTimeout(function() {
            $('.buttonHolder').removeClass(function (index, css) {
                    return (css.match (/\bmoveIn-\S+/g) || []).join(' ');
                }).addClass('moveOut');
        }, (setback));
        setTimeout(function() {
            change_question();
        }, (setback + 445));
    }

    function moveIn() {
        var $container = $('#question').children();
        var timer = ($container.length) / 2 * 30;
        var setback = ($container.length + 1) / 2;

        $('#question h3').addClass('moveIn-header');
        setTimeout(function() {
            $('.buttonHolder').removeClass('moveOut');
            $('.buttonHolder button, .buttonHolder a').css({ opacity : 0.8 });
            $('.buttonHolder').addClass('moveIn-q' + setback);
        }, timer);
    }
}// end Quiz.prototype.render

var ScoreKeeper = function(title, score, answer) {
    this.title  = title,
    this.score  = score,
    this.answer = answer;
}
var userScore = [];
var occurrence = function(score) {
    var countedSpecs = function(choices) {
        var specNum = 0
        for(i = 0; i < choices.length; i++) {
            if(choices[i] == 'choices-spec') {
                specNum++;
            }
        }
        return specNum;
    }

    var result, item = [], tempCount = 0, suggestion, flat, level, autoRec;
    console.log(score);
    if(score[2].includes('choices-archpro') || score[2].includes('choices-devops') ) {
        level = 'pro';
    }
    if(score[2].includes('choices-spec') && score[2].includes('choices-archpro') || score[2].includes('choices-spec') && score[2].includes('choices-devops') ) {
        autoRec = 'specs';
    }

    if(countedSpecs(score[2]) == 3) {
        if(score[2].includes('choices-proSpec1') && score[2].includes('choices-proSpec2')) {
            autoRec = 'all';
        } else if(score[2].includes('choices-proSpec1')) {
            autoRec = 'choices-proSpec2';
        } else if(score[2].includes('choices-proSpec2')) {
            autoRec = 'choices-proSpec1';
        } else if(level == null) {
            autoRec = 'all';
        }
    }

    // if(score[2].includes('choices-spec') && score[2].includes('choices-proSpec1') && score[2].includes('choices-proSpec2')) {
    //     // if(countedSpecs(score[2]) == 5) {
    //         autoRec = 'all';
    //     // }
    // }
    // if(score[2].includes('choices-spec') && score[2].length >= 3 && level == null) {
    //     console.log(countedSpecs(score[2]));
    //     if(countedSpecs(score[2]) == 3) {
    //         autoRec = 'all';
    //     }
    // }
    // if(score[2].includes('choices-spec', 'choices-spec', 'choices-spec', 'choices-spec', 'choices-spec')) {
    //     autoRec = 'all';
    // }



    flat = [].concat.apply([], score);
    // console.log(flat);
    result = flat.reduce(function(prev, next) {
        prev[next] = (prev[next] + 1) || 1;
        return prev;
    },{});
    console.log("level: " + level);
    console.log("autoRec: " + autoRec);
    console.log(result);
    for(var k in result) {
        console.log('k: ' + k);
        if(k == 'choices-all') {
            // console.log('inside the if');
            item.pop();
            item.unshift(k);
            // console.log('item is: ' + item);
            break;
        }
        if(k == 'choices-devops' && item == 'choices-dev') {
            // console.log('inside DevOps');
            item.pop();
            item.unshift(k);
            // console.log('new k item is: ' + item);
        }
        if(result[k] > tempCount) {
            item.pop();
            tempCount = result[k];
            item.unshift(k);
        }
        console.log('item: ' + item);
    }
    suggestion = item[0];
    if(suggestion == 'choices-archassoc' && level == 'pro') {
        suggestion = 'choices-archpro';
    } else if(suggestion == 'choices-devops' || suggestion == 'choices-sysops' && level == 'pro') {
        suggestion = 'choices-devops';
    }
    if(autoRec == 'specs') {
        suggestion = 'choices-spec';
    }
    if(autoRec == 'all') {
        suggestion = 'choices-all';
    }
    if(autoRec == 'choices-proSpec1') {
        suggestion = 'choices-archpro';
    }
    if(autoRec == 'choices-proSpec2') {
        suggestion = 'choices-devops';
    }
    // console.log(suggestion);
    return suggestion;
};

var resultsDisplay = function(choice) {
    var classInfo = all_results, display = "", mainClass = "", otherClasses = "";
    // console.log(classInfo);
    // console.log('choice is: ' + choice);
    if(choice == 'choices-none') {
        choice = 'choices-cloud';
    }
    if(choice == 'choices-arch') {
        choice = 'choices-archassoc';
    }
    display += "<div class='resultsHeader'><span class='shapeHolderLeftResults shapeResults'></span>";
    mainClass += "<div class='innerResults'>";
    mainClass += "<div class='innerHolder'>";
    if(choice == "choices-spec") {
        mainClass += "<h2>AWS Specialty 认证</h2>";
        mainClass += "<p>这些高级认证将检验您在特定技术领域的高级技能。它们适用于已获得 AWS Certified Cloud Practitioner 认证或持有有效的 Associate 级认证的人士。</p>";
        // mainClass += "<h2>AWS Specialty Certifications</h2>";
        // mainClass += "<p>These certifications validate your advanced skills in specific technical areas. They are for individuals who have achieved AWS Certified Cloud Practitioner or have an active Associate-level certification.</p>";
    } else if(choice == "choices-all") {
        mainClass += "<h2>继续 AWS Certification 之旅</h2>";
        mainClass += "<p>您已经通过高级 AWS Certification 检验了自己的 AWS 技能；现在，是时候继续拓展您的技术知识了。请随时关注新的 Specialty 认证，并留意相关提醒，确保每 24 个月重新取得一次认证。</p>";
    } else {
        mainClass += "<h2>您的认证路径</h2>";
        mainClass += "<p>根据您的回答，我们建议您专注于以下认证：</p>";
    }
    mainClass += "<span class='shapeHolderRightResults shapeResults'></span></div></div></div>";
    for(var i = 0; i < classInfo.length; i++) {
        if(choice == 'choices-all') {
            mainClass += "<div class='mainClass thanksResults'><div class='innerResults'>";
            break;
        }
        if(choice == 'choices-spec') {
            mainClass += "<div class='mainClass specResults'><div class='innerResults'>";
            mainClass += "<article>"
            mainClass += "<img src='./img/icons/aws-certificate-icon-" + classInfo[6].type + ".svg' />";
            mainClass += "<div class='mainText'>"
            mainClass += "<h3>" + classInfo[6].text.headline + "</h3>";
            mainClass += "<p>" + classInfo[6].text.long + "</p>";
            mainClass += "</article>";
            mainClass += "<article>"
            mainClass += "<img src='./img/icons/aws-certificate-icon-" + classInfo[7].type + ".svg' />";
            mainClass += "<div class='mainText'>"
            mainClass += "<h3>" + classInfo[7].text.headline + "</h3>";
            mainClass += "<p>" + classInfo[7].text.long + "</p>";
            mainClass += "</article>";
            mainClass += "<article>"
            mainClass += "<img src='./img/icons/aws-certificate-icon-" + classInfo[8].type + ".svg' />";
            mainClass += "<div class='mainText'>"
            mainClass += "<h3>" + classInfo[8].text.headline + "</h3>";
            mainClass += "<p>" + classInfo[8].text.long + "</p>";
            mainClass += "</article>";
            mainClass += "<p class='specLearnMore'>请随时关注新推出的 Specialty 认证。</p>";
            mainClass += "<a class='ctaButton' href='" + classInfo[6].text.link + "' target='_blank'>了解更多</a>";
            mainClass += "</div>";
            break;
        }
        else if(choice == classInfo[i].type) {
            mainClass += "<div class='mainClass'><div class='innerResults'>";
            mainClass += "<img src='./img/icons/aws-certificate-icon-" + classInfo[i].type + ".svg' />";
            mainClass += "<div class='mainText'>"
            mainClass += "<h3>" + classInfo[i].text.headline + "</h3>";
            mainClass += "<p>" + classInfo[i].text.long + "</p>";
            mainClass += "<a class='ctaButton' href='" + classInfo[i].text.link + "' target='_blank'>了解更多</a>";
            mainClass += "</div>";
        }
    }
     mainClass += "</div></div>";
    display += mainClass;
    display += "<div class='otherClassesContainer'><p>我们提供四种基于角色的认证路径（旨在逐步提高专业水平），以及多种 Specialty 认证。 <a href='https://aws.amazon.com/certification/'>查看所有 AWS Certification 选项。</a></p></div>";
    return display;
};

// object for a Question. Constructor
var Question = function(question_string, choices, type) {
    this.question_string   = question_string;
    this.choices           = [];
    this.user_choice_index = null; //Index of the user's choice selection
    this.type              = type;

    // TODO - fill choices[] with choices and do something with it
    var number_of_choices = choices;
    for(var key in number_of_choices) {
        var obj = number_of_choices[key];
        for(var prop in obj) {
            var quest = new ScoreKeeper(key, prop, obj[prop]);
            this.choices.push(quest);
        }
    }
}// end Question

var makeRadio = function(curRadio, appendTo, index) {
    var choice_radio_button = $('<input>')
        .attr('id', 'choices-' + curRadio.choices[index].title)
        .attr('type', 'radio')
        .attr('name', 'choices')
        .attr('data-answer', this.choices[i].answer)
        .attr('value', 'choices-' + curRadio.choices[index].score)
        .attr('checked', index === (curRadio.user_choice_index - 1))
        .appendTo(appendTo);
}

var makeLabel = function(curRadio, appendTo, index, size) {
    var choice_label = $('<label>')
        .text(curRadio.choices[index].answer)
        .attr('for', 'choices-' + curRadio.choices[index].title)
        .attr('class', 'moveIn-' + curRadio.choices[index].title + ' ' + size)
        .appendTo(appendTo);
}

// function that will render the questions inside the container
Question.prototype.render = function(container, direction) {
    var self = this;
    console.log(self);
    var dir = direction;
    // Fill out the question label
    var question_string_h3;
    var question_string_hint;
    if(container.children('h3').length === 0) {
        question_string_h3 = $('<h3>').appendTo(container);
        // console.log('inside the if');
    } else {
        question_string_h3 = container.children('h3').first();
        // console.log('inside the else');
    }
    question_string_h3.text(this.question_string);
    question_string_hint = $('<span class="hintText">').appendTo(question_string_h3);
    if(this.type == "checkbox") {
        question_string_hint.text('多选');
    } else if(this.type == "radio") {
        question_string_hint.text('(Choose one)');
    }
    // Clear any radio buttons and create new ones
    if(container.children('input[type=radio]').length > 0) {
        container.children('input[type=radio]').each(function() {
            var radio_button_id = $(this).attr('id');
            $(this).remove();
            container.children('label[for=' + radio_button_id + ']').remove();
        });
    }
    if(container.children('input[type=checkbox]').length > 0 ) {
        container.children('input[type=checkbox]').each(function() {
            var checkbox_id = $(this).attr('id');
            $(this).remove();
            container.children('label[for=' + checkbox_id + ']').remove();
        });
    }
    // console.log(this.choices.length);
    for(var i = 0; i < this.choices.length; i++) {
        //console.log("i: " + i);
        if(this.type == "checkbox") {
            // Create the checkbox
            var choice_radio_button = $('<input>')
                .attr('id', 'choices-' + this.choices[i].title)
                .attr('type', 'checkbox')
                .attr('name', 'choices')
                .attr('data-answer', this.choices[i].answer)
                .attr('value', 'choices-' + this.choices[i].score);
                //.appendTo(container);
            if(this.user_choice_index !== null) {
                // console.log("i : " + i);
                // console.log("userchoiceindex: " + this.user_choice_index);
                choice_radio_button.attr('checked', this.user_choice_index.includes(i + 1))
                    .appendTo(container);
            } else {
                choice_radio_button.appendTo(container);
            }
            if(this.choices[i].score == 'all') {
                choice_radio_button.addClass('preventMore');
            }
        // } else if(this.type == "radio" && this.choices.length > 9) {
        //         // Create the radio button
        //         makeRadio(this, container, i);
        } else {
            // Create the radio button
            makeRadio(this, container, i);
        }
        // Create the label
        if(dir == 'prev') {
            if(this.type == "checkbox" && this.choices.length > 9) {
                var choice_label = $('<label>')
                    .text(this.choices[i].answer)
                    .attr('for', 'choices-' + this.choices[i].title)
                    .attr('class', 'moveIn-place halfSize')
                    .appendTo(container);
            } else {
                var choice_label = $('<label>')
                    .text(this.choices[i].answer)
                    .attr('for', 'choices-' + this.choices[i].title)
                    .attr('class', 'moveIn-place')
                    .appendTo(container);
            }

        } else if(this.type == "checkbox" && this.choices.length > 9) {
                makeLabel(this, container, i, 'halfSize');
                // makeLabel(this, container, i, 'fullSize');
        } else {
            makeLabel(this, container, i, 'fullSize');
        }
    } // end for loop

    var checkboxAnswerIndex = [];
    // Add a listener for the radio button to change which on the user has clicked on
    $('input[name=choices]').change(function(index) {
        //var selected_radio_button_value = $('input[name=choices]:checked').val();
        // var answerIndex = $('input[name=choices]:checked').attr('id');
        var answerIndex = $(this).attr('id');
        // console.log(checkboxAnswerIndex);
        if($(this).attr('type') == 'checkbox') {
            // console.log("answerIndex : " + answerIndex);
            // console.log($(this).prop('checked'));
            // console.log($(this));
            // console.log($(this)[0].value);
            if($(this)[0].value == 'choices-all' || $(this)[0].value == 'choices-none' || $(this)[0].value == 'choices-blank') {
                // console.log(this.id);
                var $ignore = $("label[for='" + this.id + "']");
                $('input[name=choices] + label').not($ignore).toggleClass('preventClick');
                $('input[name=choices]').not(this).prop('checked', false);
            }
            if(checkboxAnswerIndex.includes(parseInt(answerIndex.substring(9)))) {
                var i = checkboxAnswerIndex.indexOf(parseInt(answerIndex.substring(9)));
                if(i != -1) {
                    checkboxAnswerIndex.splice(i, 1);
                }
                // checkboxAnswerIndex = checkboxAnswerIndex.filter(function(item) {
                //     return item !== answerIndex;
                // });
            } else {
                checkboxAnswerIndex.push(parseInt(answerIndex.substr(9)));
            }
            //console.log('checkboxindes: ' + checkboxAnswerIndex);
        }
        // console.log(checkboxAnswerIndex);
        // console.log($(this).attr('id'));
        // console.log(answerIndex);
        if($('#next-question-button').prop('disabled')) {
            $('#next-question-button').prop('disabled', false);
        }
        // Change the user choice index
        if($(this).attr('type') == 'checkbox') {
            self.user_choice_index = checkboxAnswerIndex;
        } else {
            self.user_choice_index = parseInt(answerIndex.substr(answerIndex.length - 1, 1));
        }


        // Trigger a user-select-change
        container.trigger('user-select-change');
    });
}// end Question.prototype.render


$(document).ready(function() {
    // Create an instance of the Quiz object
    var quiz = new Quiz('Modality');
    // Create the Question objects from all_questions
    for(var i = 0; i < all_questions.length; i++) {
        var question = new Question(all_questions[i].question_string, all_questions[i].choices, all_questions[i].type);
        // Add the question to the instance of the Quiz object
        quiz.add_question(question);
    }

    // Render the quiz
    var quiz_container = $('.quizContainer');
    quiz.render(quiz_container);

    $('.pageStart').on('click', 'a', function() {
        $('.pageStart').addClass('noHeight');
        var $container = $('.pageStart').children();
        //$('#pageNumber').fadeToggle();
        $container.each(function(index, element) {
            setTimeout(function() {
                $(element).addClass('moveOut');
            }, 100 + ( index * 100 ));
        });

        $('.quizContainer').toggleClass('btnHidden');
        $('.questionsHold').addClass('show');
    });
});


})(jQuery);
























