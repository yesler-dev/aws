(function($) {

// answers
// cloud - intro
// arch - assoc or pro
// archassoc - assoc
// archpro - pro
// dev - assoc
// sysops - assoc
// devops - pro
// pro - only recommend pro level
// assoc - only recommend assoc level
// none - auto cloud rec
// all - special message
// spec - one of the add ons
// data
// security
// networking


var all_results = [{
    type: "choices-cloud",
    text: {
        headline: "AWS 認定クラウドプラクティショナー",
        link: "https://aws.amazon.com/certification/certified-cloud-practitioner/",
        desc: "このエントリーレベルの認定では、AWS クラウドのプラクティス、原則、特性について、重要な点を理解しているかどうかが検証されます。役割を問わず、6 か月以上の AWS 使用経験を持つ方にお勧めします。",
        long: "このエントリーレベルの認定では、AWS クラウドのプラクティス、原則、特性について、重要な点を理解しているかどうかが検証されます。役割を問わず、6 か月以上の AWS 使用経験を持つ方にお勧めします。"
    }
}, {
    type: "choices-archassoc",
    text: {
        headline: "AWS 認定ソリューションアーキテクト – アソシエイト",
        link: "https://aws.amazon.com/certification/certified-solutions-architect-associate/",
        desc: "このアソシエイト認定では、AWS で分散型のアプリケーションやシステムを設計するための知識とスキルが検証されます。AWS での設計に 1 年以上の経験を持つ方にお勧めします。",
        long: "このアソシエイト認定では、AWS で分散型のアプリケーションやシステムを設計するための知識とスキルが検証されます。AWS での設計に 1 年以上の経験を持つ方にお勧めします。"
    }
}, {
    type: "choices-archpro",
    text: {
        headline: "AWS 認定ソリューションアーキテクト – プロフェッショナル",
        link: "https://aws.amazon.com/certification/certified-solutions-architect-professional/",
        desc: "このプロフェッショナル認定では、AWS で分散型のアプリケーションとシステムを設計する高度な技術スキルと経験が検証されます。既に AWS 認定ソリューションアーキテクト – アソシエイトを取得している、経験豊富なアーキテクトを対象としています。",
        long: "このプロフェッショナル認定では、AWS で分散型のアプリケーションとシステムを設計する高度な技術スキルと経験が検証されます。既に AWS 認定ソリューションアーキテクト – アソシエイトを取得している、経験豊富なアーキテクトを対象としています。"
    }
}, {
    type: "choices-dev",
    text: {
        headline: "AWS 認定デベロッパー – アソシエイト",
        link: "https://aws.amazon.com/certification/certified-developer-associate/",
        desc: "このアソシエイト認定では、AWS でアプリケーションの開発と保守を行うための技術的な知識が検証されます。AWS ベースのアプリケーションの設計と保守について 1 年以上の実務経験を持つ開発者にお勧めします。",
        long: "このアソシエイト認定では、AWS でアプリケーションの開発と保守を行うための技術的な知識が検証されます。AWS ベースのアプリケーションの設計と保守について 1 年以上の実務経験を持つ開発者にお勧めします。"
    }
}, {
    type: "choices-sysops",
    text: {
        headline: "AWS 認定 SysOps アドミニストレーター – アソシエイト",
        link: "https://aws.amazon.com/certification/certified-sysops-admin-associate/",
        desc: "このアソシエイト認定では、AWS でのデプロイ、管理、運用に関する技術的な知識が検証されます。AWS ベースのアプリケーションの運用について 1 年以上の実務経験を持つ管理者にお勧めします。",
        long: "このアソシエイト認定では、AWS でのデプロイ、管理、運用に関する技術的な知識が検証されます。AWS ベースのアプリケーションの運用について 1 年以上の実務経験を持つ管理者にお勧めします。"
    }
}, {
    type: "choices-devops",
    text: {
        headline: "AWS 認定 DevOps エンジニア – プロフェッショナル",
        link: "https://aws.amazon.com/certification/certified-devops-engineer-professional/",
        desc: "このプロフェッショナル認定では、AWS で分散アプリケーションシステムのプロビジョニング、運用、管理を行うための技術的な専門知識が検証されます。AWS 認定 デベロッパー – アソシエイト、または AWS 認定 SysOps アドミニストレーター – アソシエイトレベルの知識を有する、経験豊富なアーキテクトを対象としています。",
        long: "このプロフェッショナル認定では、AWS で分散アプリケーションシステムのプロビジョニング、運用、管理を行うための技術的な専門知識が検証されます。AWS 認定 デベロッパー – アソシエイト、または AWS 認定 SysOps アドミニストレーター – アソシエイトレベルの知識を有する、経験豊富なアーキテクトを対象としています。"
    }
}, {
    type: "choices-networking",
    text: {
        headline: "AWS 認定高度なネットワーク – 専門知識",
        link: "https://aws.amazon.com/certification/",
        desc: "この専門知識認定では、大規模な AWS およびハイブリッド IT ネットワークアーキテクチャの設計と実装を行う高度な技術スキルが検証されます。",
        long: "この専門知識認定では、大規模な AWS およびハイブリッド IT ネットワークアーキテクチャの設計と実装を行う高度な技術スキルが検証されます。"
    }
}, {
    type: "choices-data",
    text: {
        headline: "AWS 認定ビッグデータ – 専門知識",
        link: "https://aws.amazon.com/certification/",
        desc: "この専門知識認定では、データから価値を引き出すために AWS のサービスを設計および実装する技術的スキルと経験が検証されます。",
        long: "この専門知識認定では、データから価値を引き出すために AWS のサービスを設計および実装する技術的スキルと経験が検証されます。"
    }
}, {
    type: "choices-security",
    text: {
        headline: "AWS 認定セキュリティ – 専門知識",
        link: "https://aws.amazon.com/certification/",
        desc: "この専門知識認定では、AWS でセキュリティを確保するための知識、およびインシデント対応、ロギングとモニタリング、インフラストラクチャのセキュリティ、アイデンティティとアクセス管理、データ保護に関連した技術スキルについて検証されます。",
        long: "この専門知識認定では、AWS でセキュリティを確保するための知識、およびインシデント対応、ロギングとモニタリング、インフラストラクチャのセキュリティ、アイデンティティとアクセス管理、データ保護に関連した技術スキルについて検証されます。"
    }
}];

var all_questions = [{

    question_string: "AWS の経験についてお聞かせください",
    choices: {
        q1: {
            cloud: "AWS プロジェクトで、1 年未満の期間、管理、リーダー、テスト、その他の非コーディング業務に携わったことがある"
        },
        q2: {
            archassoc: "複数の AWS プロジェクトに、アーキテクト/設計者として携わったことがある"
        },
        q3: {
            sysops: "AWS における複数のプロジェクトのデプロイに携わったことがある"
        },
        q4: {
            dev: "AWS における複数のプロジェクトの構築と開発に携わったことがある"
        },
        q5: {
            none: "AWS の使用経験がない"
        }
    },
    type: "checkbox"

}, {
    question_string: "どのような役割を目指していますか",
    choices: {
        q1: {
            archassoc: "クラウドセキュリティエンジニア"
        },
        q2: {
            dev: "ネットワークエンジニア"
        },
        q3: {
            cloud: "プロジェクトマネージャー"
        },
        q4: {
            archassoc: "データサイエンティスト"
        },
        q5: {
            dev: "エントリレベルの IT"
        },
        q6: {
            cloud: "リーダーシップ/マネージメント"
        },
        q7: {
            archassoc: "ソリューションアーキテクト"
        },
        q8: {
            archassoc: "エンタープライズアーキテクト"
        },
        q9: {
            archassoc: "テクニカルアーキテクト"
        },
        q10: {
            archassoc: "アプリケーションアーキテクト"
        },
        q11: {
            dev: "開発者"
        },
        q12: {
            dev: "ソフトウェアエンジニア"
        },
        q13: {
            dev: "コーダー"
        },
        q14: {
            sysops: "SysOps"
        },
        q15: {
            sysops: "SecOps"
        },
        q16: {
            devops: "DevOps"
        },
        q17: {
            sysops: "システムオペレーター"
        },
        q18: {
            sysops: "システム管理者"
        }
    },
    type: "checkbox"
}, {
    question_string: "これまでにどの AWS 認定を取得しましたか?",
    choices: {
        q1: {
            archassoc: "AWS認定 クラウドプラクティショナー"
        },
        q2: {
            archpro: "AWS認定 ソリューションアーキテクト – アソシエイト"
        },
        q3: {
            devops: "AWS認定 デベロッパー – アソシエイト"
        },
        q4: {
            devops: "AWS認定 SysOps アドミニストレーター – アソシエイト"
        },
        q5: {
            proSpec1: "AWS認定 ソリューションアーキテクト – プロフェッショナル"
        },
        q6: {
            proSpec2: "AWS認定 DevOps エンジニア – プロフェッショナル"
        },
        q7: {
            spec: "AWS認定 高度なネットワーク – 専門知識"
        },
        q8: {
            spec: "AWS認定 ビッグデータ – 専門知識"
        },
        q9: {
            spec: "AWS認定 セキュリティ – 専門知識"
        },
        q10: {
            all: "上記すべて"
        },
        q11: {
            none: "取得していない"
        }
    },
    type: "checkbox"
}, {
    question_string: "クラウドに関する知識習得の目標は何ですか?",
    choices: {
        q1: {
            dev: "クラウドに堅牢で革新的なアプリケーションを構築したい"
        },
        q2: {
            archassoc: "さまざまな負荷や顧客需要に合わせてスケールするシステムを構築し、デプロイしたい"
        },
        q3: {
            archassoc: "自分が構築し、デプロイするシステムに障害が発生しないようにしたい"
        },
        q4: {
            archassoc: "冗長性のある本番アプリケーションのデプロイ方法を知りたい"
        },
        q5: {
            archassoc: "セキュリティ関連の問題について社内の第一人者になりたい"
        },
        q6: {
            spec: "データから情報に基づいた結論を引き出す方法を学習したい"
        },
        q7: {
            spec: "アクセスログを解釈する方法や、機密データへのアクセス制御を改善する方法を学習したい"
        },
        q8: {
            sysops: "オンプレミスアプリケーションをクラウドに移行できるようになりたい"
        },
        q9: {
            sysops: "CI/CD 開発環境を効果的に設定したい"
        },
        q10: {
            spec: "システムを DDoS および EDoS から保護できるよう設定したい"
        },
        q11: {
            archassoc: "ビジネスでより良い意思決定ができるよう、基本的なクラウドアーキテクチャと主要なサービスについて理解したい"
        },
        q12: {
            other: "その他"
        }
    },
    type: "checkbox"
}, {
    question_string: "どの AWS トピックに関心がありますか?",
    choices: {
        q1: {
            spec: "オンプレミスのデータセンターにおける要素と AWS の構成要素を組み合わせたハイブリッドシステムをデプロイしたい"
        },
        q2: {
            archassoc: "AWS の主要なサービス (データベース、通知、ストレージ、ワークフロー、変更管理といったサービス) を使用するアプリケーションについての実践的な知識を身に付けたい"
        },
        q3: {
            dev: "AWS で安全性と信頼性の高いアプリケーションの設計、開発、デプロイ、保守を行う方法を知りたい"
        },
        q4: {
            spec: "NACL/ACL、ファイアウォール、ネットワークゲートウェイ、VPN、キー管理、SSL、HTTPS、SAML、トークン、証明書などの設定方法を学習したい"
        },
        q5: {
            blank: "上記のいずれでもない"
        }
    },
    type: "checkbox"
}];

var Quiz = function(quiz_name) {
    this.quiz_name = quiz_name;
    this.questions = [];
}

// Function to add a question to the Quiz object
Quiz.prototype.add_question = function(question) {
    // Randomly choose where to add question
    // var index_to_add_question = Math.floor(Math.random() * this.questions.length);

    var index_to_add_question = this.questions.length;
    this.questions.splice(index_to_add_question, 0, question);
}

Quiz.prototype.render = function(container) {
    var self = this;
    var $submit = $('#submit-button'), $prev = $('#prev-question-button'), $next = $('#next-question-button');

    $('#quiz-results').hide();

    var question_container = $('<div>').attr('id', 'question').insertAfter('#quiz-name');
    function change_question(direction) {
        var dir = direction;
        self.questions[current_question_index].render(question_container, dir);
        // console.log(self);
        moveIn();
        $prev.prop('disabled', current_question_index === 0)
        $prev.toggleClass('btnHidden', current_question_index === 0);
        $next.prop('disabled', self.questions[current_question_index].user_choice_index === null);

        // Determine if all questions have been answered
        var all_questions_answered = true;
        for(var i = 0; i < self.questions.length; i++) {
            if(self.questions[i].user_choice_index === null) {
                all_questions_answered = false;
                break;
            }
        }
        if(all_questions_answered && current_question_index == self.questions.length - 1) {
            $next.toggleClass('btnHidden');
            $submit.prop('enable')
            $submit.toggleClass('btnHidden');
        } else {
            $next.removeClass('btnHidden');
            $submit.prop('disabled');
            $submit.addClass('btnHidden');
        }
    }

    function change_page_number() {
        var curPage = parseInt(self.questions.indexOf(self.questions[current_question_index])) + 1;
        var totalPages = self.questions.length;
        var numbers = curPage + "/" + totalPages;
        $('#pageNumber').text(numbers);
    }

    // Render the first question
    var current_question_index = 0;
    change_question();
    change_page_number();
    // Add listener for the previous question button
    $prev.on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        if(current_question_index > 0) {
            current_question_index--;
            userScore.pop();
            change_question('prev');
            change_page_number();
        }
    });
    // Add listener for the next question button
    $next.on('click', function() {
        var body = $(document).height();

        //console.log(body);
        if(current_question_index < self.questions.length - 1 && current_question_index !== undefined) {
            current_question_index++;
            userScore.push(pushScore($('#question input[name=choices]:checked')));
            // console.log(userScore);

            moveOut();

            change_page_number();
            // $('#body').height(body);
        }
    });

    function pushScore(vals) {
        var curScore, checkScore = [], $this = vals;
        if($this.attr('type') == 'radio') {
            curScore = $this.val();
            // console.log(curScore);
            return curScore;
        } else {
            for(var i = 0; i < $this.length; i++) {
                if($this[i].value == "choices-other") {
                    break;
                }
                if($this[i].value == "choices-blank") {
                    break;
                }
                checkScore.push($this[i].value);
            }
            // console.log(checkScore);
            return curScore = checkScore;
        }

    }

    function submitMkto() {
        MktoForms2.whenReady(function(form) {
            form.submit();
        });
    }

    // Add listener for the submit button
    $submit.on('click', function() {
        userScore.push(pushScore($('#question input[name=choices]:checked')));
        // console.log(userScore);
        var disp = resultsDisplay(occurrence(userScore));
        $('#quiz-results').prepend(disp);
        submitMkto();
        $('body').toggleClass('bodyResults');
        $('.questionsHold').toggleClass('btnHidden');
        $('.resultsFooter').toggleClass('btnHidden');
        $('#quiz-results').css('opacity', 0).slideDown(1000).animate({ opacity: 1}, { queue: false, duration: 2000});
        $('#quiz button').slideUp();
    });

    // Add a listener on the questions container to listen for user select changes
    // This is for determining whether we can submit answers of not.
    question_container.bind('user-select-change', function() {
        var all_questions_answered = true;
        for(var i = 0; i < self.questions.length; i++) {
            if(self.questions[i].user_choice_index === null) {
                all_questions_answered = false;
                break;
            }
            //console.log(self);
        }

        if(all_questions_answered && current_question_index == self.questions.length - 1) {
            //console.log('the end');
            $next.addClass('btnHidden');
            $submit.prop('enable')
            $submit.removeClass('btnHidden');
        } else {
            //console.log('where am I');
            $next.removeClass('btnHidden');
            $submit.prop('disabled');
            $submit.addClass('btnHidden');
        }
    });

    function moveOut() {
        var $container = $('#question').children();
        var setback = ($container.length) * 30;
        $container.each(function(index, element) {
            setTimeout(function() {
                $(element).removeClass(function (index, css) {
                    return (css.match (/\bmoveIn-\S+/g) || []).join(' ');
                });
                $(element).addClass('moveOut');
            }, ( index * 30 ));
        });

        setTimeout(function() {
            $('.buttonHolder').removeClass(function (index, css) {
                    return (css.match (/\bmoveIn-\S+/g) || []).join(' ');
                }).addClass('moveOut');
        }, (setback));
        setTimeout(function() {
            change_question();
        }, (setback + 445));
    }

    function moveIn() {
        var $container = $('#question').children();
        var timer = ($container.length) / 2 * 30;
        var setback = ($container.length + 1) / 2;

        $('#question h3').addClass('moveIn-header');
        setTimeout(function() {
            $('.buttonHolder').removeClass('moveOut');
            $('.buttonHolder button, .buttonHolder a').css({ opacity : 0.8 });
            $('.buttonHolder').addClass('moveIn-q' + setback);
        }, timer);
    }
}// end Quiz.prototype.render

var ScoreKeeper = function(title, score, answer) {
    this.title  = title,
    this.score  = score,
    this.answer = answer;
}
var userScore = [];
var occurrence = function(score) {
    var countedSpecs = function(choices) {
        var specNum = 0
        for(i = 0; i < choices.length; i++) {
            if(choices[i] == 'choices-spec') {
                specNum++;
            }
        }
        return specNum;
    }

    var result, item = [], tempCount = 0, suggestion, flat, level, autoRec;
    // console.log(score);
    if(score[2].includes('choices-archpro') || score[2].includes('choices-devops') ) {
        level = 'pro';
    }
    if(score[2].includes('choices-spec') && score[2].includes('choices-archpro') || score[2].includes('choices-spec') && score[2].includes('choices-devops') ) {
        autoRec = 'specs';
    }

    if(countedSpecs(score[2]) == 3) {
        if(score[2].includes('choices-proSpec1') && score[2].includes('choices-proSpec2')) {
            autoRec = 'all';
        } else if(score[2].includes('choices-proSpec1')) {
            autoRec = 'choices-proSpec2';
        } else if(score[2].includes('choices-proSpec2')) {
            autoRec = 'choices-proSpec1';
        } else if(level == null) {
            autoRec = 'all';
        }
    }

    flat = [].concat.apply([], score);
    // console.log(flat);
    result = flat.reduce(function(prev, next) {
        prev[next] = (prev[next] + 1) || 1;
        return prev;
    },{});
    // console.log("level: " + level);
    // console.log("autoRec: " + autoRec);
    // console.log(result);
    for(var k in result) {
        // console.log('k: ' + k);
        if(k == 'choices-all') {
            // console.log('inside the if');
            item.pop();
            item.unshift(k);
            // console.log('item is: ' + item);
            break;
        }
        if(k == 'choices-devops' && item == 'choices-dev') {
            // console.log('inside DevOps');
            item.pop();
            item.unshift(k);
            // console.log('new k item is: ' + item);
        }
        if(result[k] > tempCount) {
            item.pop();
            tempCount = result[k];
            item.unshift(k);
        }
        // console.log('item: ' + item);
    }
    suggestion = item[0];
    if(suggestion == 'choices-archassoc' && level == 'pro') {
        suggestion = 'choices-archpro';
    } else if(suggestion == 'choices-devops' || suggestion == 'choices-sysops' && level == 'pro') {
        suggestion = 'choices-devops';
    }
    if(autoRec == 'specs') {
        suggestion = 'choices-spec';
    }
    if(autoRec == 'all') {
        suggestion = 'choices-all';
    }
    if(autoRec == 'choices-proSpec1') {
        suggestion = 'choices-archpro';
    }
    if(autoRec == 'choices-proSpec2') {
        suggestion = 'choices-devops';
    }
    // console.log(suggestion);
    return suggestion;
};

var resultsDisplay = function(choice) {
    var classInfo = all_results, display = "", mainClass = "", otherClasses = "";
    // console.log(classInfo);
    // console.log('choice is: ' + choice);
    if(choice == 'choices-none') {
        choice = 'choices-cloud';
    }
    if(choice == 'choices-arch') {
        choice = 'choices-archassoc';
    }
    display += "<div class='resultsHeader'><span class='shapeHolderLeftResults shapeResults'></span>";
    mainClass += "<div class='innerResults'>";
    mainClass += "<div class='innerHolder'>";
    if(choice == "choices-spec") {
        mainClass += "<h2>AWS 専門知識認定</h2>";
        mainClass += "<p>これらの上級認定では、特定の技術分野における高度なスキルが検証されます。専門知識認定は、有効なアソシエイトレベルの認定に相当する知識を持つ個人を対象としています。</p>";
        // mainClass += "<h2>AWS Specialty Certifications</h2>";
        // mainClass += "<p>These certifications validate your advanced skills in specific technical areas. They are for individuals who have achieved AWS Certified Cloud Practitioner or have an active Associate-level certification.</p>";
    } else if(choice == "choices-all") {
        mainClass += "<h2>AWS 認定の取得を継続する</h2>";
        mainClass += "<p>上級 AWS 認定で AWS スキルを検証した後は、技術的な知識を広げていくことができます。新しい専門知識認定や、再認定試験のお知らせにご注意ください。</p>";
    } else {
        mainClass += "<h2>お客様の認定パス</h2>";
        mainClass += "<p>回答に基づき、以下の認定の準備を始めることをお勧めします。</p>";
    }
    mainClass += "<span class='shapeHolderRightResults shapeResults'></span></div></div></div>";
    for(var i = 0; i < classInfo.length; i++) {
        if(choice == 'choices-all') {
            mainClass += "<div class='mainClass thanksResults'><div class='innerResults'>";
            break;
        }
        if(choice == 'choices-spec') {
            mainClass += "<div class='mainClass specResults'><div class='innerResults'>";
            mainClass += "<article>"
            mainClass += "<img src='./img/icons/aws-certificate-icon-" + classInfo[6].type + ".svg' />";
            mainClass += "<div class='mainText'>"
            mainClass += "<h3>" + classInfo[6].text.headline + "</h3>";
            mainClass += "<p>" + classInfo[6].text.long + "</p>";
            mainClass += "</article>";
            mainClass += "<article>"
            mainClass += "<img src='./img/icons/aws-certificate-icon-" + classInfo[7].type + ".svg' />";
            mainClass += "<div class='mainText'>"
            mainClass += "<h3>" + classInfo[7].text.headline + "</h3>";
            mainClass += "<p>" + classInfo[7].text.long + "</p>";
            mainClass += "</article>";
            mainClass += "<article>"
            mainClass += "<img src='./img/icons/aws-certificate-icon-" + classInfo[8].type + ".svg' />";
            mainClass += "<div class='mainText'>"
            mainClass += "<h3>" + classInfo[8].text.headline + "</h3>";
            mainClass += "<p>" + classInfo[8].text.long + "</p>";
            mainClass += "</article>";
            mainClass += "<p class='specLearnMore'>今後リリースされる新しい専門知識認定にもご注目ください。</p>";
            mainClass += "<a class='ctaButton' href='" + classInfo[6].text.link + "' target='_blank'>詳細を見る</a>";
            mainClass += "</div>";
            break;
        }
        else if(choice == classInfo[i].type) {
            mainClass += "<div class='mainClass'><div class='innerResults'>";
            mainClass += "<img src='./img/icons/aws-certificate-icon-" + classInfo[i].type + ".svg' />";
            mainClass += "<div class='mainText'>"
            mainClass += "<h3>" + classInfo[i].text.headline + "</h3>";
            mainClass += "<p>" + classInfo[i].text.long + "</p>";
            mainClass += "<a class='ctaButton' href='" + classInfo[i].text.link + "' target='_blank'>詳細を見る</a>";
            mainClass += "</div>";
        }
    }
     mainClass += "</div></div>";
    display += mainClass;
    display += "<div class='otherClassesContainer'><p>ハイレベルな専門知識が要求される役割ベースの認定パス 4 種類と、専門知識認定が用意されています。<a href='https://aws.amazon.com/certification/'>AWS 認定のオプションすべてを見る</a></p></div>";
    return display;
};

// object for a Question. Constructor
var Question = function(question_string, choices, type) {
    this.question_string   = question_string;
    this.choices           = [];
    this.user_choice_index = null; //Index of the user's choice selection
    this.type              = type;

    // TODO - fill choices[] with choices and do something with it
    var number_of_choices = choices;
    for(var key in number_of_choices) {
        var obj = number_of_choices[key];
        for(var prop in obj) {
            var quest = new ScoreKeeper(key, prop, obj[prop]);
            this.choices.push(quest);
        }
    }
}// end Question

var makeRadio = function(curRadio, appendTo, index) {
    var choice_radio_button = $('<input>')
        .attr('id', 'choices-' + curRadio.choices[index].title)
        .attr('type', 'radio')
        .attr('name', 'choices')
        .attr('data-answer', this.choices[i].answer)
        .attr('value', 'choices-' + curRadio.choices[index].score)
        .attr('checked', index === (curRadio.user_choice_index - 1))
        .appendTo(appendTo);
}

var makeLabel = function(curRadio, appendTo, index, size) {
    var choice_label = $('<label>')
        .text(curRadio.choices[index].answer)
        .attr('for', 'choices-' + curRadio.choices[index].title)
        .attr('class', 'moveIn-' + curRadio.choices[index].title + ' ' + size)
        .appendTo(appendTo);
}

// function that will render the questions inside the container
Question.prototype.render = function(container, direction) {
    var self = this;
    // console.log(self);
    var dir = direction;
    // Fill out the question label
    var question_string_h3;
    var question_string_hint;
    if(container.children('h3').length === 0) {
        question_string_h3 = $('<h3>').appendTo(container);
        // console.log('inside the if');
    } else {
        question_string_h3 = container.children('h3').first();
        // console.log('inside the else');
    }
    question_string_h3.text(this.question_string);
    question_string_hint = $('<span class="hintText">').appendTo(question_string_h3);
    if(this.type == "checkbox") {
        question_string_hint.text('(当てはまるものをすべて選択してください)');
    } else if(this.type == "radio") {
        question_string_hint.text('(Choose one)');
    }
    // Clear any radio buttons and create new ones
    if(container.children('input[type=radio]').length > 0) {
        container.children('input[type=radio]').each(function() {
            var radio_button_id = $(this).attr('id');
            $(this).remove();
            container.children('label[for=' + radio_button_id + ']').remove();
        });
    }
    if(container.children('input[type=checkbox]').length > 0 ) {
        container.children('input[type=checkbox]').each(function() {
            var checkbox_id = $(this).attr('id');
            $(this).remove();
            container.children('label[for=' + checkbox_id + ']').remove();
        });
    }
    // console.log(this.choices.length);
    for(var i = 0; i < this.choices.length; i++) {
        //console.log("i: " + i);
        if(this.type == "checkbox") {
            // Create the checkbox
            var choice_radio_button = $('<input>')
                .attr('id', 'choices-' + this.choices[i].title)
                .attr('type', 'checkbox')
                .attr('name', 'choices')
                .attr('data-answer', this.choices[i].answer)
                .attr('value', 'choices-' + this.choices[i].score);
                //.appendTo(container);
            if(this.user_choice_index !== null) {
                // console.log("i : " + i);
                // console.log("userchoiceindex: " + this.user_choice_index);
                choice_radio_button.attr('checked', this.user_choice_index.includes(i + 1))
                    .appendTo(container);
            } else {
                choice_radio_button.appendTo(container);
            }
            if(this.choices[i].score == 'all') {
                choice_radio_button.addClass('preventMore');
            }
        // } else if(this.type == "radio" && this.choices.length > 9) {
        //         // Create the radio button
        //         makeRadio(this, container, i);
        } else {
            // Create the radio button
            makeRadio(this, container, i);
        }
        // Create the label
        if(dir == 'prev') {
            if(this.type == "checkbox" && this.choices.length > 9) {
                var choice_label = $('<label>')
                    .text(this.choices[i].answer)
                    .attr('for', 'choices-' + this.choices[i].title)
                    .attr('class', 'moveIn-place halfSize')
                    .appendTo(container);
            } else {
                var choice_label = $('<label>')
                    .text(this.choices[i].answer)
                    .attr('for', 'choices-' + this.choices[i].title)
                    .attr('class', 'moveIn-place')
                    .appendTo(container);
            }

        } else if(this.type == "checkbox" && this.choices.length > 9) {
                makeLabel(this, container, i, 'halfSize');
                // makeLabel(this, container, i, 'fullSize');
        } else {
            makeLabel(this, container, i, 'fullSize');
        }
    } // end for loop

    var checkboxAnswerIndex = [];
    // Add a listener for the radio button to change which on the user has clicked on
    $('input[name=choices]').change(function(index) {
        //var selected_radio_button_value = $('input[name=choices]:checked').val();
        // var answerIndex = $('input[name=choices]:checked').attr('id');
        var answerIndex = $(this).attr('id');
        // console.log(checkboxAnswerIndex);
        if($(this).attr('type') == 'checkbox') {
            // console.log("answerIndex : " + answerIndex);
            // console.log($(this).prop('checked'));
            // console.log($(this));
            // console.log($(this)[0].value);
            if($(this)[0].value == 'choices-all' || $(this)[0].value == 'choices-none' || $(this)[0].value == 'choices-blank') {
                // console.log(this.id);
                var $ignore = $("label[for='" + this.id + "']");
                $('input[name=choices] + label').not($ignore).toggleClass('preventClick');
                $('input[name=choices]').not(this).prop('checked', false);
            }
            if(checkboxAnswerIndex.includes(parseInt(answerIndex.substring(9)))) {
                var i = checkboxAnswerIndex.indexOf(parseInt(answerIndex.substring(9)));
                if(i != -1) {
                    checkboxAnswerIndex.splice(i, 1);
                }
                // checkboxAnswerIndex = checkboxAnswerIndex.filter(function(item) {
                //     return item !== answerIndex;
                // });
            } else {
                checkboxAnswerIndex.push(parseInt(answerIndex.substr(9)));
            }
            //console.log('checkboxindes: ' + checkboxAnswerIndex);
        }
        // console.log(checkboxAnswerIndex);
        // console.log($(this).attr('id'));
        // console.log(answerIndex);
        if($('#next-question-button').prop('disabled')) {
            $('#next-question-button').prop('disabled', false);
        }
        // Change the user choice index
        if($(this).attr('type') == 'checkbox') {
            self.user_choice_index = checkboxAnswerIndex;
        } else {
            self.user_choice_index = parseInt(answerIndex.substr(answerIndex.length - 1, 1));
        }


        // Trigger a user-select-change
        container.trigger('user-select-change');
    });
}// end Question.prototype.render


$(document).ready(function() {
    // Create an instance of the Quiz object
    var quiz = new Quiz('Modality');
    // Create the Question objects from all_questions
    for(var i = 0; i < all_questions.length; i++) {
        var question = new Question(all_questions[i].question_string, all_questions[i].choices, all_questions[i].type);
        // Add the question to the instance of the Quiz object
        quiz.add_question(question);
    }

    // Render the quiz
    var quiz_container = $('.quizContainer');
    quiz.render(quiz_container);

    $('.pageStart').on('click', 'a', function() {
        $('.pageStart').addClass('noHeight');
        var $container = $('.pageStart').children();
        //$('#pageNumber').fadeToggle();
        $container.each(function(index, element) {
            setTimeout(function() {
                $(element).addClass('moveOut');
            }, 100 + ( index * 100 ));
        });

        $('.quizContainer').toggleClass('btnHidden');
        $('.questionsHold').addClass('show');
    });
});


})(jQuery);
























