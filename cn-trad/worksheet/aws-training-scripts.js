(function($) {

// new categories
// hands
// class
// digital
// exam

var all_results = [{
    type: "choices-class",
    text: {
        headline: "課堂培訓",
        link: "https://aws.amazon.com/training/course-descriptions/",
        desc: "由取得認證且擁有深度技術專業的講師帶領的面授、虛擬和私人現場培訓",
        long: "無論是實體或虛擬課堂，這些課程都能為想要有更紮實技術能力的人員提供更深入的培訓。課程內容包含簡報、實作實驗室，以及由各領域專家帶領的團體討論。"
    }
}, {
    type: "choices-digital",
    text: {
        headline: "數位培訓",
        link: "https://aws.amazon.com/training/course-descriptions/",
        desc: "免費的隨需數位課程可協助您以自己的步調強化知識",
        long: "只需要一些時間和計劃，您就能透過隨需線上培訓增進實用的雲端知識。這些線上學習課程的時間從 1 小時到 1 天不等，可協助您對特定主題有更廣泛的了解。"
    }
}, {
    type: "choices-hands",
    text: {
        headline: "實作體驗",
        link: "https://aws.amazon.com/training/course-descriptions/cloud-practitioner-essentials/",
        desc: "建議您在實際雲端環境進行 3-6 個月的實作，為您的學習打好基礎",
        long: "實際上團隊合作才是最好的培訓類型，您可在此過程獲得真實雲端環境中開發和操作所需的實用知識。3 到 6 個月的實作體驗將為您的工作知識打好基礎。"
    }
}, {
    type: "choices-exam",
    text: {
        headline: "自學研讀",
        link: "https://www.youtube.com/user/AWSwebinars/featured",
        desc: "獨立學習可讓您獲得更完整的知識，並依照自己的步調學習新的主題",
        long: "獨立學習可讓您獲得更完整的知識，並依照自己的步調學習新的主題。對於想要深入了解特定技術主題的 IT 專業人員，我們提供多種白皮書、部落格文章、影片、網路研討會、使用案例和同儕資源等。"
    }
}];

var all_questions = [{

    question_string: "您的雲端之旅進度為何？選擇所有適用的選項。",
    choices: {
        q1: {
            class: "我擁有 AWS 雲端經驗"
        },
        q2: {
            digital: "我是擁有現場部署經驗的 IT 專業人員"
        },
        q3: {
            hands: "我不是 IT 專業人員，也沒有雲端經驗"
        },
        q4: {
            class: "我有與其他雲端供應商合作的經驗"
        },
        q5: {
            digital: "我不是 IT 專業人員，但擁有雲端經驗"
        },
        q6: {
            digital: "我剛畢業，初次進入就業市場"
        }
    },
    type: "checkbox"

}, {
    question_string: "您需要快速接受培訓以達成工作目標，還是正在進行自主進度專業開發之旅？",
    choices: {
        q1: {
            digital: "我需要快速接受培訓"
        },
        q2: {
            digital: "我的時間很有彈性"
        }
    },
    type: "radio"
}, {
    question_string: "每個人都不一樣。您偏好的學習方式為何？選擇所有適用的選項。",
    choices: {
        q1: {
            class: "我最適合結構化課程學習"
        },
        q2: {
            class: "我想詢問 AWS 雲端專家問題"
        },
        q3: {
            digital: "我喜歡進行試驗，嘗試後失敗對我來說不是問題"
        },
        q4: {
            digital: "我是自學學生，最適合從多種選擇進行學習"
        },
        q5: {
            hands: "我最適合透過閱讀學習"
        },
        q6: {
            digital: "我偏好影片和視覺化簡報"
        },
        q7: {
            hands: "我喜歡透過實作學習，需要實際的雲端經驗"
        }
    },
    type: "checkbox"
}, {
    question_string: "您的培訓方式為個人或團體培訓？",
    choices: {
        q1: {
            class: "我與一個小團體或團隊一起培訓"
        },
        q2: {
            class: "我與整個團隊 (或部門或大團體) 一起培訓"
        },
        q3: {
            digital: "我專注在個人開發"
        }
    },
    type: "radio"
}, {
    question_string: "誰會支付您的培訓費用？",
    choices: {
        q1: {
            class: "公司會支付我的培訓費用"
        },
        q2: {
            digital: "我沒有預算或預算很少"
        },
        q3: {
            class: "我支付自己的所有培訓費用"
        },
        q4: {
            digital: "我支付部分培訓費用"
        },
        q5: {
            hands: "我沒有已預先核准的預算，但我的團體可能有興趣接受進一步的培訓"
        },
        q6: {
            class: "我有已預先核准的預算"
        }
    },
    type: "radio"
}];

var Quiz = function(quiz_name) {
    this.quiz_name = quiz_name;
    this.questions = [];
}

// Function to add a question to the Quiz object
Quiz.prototype.add_question = function(question) {
    // Randomly choose where to add question
    // var index_to_add_question = Math.floor(Math.random() * this.questions.length);

    var index_to_add_question = this.questions.length;
    this.questions.splice(index_to_add_question, 0, question);
}

Quiz.prototype.render = function(container) {
    var self = this;
    var $submit = $('#submit-button'), $prev = $('#prev-question-button'), $next = $('#next-question-button');

    $('#quiz-results').hide();

    var question_container = $('<div>').attr('id', 'question').insertAfter('#quiz-name');
    function change_question(direction) {
        var dir = direction;
        self.questions[current_question_index].render(question_container, dir);
        moveIn();
        $prev.prop('disabled', current_question_index === 0)
        $prev.toggleClass('btnHidden', current_question_index === 0);
        $next.prop('disabled', self.questions[current_question_index].user_choice_index === null);

        // Determine if all questions have been answered
        var all_questions_answered = true;
        for(var i = 0; i < self.questions.length; i++) {
            if(self.questions[i].user_choice_index === null) {
                all_questions_answered = false;
                break;
            }
        }
        if(all_questions_answered && current_question_index == self.questions.length - 1) {
            $next.toggleClass('btnHidden');
            $submit.prop('enable')
            $submit.toggleClass('btnHidden');
        } else {
            $next.removeClass('btnHidden');
            $submit.prop('disabled');
            $submit.addClass('btnHidden');
        }
    }

    function change_page_number() {
        var curPage = parseInt(self.questions.indexOf(self.questions[current_question_index])) + 1;
        var totalPages = self.questions.length;
        var numbers = curPage + "/" + totalPages;
        $('#pageNumber').text(numbers);
    }

    // Render the first question
    var current_question_index = 0;
    change_question();
    change_page_number();
    // Add listener for the previous question button
    $prev.on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        if(current_question_index > 0) {
            current_question_index--;
            userScore.pop();
            change_question('prev');
            change_page_number();
        }
    });
    // Add listener for the next question button
    $next.on('click', function() {
        if(current_question_index < self.questions.length - 1 && current_question_index !== undefined) {
            current_question_index++;
            userScore.push(pushScore($('#question input[name=choices]:checked')));
            // console.log(userScore);
            moveOut();
            change_page_number();
        }
    });

    function pushScore(vals) {
        var curScore, checkScore = [], $this = vals;
        if($this.attr('type') == 'radio') {
            curScore = $this.val();
            //console.log(curScore);
            return curScore;
        } else {
            for(var i = 0; i < $this.length; i++) {
                checkScore.push($this[i].value);
            }
            //console.log(checkScore);
            return curScore = checkScore;
        }

    }

    function submitMkto() {
        MktoForms2.whenReady(function(form) {
            form.submit();
        });
    }

    // Add listener for the submit button
    $submit.on('click', function() {
        userScore.push(pushScore($('#question input[name=choices]:checked')));
        var disp = resultsDisplay(occurrence(userScore));
        $('#quiz-results').prepend(disp);
        // submitMkto();
        $('body').toggleClass('bodyResults');
        $('.questionsHold').toggleClass('btnHidden');
        $('.resultsFooter').toggleClass('btnHidden');
        $('#quiz-results').css('opacity', 0).slideDown(1000).animate({ opacity: 1}, { queue: false, duration: 2000});
        $('#quiz button').slideUp();
    });

    // Add a listener on the questions container to listen for user select changes
    // This is for determining whether we can submit answers of not.
    question_container.bind('user-select-change', function() {
        var all_questions_answered = true;
        for(var i = 0; i < self.questions.length; i++) {
            if(self.questions[i].user_choice_index === null) {
                all_questions_answered = false;
                break;
            }
        }

        if(all_questions_answered && current_question_index == self.questions.length - 1) {
            //console.log('the end');
            $next.addClass('btnHidden');
            $submit.prop('enable')
            $submit.removeClass('btnHidden');
        } else {
            //console.log('where am I');
            $next.removeClass('btnHidden');
            $submit.prop('disabled');
            $submit.addClass('btnHidden');
        }
    });

    function moveOut() {
        var $container = $('#question').children();
        var setback = ($container.length) * 30;
        $container.each(function(index, element) {
            setTimeout(function() {
                $(element).removeClass(function (index, css) {
                    return (css.match (/\bmoveIn-\S+/g) || []).join(' ');
                });
                $(element).addClass('moveOut');
            }, ( index * 30 ));
        });

        setTimeout(function() {
            $('.buttonHolder').removeClass(function (index, css) {
                    return (css.match (/\bmoveIn-\S+/g) || []).join(' ');
                }).addClass('moveOut');
        }, (setback));
        setTimeout(function() {
            change_question();
        }, (setback + 445));
    }

    function moveIn() {
        var $container = $('#question').children();
        var timer = ($container.length) / 2 * 30;
        var setback = ($container.length + 1) / 2;

        $('#question h3').addClass('moveIn-header');
        setTimeout(function() {
            $('.buttonHolder').removeClass('moveOut');
            $('.buttonHolder button, .buttonHolder a').css({ opacity : 1 });
            $('.buttonHolder').addClass('moveIn-q' + setback);
        }, timer);
    }
}// end Quiz.prototype.render

var ScoreKeeper = function(title, score, answer) {
    this.title  = title,
    this.score  = score,
    this.answer = answer
}
var userScore = [];
var occurrence = function(score) {
    var result, item = [], tempCount = 0, suggestion, flat;
    flat = [].concat.apply([], score);

    result = flat.reduce(function(prev, next) {
        prev[next] = (prev[next] + 1) || 1;
        return prev;
    },{});
    for(var k in result) {
        if(result[k] > tempCount) {
            item.pop();
            tempCount = result[k];
            item.unshift(k);
        }
    }
    suggestion = item[0];
    return suggestion;
};

var resultsDisplay = function(choice) {
    var classInfo = all_results, display = "", mainClass = "", otherClasses = "";
    //console.log('choice is: ' + choice);

    for(var i = 0; i < classInfo.length; i++) {
        //console.log(classInfo[i].type);
        if(choice == classInfo[i].type) {
            display += "<h2>根據您的獨特偏好和各項因素，我們建議以下培訓類型 <span>" + classInfo[i].text.headline + "</span></h2>";
            mainClass += "<div class='mainClass'>";
            mainClass += "<img src='./img/aws-training-icon-" + classInfo[i].type + ".svg' />";
            mainClass += "<div class='mainText'>"
            mainClass += "<h3>" + classInfo[i].text.headline + "</h3>";
            mainClass += "<p>" + classInfo[i].text.long + "</p>";
            mainClass += "<a href='" + classInfo[i].text.link + "' target='_blank'>進一步了解</a>";
            mainClass += "</div>";
            mainClass += "</div>";
        } else {
            otherClasses += "<article class='otherClass'>";
            otherClasses += "<img src='./img/aws-training-icon-" + classInfo[i].type + ".svg' />";
            otherClasses += "<div><h5>" + classInfo[i].text.headline + "</h5><p>" + classInfo[i].text.desc + "</p><a href='" + classInfo[i].text.link + "' target='_blank'>進一步了解</a>";
            otherClasses += "</article>";
        }
    }
    display += mainClass;
    display += "<div class='otherClassesContainer'><h4>透過以下其他培訓類型，讓您的學習更加完整：</h4>" + otherClasses + "</div>";
    return display;
};

// object for a Question. Constructor
var Question = function(question_string, choices, type) {
    this.question_string   = question_string;
    this.choices           = [];
    this.user_choice_index = null; //Index of the user's choice selection
    this.type              = type;


    // TODO - fill choices[] with choices and do something with it
    var number_of_choices = choices;
    for(var key in number_of_choices) {
        var obj = number_of_choices[key];
        for(var prop in obj) {
            var quest = new ScoreKeeper(key, prop, obj[prop]);
            this.choices.push(quest);
        }
    }
}// end Question

// function that will render the questions inside the container
Question.prototype.render = function(container, direction) {
    var self = this;
    var dir = direction;
    // Fill out the question label
    var question_string_h3;
    if(container.children('h3').length === 0) {
        question_string_h3 = $('<h3>').appendTo(container);
    } else {
        question_string_h3 = container.children('h3').first();
    }
    question_string_h3.text(this.question_string);
    // console.log(question_string_h3);
    // Clear any radio buttons and create new ones
    if(container.children('input[type=radio]').length > 0) {
        container.children('input[type=radio]').each(function() {
            var radio_button_id = $(this).attr('id');
            $(this).remove();
            container.children('label[for=' + radio_button_id + ']').remove();
        });
    }
    if(container.children('input[type=checkbox]').length > 0 ) {
        container.children('input[type=checkbox]').each(function() {
            var checkbox_id = $(this).attr('id');
            $(this).remove();
            container.children('label[for=' + checkbox_id + ']').remove();
        });
    }
    for(var i = 0; i < this.choices.length; i++) {
        //console.log(this.user_choice_index);
        // console.log(this.choices[i].answer);
        if(this.type == "checkbox") {
            // Create the checkbox
            var choice_radio_button = $('<input>')
                .attr('id', 'choices-' + this.choices[i].title)
                .attr('type', 'checkbox')
                .attr('name', 'choices')
                .attr('data-answer', this.choices[i].answer)
                .attr('value', 'choices-' + this.choices[i].score);

                //.appendTo(container);
            if(this.user_choice_index !== null) {
                // console.log("i : " + i);
                // console.log("userchoiceindex: " + this.user_choice_index);
                choice_radio_button.attr('checked', this.user_choice_index.includes(i + 1))
                    .appendTo(container);
            } else {
                choice_radio_button.appendTo(container);
            }
        } else {
            // Create the radio button
            var choice_radio_button = $('<input>')
                .attr('id', 'choices-' + this.choices[i].title)
                .attr('type', 'radio')
                .attr('name', 'choices')
                .attr('data-answer', this.choices[i].answer)
                .attr('value', 'choices-' + this.choices[i].score)
                .attr('checked', i === (this.user_choice_index - 1))
                .appendTo(container);
        }
        // Create the lable
        if(dir == 'prev') {
            var choice_label = $('<label>')
                .text(this.choices[i].answer)
                .attr('for', 'choices-' + this.choices[i].title)
                .attr('class', 'moveIn-place')
                .appendTo(container);
        } else {
            var choice_label = $('<label>')
                .text(this.choices[i].answer)
                .attr('for', 'choices-' + this.choices[i].title)
                .attr('class', 'moveIn-' + this.choices[i].title)
                .appendTo(container);
        }

    }
    var checkboxAnswerIndex = [];
    // Add a listener for the radio button to change which on the user has clicked on
    $('input[name=choices]').change(function(index) {
        //var selected_radio_button_value = $('input[name=choices]:checked').val();
        // var answerIndex = $('input[name=choices]:checked').attr('id');
        var answerIndex = $(this).attr('id');
        if($(this).attr('type') == 'checkbox') {
            //console.log("answerIndex : " + answerIndex);
            // console.log($(this).prop('checked'));
            if(checkboxAnswerIndex.includes(parseInt(answerIndex.substr(answerIndex.length - 1, 1)))) {

                var i = checkboxAnswerIndex.indexOf(parseInt(answerIndex.substr(answerIndex.length - 1, 1)));
                if(i != -1) {
                    checkboxAnswerIndex.splice(i, 1);
                }
                // checkboxAnswerIndex = checkboxAnswerIndex.filter(function(item) {
                //     return item !== answerIndex;
                // });
            } else {
                checkboxAnswerIndex.push(parseInt(answerIndex.substr(answerIndex.length - 1, 1)));
            }
            //console.log('checkboxindes: ' + checkboxAnswerIndex);
        }
        // console.log(checkboxAnswerIndex);
        // console.log($(this).attr('id'));
        // console.log(answerIndex);
        if($('#next-question-button').prop('disabled')) {
            $('#next-question-button').prop('disabled', false);
        }
        // Change the user choice index
        if($(this).attr('type') == 'checkbox') {
            self.user_choice_index = checkboxAnswerIndex;
        } else {
            self.user_choice_index = parseInt(answerIndex.substr(answerIndex.length - 1, 1));
        }


        // Trigger a user-select-change
        container.trigger('user-select-change');
    });
}// end Question.prototype.render


$(document).ready(function() {
    // Create an instance of the Quiz object
    var quiz = new Quiz('Modality');
    // Create the Question objects from all_questions
    for(var i = 0; i < all_questions.length; i++) {
        var question = new Question(all_questions[i].question_string, all_questions[i].choices, all_questions[i].type);
        // Add the question to the instance of the Quiz object
        quiz.add_question(question);
    }

    // Render the quiz
    var quiz_container = $('.quizContainer');
    quiz.render(quiz_container);

    $('.pageStart').on('click', 'a', function() {
        $('.pageStart').addClass('noHeight');
        var $container = $('.pageStart').children();
        //$('#pageNumber').fadeToggle();
        $container.each(function(index, element) {
            setTimeout(function() {
                $(element).addClass('moveOut');
            }, 100 + ( index * 100 ));
        });

        $('.quizContainer').toggleClass('btnHidden');
        $('.questionsHold').addClass('show');
    });
});


})(jQuery);
























