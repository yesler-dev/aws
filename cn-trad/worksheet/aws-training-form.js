(function($) {
    console.log('form script');

    // JS for grabbing utm params
    function getRefQueryParam(name, url) {
        if(!url) {
            url = window.location.href;
        }
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    // Get all the utm parameters before the page finishes rendering
    var utmParamQueryString = '',
        utmParamQueryStringTrimmed = '',
        utm_firstname = '',
        utm_lastname = '',
        utm_email = '';

    MktoForms2.loadForm("//app-sj23.marketo.com", "539-TIB-603", 13879, function(form) {
        // form.vals({
        //     "FirstName":"Lisa",
        //     "LastName":"Simpson",
        //     "Email":"lisa@testing.com"
        // });
        form.onSuccess(function(values, followUpUrl) {
            form.getFormElem().hide();
            return false;
        });
    });

    MktoForms2.whenReady(function(form) {
        var $mktoForm = $('form');
        var $inputs = $('form :input');
        var $quiz = $('#question');

        utm_firstname = getRefQueryParam("utm_firstname");
        utm_lastname = getRefQueryParam("utm_lastname");
        utm_email = getRefQueryParam("utm_email");

        if(utm_firstname) {
            utmParamQueryString += '&utm_firstname=' + utm_firstname;
            if(utm_firstname != "null") {
                $('input[name="FirstName"]').val(utm_firstname);
            }
        }
        if(utm_lastname) {
            utmParamQueryString += '&utm_lastname=' + utm_lastname;
            if(utm_lastname != "null") {
                $('input[name="LastName"]').val(utm_lastname);
            }
        }
        if(utm_email) {
            utmParamQueryString += '&utm_email=' + utm_email;
            if(utm_email != "null") {
                $('input[name="Email"]').val(utm_email);
            }
        }

        if(utmParamQueryString.length > 0) {
            utmParamQueryString = utmParamQueryString.substring(1);
            utmParamQueryStringTrimmed = utmParamQueryString;
            utmParamQueryString = '?' + utmParamQueryString;
        }

        console.log(utmParamQueryStringTrimmed);
        console.log($('input[name="FirstName"]').val());
        console.log($('input[name="LastName"]').val());
        console.log($('input[name="Email"]').val());
        // console.log($inputs);
        // console.log($mktoForm);

        $quiz.on('change', 'input[name=choices]', function() {
            console.log($(this));
            console.log($(this).attr('data-answer'));
            var answer = $(this).attr('data-answer');
            for(var i = 0; i < $inputs.length; i++) {
                if($inputs[i].value == answer && $($inputs[i]).prop('checked')) {
                    $($inputs[i]).prop('checked', false);
                } else if($inputs[i].value == answer) {
                    $($inputs[i]).prop('checked', true);
                } else {
                    // console.log('answer false');
                }
            }
        });
    });

    function submitMkto() {
        MktoForms2.whenReady(function(form) {
            form.submit();
        });
    }

    // Function to read the value of a cookie
    function readCookie(name) {
        //console.log(document.cookie);
        var cookiename = name + "=";
        var ca = document.cookie.split(';');
        for(var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while(c.charAt(0) == ' ') c = c.substring(1, c.length);
            if(c.indexOf(cookiename) == 0) return c.substring(cookiename.length, c.length);
        }
        return null;
    }

    $(document).ready(function() {
        var cook = readCookie('_mkto_trk');
        //console.log(cook);
        $('#testSubmit').on('click', function() {
            console.log('inside the torn apart');
            MktoForms2.whenReady(function(form) {
                form.submit();
            });
        });
    });




})(jQuery);




// <script src="//app-sj23.marketo.com/js/forms2/js/forms2.min.js"></script>
// <form id="mktoForm_13871"></form>
// <script>MktoForms2.loadForm("//app-sj23.marketo.com", "539-TIB-603", 13871);</script>

