(function($) {

// answers
// cloud - intro
// arch - assoc or pro
// archassoc - assoc
// archpro - pro
// dev - assoc
// sysops - assoc
// devops - pro
// pro - only recommend pro level
// assoc - only recommend assoc level
// none - auto cloud rec
// all - special message
// spec - one of the add ons
// data
// security
// networking


var all_results = [{
    type: "choices-cloud",
    text: {
        headline: "AWS Certified Cloud Practitioner",
        link: "https://aws.amazon.com/certification/certified-cloud-practitioner/",
        desc: "這個基礎認證可驗證您對 AWS 雲端實務、原則和特性的核心理解。推薦在任何角色至少具備六個月 AWS 經驗的個人。",
        long: "這個基礎認證可驗證您對 AWS 雲端實務、原則和特性的核心理解。推薦在任何角色至少具備六個月 AWS 經驗的個人。"
    }
}, {
    type: "choices-archassoc",
    text: {
        headline: "AWS Certified Solutions Architect – Associate",
        link: "https://aws.amazon.com/certification/certified-solutions-architect-associate/",
        desc: "這個助理級認證可驗證您在 AWS 上設計分散式應用程式和系統的知識和技能。推薦給具備一或多年 AWS 設計經驗的專業人士。",
        long: "這個助理級認證可驗證您在 AWS 上設計分散式應用程式和系統的知識和技能。推薦給具備一或多年 AWS 設計經驗的專業人士。"
    }
}, {
    type: "choices-archpro",
    text: {
        headline: "AWS Certified Solutions Architect – Professional",
        link: "https://aws.amazon.com/certification/certified-solutions-architect-professional/",
        desc: "這個專業級認證可驗證您在 AWS 上設計分散式應用程式和系統的進階技術技能和經驗。適用於已獲得 AWS Certified Solutions Architect – Associate 而且經驗豐富的架構師。",
        long: "這個專業級認證可驗證您在 AWS 上設計分散式應用程式和系統的進階技術技能和經驗。適用於已獲得 AWS Certified Solutions Architect – Associate 而且經驗豐富的架構師。"
    }
}, {
    type: "choices-dev",
    text: {
        headline: "AWS Certified Developer – Associate",
        link: "https://aws.amazon.com/certification/certified-developer-associate/",
        desc: "這個助理級認證可驗證您在 AWS 上開發和維護應用程式的技術專業知識。推薦給具備一或多年設計和維護 AWS 應用程式實作經驗的開發人員。",
        long: "這個助理級認證可驗證您在 AWS 上開發和維護應用程式的技術專業知識。推薦給具備一或多年設計和維護 AWS 應用程式實作經驗的開發人員。"
    }
}, {
    type: "choices-sysops",
    text: {
        headline: "AWS Certified SysOps Administrator – Associate",
        link: "https://aws.amazon.com/certification/certified-sysops-admin-associate/",
        desc: "這個助理級認證可驗證您在 AWS 上部署、管理和操作的專業技術知識。推薦給具備一或多年操作 AWS 應用程式實作經驗的管理員。",
        long: "這個助理級認證可驗證您在 AWS 上部署、管理和操作的專業技術知識。推薦給具備一或多年操作 AWS 應用程式實作經驗的管理員。"
    }
}, {
    type: "choices-devops",
    text: {
        headline: "AWS Certified DevOps Engineer – Professional",
        link: "https://aws.amazon.com/certification/certified-devops-engineer-professional/",
        desc: "這個專業級認證可驗證您在 AWS 上佈建、操作和管理分散式應用程式系統的技術專業知識。適用於已獲得 AWS Certified Developer – Associate 或 AWS Certified SysOps Administrator – Associate 而且經驗豐富的架構師。",
        long: "這個專業級認證可驗證您在 AWS 上佈建、操作和管理分散式應用程式系統的技術專業知識。適用於已獲得 AWS Certified Developer – Associate 或 AWS Certified SysOps Administrator – Associate 而且經驗豐富的架構師。"
    }
}, {
    type: "choices-networking",
    text: {
        headline: "AWS Certified Advanced Networking – Specialty",
        link: "https://aws.amazon.com/certification/",
        desc: "這個專家級認證可驗證您在大規模設計和實作 AWS 和混合 IT 網路架構方面的進階技術技能。",
        long: "這個專家級認證可驗證您在大規模設計和實作 AWS 和混合 IT 網路架構方面的進階技術技能。"
    }
}, {
    type: "choices-data",
    text: {
        headline: "AWS Certified Big Data – Specialty",
        link: "https://aws.amazon.com/certification/",
        desc: "這個專家級認證可驗證您在設計和實作 AWS 服務，以從資料獲得價值的技術技能和經驗。",
        long: "這個專家級認證可驗證您在設計和實作 AWS 服務，以從資料獲得價值的技術技能和經驗。"
    }
}, {
    type: "choices-security",
    text: {
        headline: "AWS Certified Security – Specialty",
        link: "https://aws.amazon.com/certification/",
        desc: "這個專家級認證可驗證您保護 AWS 安全的知識，以及與事件回應、日誌記錄和監控、基礎設施安全、身分和存取管理以及資料保護等方面相關的技術技能。",
        long: "這個專家級認證可驗證您保護 AWS 安全的知識，以及與事件回應、日誌記錄和監控、基礎設施安全、身分和存取管理以及資料保護等方面相關的技術技能。"
    }
}];

var all_questions = [{

    question_string: "請告訴我們關於您的 AWS 經驗",
    choices: {
        q1: {
            cloud: "我曾參與管理、領導、測試或其他非程式設計功能方面的 AWS 專案不到一年的時間"
        },
        q2: {
            archassoc: "我曾以架構師/設計師的身分參與多個 AWS 專案"
        },
        q3: {
            sysops: "我曾參與 AWS 上的多個專案部署"
        },
        q4: {
            dev: "我曾參與在 AWS 上建立和開發多個專案"
        },
        q5: {
            none: "我沒有 AWS 經驗"
        }
    },
    type: "checkbox"

}, {
    question_string: "您想擔任哪個角色？",
    choices: {
        q1: {
            archassoc: "雲端安全工程師"
        },
        q2: {
            dev: "網路工程師"
        },
        q3: {
            cloud: "專案經理"
        },
        q4: {
            archassoc: "資料科學家"
        },
        q5: {
            dev: "入門級 IT"
        },
        q6: {
            cloud: "領導/管理"
        },
        q7: {
            archassoc: "解決方案架構師"
        },
        q8: {
            archassoc: "企業架構師"
        },
        q9: {
            archassoc: "技術架構師"
        },
        q10: {
            archassoc: "應用程式架構師"
        },
        q11: {
            dev: "開發人員"
        },
        q12: {
            dev: "軟體工程師"
        },
        q13: {
            dev: "程式設計師"
        },
        q14: {
            sysops: "SysOps"
        },
        q15: {
            sysops: "SecOps"
        },
        q16: {
            devops: "DevOps"
        },
        q17: {
            sysops: "系統操作員"
        },
        q18: {
            sysops: "系統管理員"
        }
    },
    type: "checkbox"
}, {
    question_string: "您已經取得哪些 AWS 認證？",
    choices: {
        q1: {
            archassoc: "AWS Certified Cloud Practitioner"
        },
        q2: {
            archpro: "AWS Certified Solutions Architect – Associate"
        },
        q3: {
            devops: "AWS Certified Developer – Associate"
        },
        q4: {
            devops: "AWS Certified SysOps Administrator – Associate"
        },
        q5: {
            proSpec1: "AWS Certified Solutions Architect – Professional"
        },
        q6: {
            proSpec2: "AWS Certified DevOps Engineer – Professional"
        },
        q7: {
            spec: "AWS Certified Advanced Networking – Specialty"
        },
        q8: {
            spec: "AWS Certified Big Data – Specialty"
        },
        q9: {
            spec: "AWS Certified Security – Specialty"
        },
        q10: {
            all: "以上皆是"
        },
        q11: {
            none: "無"
        }
    },
    type: "checkbox"
}, {
    question_string: "您的雲端知識目標是什麼？",
    choices: {
        q1: {
            dev: "我想在雲端建立穩健的創新應用程式。"
        },
        q2: {
            archassoc: "我想建立和部署可以隨著不同負載和客戶需求進行擴展的系統。"
        },
        q3: {
            archassoc: "我想確保所建立和部署的系統永遠不會故障。"
        },
        q4: {
            archassoc: "我想知道如何部署備援生產應用程式。"
        },
        q5: {
            archassoc: "我想成為組織安全問題的關鍵人物。"
        },
        q6: {
            spec: "我想了解如何從資料得出可靠的結論。"
        },
        q7: {
            spec: "我想了解如何解譯存取日誌，以及更好地控制敏感資料的存取。"
        },
        q8: {
            sysops: "我希望能將現場部署應用程式移到雲端。"
        },
        q9: {
            sysops: "我希望能有效地設定 CI/CD 開發環境。"
        },
        q10: {
            spec: "我想要設定系統，以提供 DDoS 和 EDoS 保護。"
        },
        q11: {
            archassoc: "我想了解基本的雲端架構和核心服務，以便做出更明智的業務決策。"
        },
        q12: {
            other: "其他"
        }
    },
    type: "checkbox"
}, {
    question_string: "您對哪些 AWS 主題感興趣？",
    choices: {
        q1: {
            spec: "我想部署一些結合了現場部署資料中心元素和 AWS 元件的混合系統。"
        },
        q2: {
            archassoc: "我想開發一些使用關鍵 AWS 服務的應用程式工作知識，例如 AWS 資料庫、通知、儲存和工作流程服務，以及變更管理服務。"
        },
        q3: {
            dev: "我想了解如何在 AWS 設計、開發、部署以及維護安全又可靠的應用程式。"
        },
        q4: {
            spec: "我想學習如何設定 NACL/ACL、防火牆、網路閘道、VPN、金鑰管理、SSL、HTTPS、SAML、字符和憑證。"
        },
        q5: {
            blank: "以上皆非。"
        }
    },
    type: "checkbox"
}];

var Quiz = function(quiz_name) {
    this.quiz_name = quiz_name;
    this.questions = [];
}

// Function to add a question to the Quiz object
Quiz.prototype.add_question = function(question) {
    // Randomly choose where to add question
    // var index_to_add_question = Math.floor(Math.random() * this.questions.length);

    var index_to_add_question = this.questions.length;
    this.questions.splice(index_to_add_question, 0, question);
}

Quiz.prototype.render = function(container) {
    var self = this;
    var $submit = $('#submit-button'), $prev = $('#prev-question-button'), $next = $('#next-question-button');

    $('#quiz-results').hide();

    var question_container = $('<div>').attr('id', 'question').insertAfter('#quiz-name');
    function change_question(direction) {
        var dir = direction;
        self.questions[current_question_index].render(question_container, dir);
        // console.log(self);
        moveIn();
        $prev.prop('disabled', current_question_index === 0)
        $prev.toggleClass('btnHidden', current_question_index === 0);
        $next.prop('disabled', self.questions[current_question_index].user_choice_index === null);

        // Determine if all questions have been answered
        var all_questions_answered = true;
        for(var i = 0; i < self.questions.length; i++) {
            if(self.questions[i].user_choice_index === null) {
                all_questions_answered = false;
                break;
            }
        }
        if(all_questions_answered && current_question_index == self.questions.length - 1) {
            $next.toggleClass('btnHidden');
            $submit.prop('enable')
            $submit.toggleClass('btnHidden');
        } else {
            $next.removeClass('btnHidden');
            $submit.prop('disabled');
            $submit.addClass('btnHidden');
        }
    }

    function change_page_number() {
        var curPage = parseInt(self.questions.indexOf(self.questions[current_question_index])) + 1;
        var totalPages = self.questions.length;
        var numbers = curPage + "/" + totalPages;
        $('#pageNumber').text(numbers);
    }

    // Render the first question
    var current_question_index = 0;
    change_question();
    change_page_number();
    // Add listener for the previous question button
    $prev.on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        if(current_question_index > 0) {
            current_question_index--;
            userScore.pop();
            change_question('prev');
            change_page_number();
        }
    });
    // Add listener for the next question button
    $next.on('click', function() {
        var body = $(document).height();

        //console.log(body);
        if(current_question_index < self.questions.length - 1 && current_question_index !== undefined) {
            current_question_index++;
            userScore.push(pushScore($('#question input[name=choices]:checked')));
            console.log(userScore);

            moveOut();

            change_page_number();
            // $('#body').height(body);
        }
    });

    function pushScore(vals) {
        var curScore, checkScore = [], $this = vals;
        if($this.attr('type') == 'radio') {
            curScore = $this.val();
            // console.log(curScore);
            return curScore;
        } else {
            for(var i = 0; i < $this.length; i++) {
                if($this[i].value == "choices-other") {
                    break;
                }
                if($this[i].value == "choices-blank") {
                    break;
                }
                checkScore.push($this[i].value);
            }
            console.log(checkScore);
            return curScore = checkScore;
        }

    }

    function submitMkto() {
        MktoForms2.whenReady(function(form) {
            form.submit();
        });
    }

    // Add listener for the submit button
    $submit.on('click', function() {
        userScore.push(pushScore($('#question input[name=choices]:checked')));
        // console.log(userScore);
        var disp = resultsDisplay(occurrence(userScore));
        $('#quiz-results').prepend(disp);
        // submitMkto();
        $('body').toggleClass('bodyResults');
        $('.questionsHold').toggleClass('btnHidden');
        $('.resultsFooter').toggleClass('btnHidden');
        $('#quiz-results').css('opacity', 0).slideDown(1000).animate({ opacity: 1}, { queue: false, duration: 2000});
        $('#quiz button').slideUp();
    });

    // Add a listener on the questions container to listen for user select changes
    // This is for determining whether we can submit answers of not.
    question_container.bind('user-select-change', function() {
        var all_questions_answered = true;
        for(var i = 0; i < self.questions.length; i++) {
            if(self.questions[i].user_choice_index === null) {
                all_questions_answered = false;
                break;
            }
            //console.log(self);
        }

        if(all_questions_answered && current_question_index == self.questions.length - 1) {
            //console.log('the end');
            $next.addClass('btnHidden');
            $submit.prop('enable')
            $submit.removeClass('btnHidden');
        } else {
            //console.log('where am I');
            $next.removeClass('btnHidden');
            $submit.prop('disabled');
            $submit.addClass('btnHidden');
        }
    });

    function moveOut() {
        var $container = $('#question').children();
        var setback = ($container.length) * 30;
        $container.each(function(index, element) {
            setTimeout(function() {
                $(element).removeClass(function (index, css) {
                    return (css.match (/\bmoveIn-\S+/g) || []).join(' ');
                });
                $(element).addClass('moveOut');
            }, ( index * 30 ));
        });

        setTimeout(function() {
            $('.buttonHolder').removeClass(function (index, css) {
                    return (css.match (/\bmoveIn-\S+/g) || []).join(' ');
                }).addClass('moveOut');
        }, (setback));
        setTimeout(function() {
            change_question();
        }, (setback + 445));
    }

    function moveIn() {
        var $container = $('#question').children();
        var timer = ($container.length) / 2 * 30;
        var setback = ($container.length + 1) / 2;

        $('#question h3').addClass('moveIn-header');
        setTimeout(function() {
            $('.buttonHolder').removeClass('moveOut');
            $('.buttonHolder button, .buttonHolder a').css({ opacity : 0.8 });
            $('.buttonHolder').addClass('moveIn-q' + setback);
        }, timer);
    }
}// end Quiz.prototype.render

var ScoreKeeper = function(title, score, answer) {
    this.title  = title,
    this.score  = score,
    this.answer = answer;
}
var userScore = [];
var occurrence = function(score) {
    var countedSpecs = function(choices) {
        var specNum = 0
        for(i = 0; i < choices.length; i++) {
            if(choices[i] == 'choices-spec') {
                specNum++;
            }
        }
        return specNum;
    }

    var result, item = [], tempCount = 0, suggestion, flat, level, autoRec;
    console.log(score);
    if(score[2].includes('choices-archpro') || score[2].includes('choices-devops') ) {
        level = 'pro';
    }
    if(score[2].includes('choices-spec') && score[2].includes('choices-archpro') || score[2].includes('choices-spec') && score[2].includes('choices-devops') ) {
        autoRec = 'specs';
    }

    if(countedSpecs(score[2]) == 3) {
        if(score[2].includes('choices-proSpec1') && score[2].includes('choices-proSpec2')) {
            autoRec = 'all';
        } else if(score[2].includes('choices-proSpec1')) {
            autoRec = 'choices-proSpec2';
        } else if(score[2].includes('choices-proSpec2')) {
            autoRec = 'choices-proSpec1';
        } else if(level == null) {
            autoRec = 'all';
        }
    }

    // if(score[2].includes('choices-spec') && score[2].includes('choices-proSpec1') && score[2].includes('choices-proSpec2')) {
    //     // if(countedSpecs(score[2]) == 5) {
    //         autoRec = 'all';
    //     // }
    // }
    // if(score[2].includes('choices-spec') && score[2].length >= 3 && level == null) {
    //     console.log(countedSpecs(score[2]));
    //     if(countedSpecs(score[2]) == 3) {
    //         autoRec = 'all';
    //     }
    // }
    // if(score[2].includes('choices-spec', 'choices-spec', 'choices-spec', 'choices-spec', 'choices-spec')) {
    //     autoRec = 'all';
    // }



    flat = [].concat.apply([], score);
    // console.log(flat);
    result = flat.reduce(function(prev, next) {
        prev[next] = (prev[next] + 1) || 1;
        return prev;
    },{});
    console.log("level: " + level);
    console.log("autoRec: " + autoRec);
    console.log(result);
    for(var k in result) {
        console.log('k: ' + k);
        if(k == 'choices-all') {
            // console.log('inside the if');
            item.pop();
            item.unshift(k);
            // console.log('item is: ' + item);
            break;
        }
        if(k == 'choices-devops' && item == 'choices-dev') {
            // console.log('inside DevOps');
            item.pop();
            item.unshift(k);
            // console.log('new k item is: ' + item);
        }
        if(result[k] > tempCount) {
            item.pop();
            tempCount = result[k];
            item.unshift(k);
        }
        console.log('item: ' + item);
    }
    suggestion = item[0];
    if(suggestion == 'choices-archassoc' && level == 'pro') {
        suggestion = 'choices-archpro';
    } else if(suggestion == 'choices-devops' || suggestion == 'choices-sysops' && level == 'pro') {
        suggestion = 'choices-devops';
    }
    if(autoRec == 'specs') {
        suggestion = 'choices-spec';
    }
    if(autoRec == 'all') {
        suggestion = 'choices-all';
    }
    if(autoRec == 'choices-proSpec1') {
        suggestion = 'choices-archpro';
    }
    if(autoRec == 'choices-proSpec2') {
        suggestion = 'choices-devops';
    }
    // console.log(suggestion);
    return suggestion;
};

var resultsDisplay = function(choice) {
    var classInfo = all_results, display = "", mainClass = "", otherClasses = "";
    // console.log(classInfo);
    // console.log('choice is: ' + choice);
    if(choice == 'choices-none') {
        choice = 'choices-cloud';
    }
    if(choice == 'choices-arch') {
        choice = 'choices-archassoc';
    }
    display += "<div class='resultsHeader'><span class='shapeHolderLeftResults shapeResults'></span>";
    mainClass += "<div class='innerResults'>";
    mainClass += "<div class='innerHolder'>";
    if(choice == "choices-spec") {
        mainClass += "<h2>AWS Specialty Certifications</h2>";
        mainClass += "<p>這些進階認證可驗證您在特定技術領域的進階技能。適用於已獲得 AWS Certified Cloud Practitioner 或具備有效助理級認證的個人。</p>";
        // mainClass += "<h2>AWS Specialty Certifications</h2>";
        // mainClass += "<p>These certifications validate your advanced skills in specific technical areas. They are for individuals who have achieved AWS Certified Cloud Practitioner or have an active Associate-level certification.</p>";
    } else if(choice == "choices-all") {
        mainClass += "<h2>繼續您的 AWS 認證之旅</h2>";
        mainClass += "<p>您已透過進階 AWS 認證證明您的 AWS 技能；現在該是時候繼續充實技術知識了。請密切關注新的專家級認證，並注意每 24 個月的重新認證提醒。</p>";
    } else {
        mainClass += "<h2>您的認證路徑</h2>";
        mainClass += "<p>根據您的回答，我們建議您專注在以下各項：</p>";
    }
    mainClass += "<span class='shapeHolderRightResults shapeResults'></span></div></div></div>";
    for(var i = 0; i < classInfo.length; i++) {
        if(choice == 'choices-all') {
            mainClass += "<div class='mainClass thanksResults'><div class='innerResults'>";
            break;
        }
        if(choice == 'choices-spec') {
            mainClass += "<div class='mainClass specResults'><div class='innerResults'>";
            mainClass += "<article>"
            mainClass += "<img src='./img/icons/aws-certificate-icon-" + classInfo[6].type + ".svg' />";
            mainClass += "<div class='mainText'>"
            mainClass += "<h3>" + classInfo[6].text.headline + "</h3>";
            mainClass += "<p>" + classInfo[6].text.long + "</p>";
            mainClass += "</article>";
            mainClass += "<article>"
            mainClass += "<img src='./img/icons/aws-certificate-icon-" + classInfo[7].type + ".svg' />";
            mainClass += "<div class='mainText'>"
            mainClass += "<h3>" + classInfo[7].text.headline + "</h3>";
            mainClass += "<p>" + classInfo[7].text.long + "</p>";
            mainClass += "</article>";
            mainClass += "<article>"
            mainClass += "<img src='./img/icons/aws-certificate-icon-" + classInfo[8].type + ".svg' />";
            mainClass += "<div class='mainText'>"
            mainClass += "<h3>" + classInfo[8].text.headline + "</h3>";
            mainClass += "<p>" + classInfo[8].text.long + "</p>";
            mainClass += "</article>";
            mainClass += "<p class='specLearnMore'>我們會不斷推出新的專家級認證，請密切關注。</p>";
            mainClass += "<a class='ctaButton' href='" + classInfo[6].text.link + "' target='_blank'>進一步了解</a>";
            mainClass += "</div>";
            break;
        }
        else if(choice == classInfo[i].type) {
            mainClass += "<div class='mainClass'><div class='innerResults'>";
            mainClass += "<img src='./img/icons/aws-certificate-icon-" + classInfo[i].type + ".svg' />";
            mainClass += "<div class='mainText'>"
            mainClass += "<h3>" + classInfo[i].text.headline + "</h3>";
            mainClass += "<p>" + classInfo[i].text.long + "</p>";
            mainClass += "<a class='ctaButton' href='" + classInfo[i].text.link + "' target='_blank'>進一步了解</a>";
            mainClass += "</div>";
        }
    }
     mainClass += "</div></div>";
    display += mainClass;
    display += "<div class='otherClassesContainer'><p>我們提供四種以角色為基礎的認證路徑，其中包含不同的專業等級，還有專家級認證。 <a href='https://aws.amazon.com/certification/'>請參閱您的所有 AWS 認證選項。</a></p></div>";
    return display;
};

// object for a Question. Constructor
var Question = function(question_string, choices, type) {
    this.question_string   = question_string;
    this.choices           = [];
    this.user_choice_index = null; //Index of the user's choice selection
    this.type              = type;

    // TODO - fill choices[] with choices and do something with it
    var number_of_choices = choices;
    for(var key in number_of_choices) {
        var obj = number_of_choices[key];
        for(var prop in obj) {
            var quest = new ScoreKeeper(key, prop, obj[prop]);
            this.choices.push(quest);
        }
    }
}// end Question

var makeRadio = function(curRadio, appendTo, index) {
    var choice_radio_button = $('<input>')
        .attr('id', 'choices-' + curRadio.choices[index].title)
        .attr('type', 'radio')
        .attr('name', 'choices')
        .attr('data-answer', this.choices[i].answer)
        .attr('value', 'choices-' + curRadio.choices[index].score)
        .attr('checked', index === (curRadio.user_choice_index - 1))
        .appendTo(appendTo);
}

var makeLabel = function(curRadio, appendTo, index, size) {
    var choice_label = $('<label>')
        .text(curRadio.choices[index].answer)
        .attr('for', 'choices-' + curRadio.choices[index].title)
        .attr('class', 'moveIn-' + curRadio.choices[index].title + ' ' + size)
        .appendTo(appendTo);
}

// function that will render the questions inside the container
Question.prototype.render = function(container, direction) {
    var self = this;
    console.log(self);
    var dir = direction;
    // Fill out the question label
    var question_string_h3;
    var question_string_hint;
    if(container.children('h3').length === 0) {
        question_string_h3 = $('<h3>').appendTo(container);
        // console.log('inside the if');
    } else {
        question_string_h3 = container.children('h3').first();
        // console.log('inside the else');
    }
    question_string_h3.text(this.question_string);
    question_string_hint = $('<span class="hintText">').appendTo(question_string_h3);
    if(this.type == "checkbox") {
        question_string_hint.text('(選擇所有適用的選項)');
    } else if(this.type == "radio") {
        question_string_hint.text('(Choose one)');
    }
    // Clear any radio buttons and create new ones
    if(container.children('input[type=radio]').length > 0) {
        container.children('input[type=radio]').each(function() {
            var radio_button_id = $(this).attr('id');
            $(this).remove();
            container.children('label[for=' + radio_button_id + ']').remove();
        });
    }
    if(container.children('input[type=checkbox]').length > 0 ) {
        container.children('input[type=checkbox]').each(function() {
            var checkbox_id = $(this).attr('id');
            $(this).remove();
            container.children('label[for=' + checkbox_id + ']').remove();
        });
    }
    // console.log(this.choices.length);
    for(var i = 0; i < this.choices.length; i++) {
        //console.log("i: " + i);
        if(this.type == "checkbox") {
            // Create the checkbox
            var choice_radio_button = $('<input>')
                .attr('id', 'choices-' + this.choices[i].title)
                .attr('type', 'checkbox')
                .attr('name', 'choices')
                .attr('data-answer', this.choices[i].answer)
                .attr('value', 'choices-' + this.choices[i].score);
                //.appendTo(container);
            if(this.user_choice_index !== null) {
                // console.log("i : " + i);
                // console.log("userchoiceindex: " + this.user_choice_index);
                choice_radio_button.attr('checked', this.user_choice_index.includes(i + 1))
                    .appendTo(container);
            } else {
                choice_radio_button.appendTo(container);
            }
            if(this.choices[i].score == 'all') {
                choice_radio_button.addClass('preventMore');
            }
        // } else if(this.type == "radio" && this.choices.length > 9) {
        //         // Create the radio button
        //         makeRadio(this, container, i);
        } else {
            // Create the radio button
            makeRadio(this, container, i);
        }
        // Create the label
        if(dir == 'prev') {
            if(this.type == "checkbox" && this.choices.length > 9) {
                var choice_label = $('<label>')
                    .text(this.choices[i].answer)
                    .attr('for', 'choices-' + this.choices[i].title)
                    .attr('class', 'moveIn-place halfSize')
                    .appendTo(container);
            } else {
                var choice_label = $('<label>')
                    .text(this.choices[i].answer)
                    .attr('for', 'choices-' + this.choices[i].title)
                    .attr('class', 'moveIn-place')
                    .appendTo(container);
            }

        } else if(this.type == "checkbox" && this.choices.length > 9) {
                makeLabel(this, container, i, 'halfSize');
                // makeLabel(this, container, i, 'fullSize');
        } else {
            makeLabel(this, container, i, 'fullSize');
        }
    } // end for loop

    var checkboxAnswerIndex = [];
    // Add a listener for the radio button to change which on the user has clicked on
    $('input[name=choices]').change(function(index) {
        //var selected_radio_button_value = $('input[name=choices]:checked').val();
        // var answerIndex = $('input[name=choices]:checked').attr('id');
        var answerIndex = $(this).attr('id');
        // console.log(checkboxAnswerIndex);
        if($(this).attr('type') == 'checkbox') {
            // console.log("answerIndex : " + answerIndex);
            // console.log($(this).prop('checked'));
            // console.log($(this));
            // console.log($(this)[0].value);
            if($(this)[0].value == 'choices-all' || $(this)[0].value == 'choices-none' || $(this)[0].value == 'choices-blank') {
                // console.log(this.id);
                var $ignore = $("label[for='" + this.id + "']");
                $('input[name=choices] + label').not($ignore).toggleClass('preventClick');
                $('input[name=choices]').not(this).prop('checked', false);
            }
            if(checkboxAnswerIndex.includes(parseInt(answerIndex.substring(9)))) {
                var i = checkboxAnswerIndex.indexOf(parseInt(answerIndex.substring(9)));
                if(i != -1) {
                    checkboxAnswerIndex.splice(i, 1);
                }
                // checkboxAnswerIndex = checkboxAnswerIndex.filter(function(item) {
                //     return item !== answerIndex;
                // });
            } else {
                checkboxAnswerIndex.push(parseInt(answerIndex.substr(9)));
            }
            //console.log('checkboxindes: ' + checkboxAnswerIndex);
        }
        // console.log(checkboxAnswerIndex);
        // console.log($(this).attr('id'));
        // console.log(answerIndex);
        if($('#next-question-button').prop('disabled')) {
            $('#next-question-button').prop('disabled', false);
        }
        // Change the user choice index
        if($(this).attr('type') == 'checkbox') {
            self.user_choice_index = checkboxAnswerIndex;
        } else {
            self.user_choice_index = parseInt(answerIndex.substr(answerIndex.length - 1, 1));
        }


        // Trigger a user-select-change
        container.trigger('user-select-change');
    });
}// end Question.prototype.render


$(document).ready(function() {
    // Create an instance of the Quiz object
    var quiz = new Quiz('Modality');
    // Create the Question objects from all_questions
    for(var i = 0; i < all_questions.length; i++) {
        var question = new Question(all_questions[i].question_string, all_questions[i].choices, all_questions[i].type);
        // Add the question to the instance of the Quiz object
        quiz.add_question(question);
    }

    // Render the quiz
    var quiz_container = $('.quizContainer');
    quiz.render(quiz_container);

    $('.pageStart').on('click', 'a', function() {
        $('.pageStart').addClass('noHeight');
        var $container = $('.pageStart').children();
        //$('#pageNumber').fadeToggle();
        $container.each(function(index, element) {
            setTimeout(function() {
                $(element).addClass('moveOut');
            }, 100 + ( index * 100 ));
        });

        $('.quizContainer').toggleClass('btnHidden');
        $('.questionsHold').addClass('show');
    });
});


})(jQuery);
























