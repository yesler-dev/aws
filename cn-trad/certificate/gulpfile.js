// Include gulp
var gulp = require('gulp');

// Include plugins
var jshint       = require('gulp-jshint');
var sass         = require('gulp-sass');
var concat       = require('gulp-concat');
var uglify       = require('gulp-uglify');
var livereload   = require('gulp-livereload');
var connect      = require('gulp-connect');
var plumber      = require('gulp-plumber');
var postcss      = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var rename       = require('gulp-rename');
var replace      = require('gulp-replace');
var cssmin       = require('gulp-cssmin');
var concatCss    = require('gulp-concat-css');

// Live Reload
gulp.task('connect', function() {
    connect.server({
        livereload: true
    });
});

// JS files
gulp.task('js', function() {
    gulp.src('js/**/*.js')
        .pipe(livereload())
        .pipe(gulp.dest('./'));
});

// HTML files
gulp.task('html', function() {
    gulp.src('*.html')
        .pipe(livereload());
});

// Compile Sass
gulp.task('sass', function() {
    return gulp.src('css/*.scss')
        .pipe(sass())
        .pipe(postcss([ autoprefixer() ]))
        .pipe(gulp.dest('./'))
        .pipe(livereload());
});

// minify styles
gulp.task('min', function() {
    return gulp.src(['./aws-certificate-style.css', './template.html', './aws-certificate-style-prod.css'])
        .pipe(cssmin())
        .pipe(gulp.dest('./dist'));
});

// URL replace for Marketo
gulp.task('mktoreplace', function() {
    return gulp.src(['./aws-certificate-style.css', './index.html', './aws-certificate-scripts.js', './aws-certificate-form.js'])
        .pipe(replace('./img/backgrounds/', 'https://na-sj23.marketo.com/rs/539-TIB-603/images/'))
        .pipe(replace('./img/design/', 'https://na-sj23.marketo.com/rs/539-TIB-603/images/'))
        .pipe(replace('./img/icons/', 'https://na-sj23.marketo.com/rs/539-TIB-603/images/'))
        .pipe(replace('./img/logos/', 'https://na-sj23.marketo.com/rs/539-TIB-603/images/'))
        .pipe(replace('href="./aws-certificate-style.css"', 'href="https://na-sj23.marketo.com/rs/539-TIB-603/images/aws-certificate-style.css"'))
        .pipe(replace('src="./js/aws-certificate-scripts.js"', 'src="https://na-sj23.marketo.com/rs/539-TIB-603/images/aws-certificate-scripts.js"'))
        .pipe(replace('src="./js/aws-certificate-form.js"', 'src="https://na-sj23.marketo.com/rs/539-TIB-603/images/aws-certificate-form.js"'))
        .pipe(replace('src: url("./css/fonts/', 'src: url("https://na-sj23.marketo.com/rs/539-TIB-603/images/'))
        .pipe(replace('), url("./css/fonts/', '), url("https://na-sj23.marketo.com/rs/539-TIB-603/images/'))
        // .pipe(cssmin())
        .pipe(gulp.dest('./dist/mkto'));
});

// URL replace for Marketo
gulp.task('mktoreplaceProd', function() {
    return gulp.src(['./aws-certificate-style-prod.css', './production.html', './aws-certificate-scripts-prod.js', './aws-certificate-form-prod.js'])
        .pipe(replace('./img/backgrounds/', 'https://na-sj23.marketo.com/rs/112-TZM-766/images/'))
        .pipe(replace('./img/design/', 'https://na-sj23.marketo.com/rs/112-TZM-766/images/'))
        .pipe(replace('./img/icons/', 'https://na-sj23.marketo.com/rs/112-TZM-766/images/'))
        .pipe(replace('./img/logos/', 'https://na-sj23.marketo.com/rs/112-TZM-766/images/'))
        .pipe(replace('href="./aws-certificate-style.css"', 'href="https://na-sj23.marketo.com/rs/112-TZM-766/images/aws-certificate-style-prod.css"'))
        .pipe(replace('src="./js/aws-certificate-scripts.js"', 'src="https://na-sj23.marketo.com/rs/112-TZM-766/images/aws-certificate-scripts-prod.js"'))
        .pipe(replace('src="./js/aws-certificate-form.js"', 'src="https://na-sj23.marketo.com/rs/112-TZM-766/images/aws-certificate-form-prod.js"'))
        .pipe(replace('src: url("./css/fonts/', 'src: url("https://na-sj23.marketo.com/rs/112-TZM-766/images/'))
        .pipe(replace('), url("./css/fonts/', '), url("https://na-sj23.marketo.com/rs/112-TZM-766/images/'))
        // .pipe(cssmin())
        .pipe(gulp.dest('./dist/mkto'));
});

// Watch files for changes
gulp.task('watch', function() {
    livereload.listen();
    gulp.watch('css/**/*.scss', ['sass']);
    gulp.watch('*.html', ['html', 'mktoreplace']);
    gulp.watch('js/**/*.js', ['js']);
});

// gulp.task('watch', function() {
//     livereload.listen();
//     gulp.watch('css/**/*.scss', ['sass']);
//     gulp.watch('*.html', ['html', 'mktoreplace']);
//     gulp.watch('js/**/*.js', ['js']);
//     gulp.watch('./style.css', ['mktoreplace']);
// });

// Default Task
gulp.task('default', ['sass', 'watch', 'connect']);
gulp.task('mkto', ['mktoreplace', 'mktoreplaceProd']);


