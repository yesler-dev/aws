(function($) {

// answers
// cloud - intro
// arch - assoc or pro
// archassoc - assoc
// archpro - pro
// dev - assoc
// sysops - assoc
// devops - pro
// pro - only recommend pro level
// assoc - only recommend assoc level
// none - auto cloud rec
// all - special message
// spec - one of the add ons
// data
// security
// networking


var all_results = [{
    type: "choices-cloud",
    text: {
        headline: "AWS Certified Cloud Practitioner",
        link: "https://aws.amazon.com/certification/certified-cloud-practitioner/",
        desc: "This foundational certification validates your core understanding of AWS Cloud practices, principles and characteristics. Recommended for individuals who have at least six months of AWS experience in any role.",
        long: "This foundational certification validates your core understanding of AWS Cloud practices, principles and characteristics. Recommended for individuals who have at least six months of AWS experience in any role."
    }
}, {
    type: "choices-archassoc",
    text: {
        headline: "AWS Certified Solutions Architect – Associate",
        link: "https://aws.amazon.com/certification/certified-solutions-architect-associate/",
        desc: "This Associate certification validates your knowledge and skills in designing distributed applications and systems on AWS. Recommended for professionals with one or more years of AWS design experience.",
        long: "This Associate certification validates your knowledge and skills in designing distributed applications and systems on AWS. Recommended for professionals with one or more years of AWS design experience."
    }
}, {
    type: "choices-archpro",
    text: {
        headline: "AWS Certified Solutions Architect – Professional",
        link: "https://aws.amazon.com/certification/certified-solutions-architect-professional/",
        desc: "This Professional certification validates your advanced technical skills and experience in designing distributed applications and systems on AWS. Intended for experienced architects who have earned AWS Certified Solutions Architect — Associate.",
        long: "This Professional certification validates your advanced technical skills and experience in designing distributed applications and systems on AWS. Intended for experienced architects who have earned AWS Certified Solutions Architect — Associate."
    }
}, {
    type: "choices-dev",
    text: {
        headline: "AWS Certified Developer – Associate",
        link: "https://aws.amazon.com/certification/certified-developer-associate/",
        desc: "This Associate certification validates your technical expertise in developing and maintaining applications on AWS. Recommended for developers with one or more years of hands-on experience designing and maintaining AWS-based applications.",
        long: "This Associate certification validates your technical expertise in developing and maintaining applications on AWS. Recommended for developers with one or more years of hands-on experience designing and maintaining AWS-based applications."
    }
}, {
    type: "choices-sysops",
    text: {
        headline: "AWS Certified SysOps Administrator – Associate",
        link: "https://aws.amazon.com/certification/certified-sysops-admin-associate/",
        desc: "This Associate certification validates your technical expertise in deployment, management and operations on AWS. Recommended for administrators with one or more years of hands-on experience operating AWS-based applications.",
        long: "This Associate certification validates your technical expertise in deployment, management and operations on AWS. Recommended for administrators with one or more years of hands-on experience operating AWS-based applications."
    }
}, {
    type: "choices-devops",
    text: {
        headline: "AWS Certified DevOps Engineer – Professional",
        link: "https://aws.amazon.com/certification/certified-devops-engineer-professional/",
        desc: "This Professional certification validates your technical expertise in provisioning, operating and managing distributed application systems on AWS. Intended for experienced architects who have achieved AWS Certified Developer — Associate or AWS Certified SysOps Administrator — Associate.",
        long: "This Professional certification validates your technical expertise in provisioning, operating and managing distributed application systems on AWS. Intended for experienced architects who have achieved AWS Certified Developer — Associate or AWS Certified SysOps Administrator — Associate."
    }
}, {
    type: "choices-networking",
    text: {
        headline: "AWS Certified Advanced Networking – Specialty",
        link: "https://aws.amazon.com/certification/",
        desc: "This specialty certification validates your advanced technical skills in designing and implementing AWS and hybrid IT network architectures at scale.",
        long: "This specialty certification validates your advanced technical skills in designing and implementing AWS and hybrid IT network architectures at scale."
    }
}, {
    type: "choices-data",
    text: {
        headline: "AWS Certified Big Data – Specialty",
        link: "https://aws.amazon.com/certification/",
        desc: "This specialty certification validates your technical skills and experience in designing and implementing AWS services to derive value from data.",
        long: "This specialty certification validates your technical skills and experience in designing and implementing AWS services to derive value from data."
    }
}, {
    type: "choices-security",
    text: {
        headline: "AWS Certified Security – Specialty",
        link: "https://aws.amazon.com/certification/",
        desc: "This specialty certification validates your knowledge of securing AWS and your technical skills relating to incident response, logging and monitoring, infrastructure security, identity and access management, and data protection.",
        long: "This specialty certification validates your knowledge of securing AWS and your technical skills relating to incident response, logging and monitoring, infrastructure security, identity and access management, and data protection."
    }
}];

var all_questions = [{

    question_string: "Tell us about your AWS experience",
    choices: {
        q1: {
            cloud: "I have contributed to an AWS project for less than a year in a management, leadership, testing or other non-coding function"
        },
        q2: {
            archassoc: "I have contributed to multiple AWS projects as an architect/designer"
        },
        q3: {
            sysops: "I have contributed to the deployment of multiple projects on AWS"
        },
        q4: {
            dev: "I have contributed to building and developing multiple projects on AWS"
        },
        q5: {
            none: "I have no AWS experience"
        }
    },
    type: "checkbox"

}, {
    question_string: "Which role would you like to have?",
    choices: {
        q1: {
            archassoc: "Cloud Security Engineer"
        },
        q2: {
            dev: "Network Engineer"
        },
        q3: {
            cloud: "Project Manager"
        },
        q4: {
            archassoc: "Data Scientist"
        },
        q5: {
            dev: "Entry-Level IT"
        },
        q6: {
            cloud: "Leadership/Management"
        },
        q7: {
            archassoc: "Solutions Architect"
        },
        q8: {
            archassoc: "Enterprise Architect"
        },
        q9: {
            archassoc: "Technical Architect"
        },
        q10: {
            archassoc: "Applications Architect"
        },
        q11: {
            dev: "Developer"
        },
        q12: {
            dev: "Software Engineer"
        },
        q13: {
            dev: "Coder"
        },
        q14: {
            sysops: "SysOps"
        },
        q15: {
            sysops: "SecOps"
        },
        q16: {
            devops: "DevOps"
        },
        q17: {
            sysops: "Systems Operator"
        },
        q18: {
            sysops: "Systems Admin"
        }
    },
    type: "checkbox"
}, {
    question_string: "Which AWS Certifications have you earned?",
    choices: {
        q1: {
            archassoc: "AWS Certified Cloud Practitioner"
        },
        q2: {
            archpro: "AWS Certified Solutions Architect – Associate"
        },
        q3: {
            devops: "AWS Certified Developer – Associate"
        },
        q4: {
            devops: "AWS Certified SysOps Administrator – Associate"
        },
        q5: {
            proSpec1: "AWS Certified Solutions Architect – Professional"
        },
        q6: {
            proSpec2: "AWS Certified DevOps Engineer – Professional"
        },
        q7: {
            spec: "AWS Certified Advanced Networking – Specialty"
        },
        q8: {
            spec: "AWS Certified Big Data – Specialty"
        },
        q9: {
            spec: "AWS Certified Security – Specialty"
        },
        q10: {
            all: "All"
        },
        q11: {
            none: "None"
        }
    },
    type: "checkbox"
}, {
    question_string: "What are your cloud knowledge goals?",
    choices: {
        q1: {
            dev: "I want to build robust, innovative applications in the cloud."
        },
        q2: {
            archassoc: "I want to build and deploy systems that can scale with variant load and customer demand."
        },
        q3: {
            archassoc: "I want to ensure the systems I build and deploy never fail."
        },
        q4: {
            archassoc: "I want to know how to deploy redundant production applications."
        },
        q5: {
            archassoc: "I want to be my organization’s go-to person for security questions."
        },
        q6: {
            spec: "I want to learn how to draw informed conclusions from data."
        },
        q7: {
            spec: "I want to learn how to interpret access logs and better control access to sensitive data."
        },
        q8: {
            sysops: "I want to be able to move on-premises applications to the cloud."
        },
        q9: {
            sysops: "I want to effectively set up a CI/CD development environment."
        },
        q10: {
            spec: "I want to configure systems to provide DDoS and EDoS protection."
        },
        q11: {
            archassoc: "I want to understand basic cloud architecture and core services to make better business decisions."
        },
        q12: {
            other: "Other"
        }
    },
    type: "checkbox"
}, {
    question_string: "What AWS topics interest you?",
    choices: {
        q1: {
            spec: "I want to deploy hybrid systems that combine on-premises data center elements and AWS components."
        },
        q2: {
            archassoc: "I want to develop working knowledge of applications that use key AWS services, such as AWS databases, notifications, storage and workflow services, and change management services."
        },
        q3: {
            dev: "I want to understand designing, developing, deploying and maintaining secure and reliable applications in AWS."
        },
        q4: {
            spec: "I want to learn how to configure NACLs/ACLs, firewalls, network gateways, VPNs, key management, SSL, HTTPS, SAML, tokens and certificates."
        },
        q5: {
            blank: "None of the above."
        }
    },
    type: "checkbox"
}];

var Quiz = function(quiz_name) {
    this.quiz_name = quiz_name;
    this.questions = [];
}

// Function to add a question to the Quiz object
Quiz.prototype.add_question = function(question) {
    // Randomly choose where to add question
    // var index_to_add_question = Math.floor(Math.random() * this.questions.length);

    var index_to_add_question = this.questions.length;
    this.questions.splice(index_to_add_question, 0, question);
}

Quiz.prototype.render = function(container) {
    var self = this;
    var $submit = $('#submit-button'), $prev = $('#prev-question-button'), $next = $('#next-question-button');

    $('#quiz-results').hide();

    var question_container = $('<div>').attr('id', 'question').insertAfter('#quiz-name');
    function change_question(direction) {
        var dir = direction;
        self.questions[current_question_index].render(question_container, dir);
        // console.log(self);
        moveIn();
        $prev.prop('disabled', current_question_index === 0)
        $prev.toggleClass('btnHidden', current_question_index === 0);
        $next.prop('disabled', self.questions[current_question_index].user_choice_index === null);

        // Determine if all questions have been answered
        var all_questions_answered = true;
        for(var i = 0; i < self.questions.length; i++) {
            if(self.questions[i].user_choice_index === null) {
                all_questions_answered = false;
                break;
            }
        }
        if(all_questions_answered && current_question_index == self.questions.length - 1) {
            $next.toggleClass('btnHidden');
            $submit.prop('enable')
            $submit.toggleClass('btnHidden');
        } else {
            $next.removeClass('btnHidden');
            $submit.prop('disabled');
            $submit.addClass('btnHidden');
        }
    }

    function change_page_number() {
        var curPage = parseInt(self.questions.indexOf(self.questions[current_question_index])) + 1;
        var totalPages = self.questions.length;
        var numbers = curPage + "/" + totalPages;
        $('#pageNumber').text(numbers);
    }

    // Render the first question
    var current_question_index = 0;
    change_question();
    change_page_number();
    // Add listener for the previous question button
    $prev.on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        if(current_question_index > 0) {
            current_question_index--;
            userScore.pop();
            change_question('prev');
            change_page_number();
        }
    });
    // Add listener for the next question button
    $next.on('click', function() {
        var body = $(document).height();

        //console.log(body);
        if(current_question_index < self.questions.length - 1 && current_question_index !== undefined) {
            current_question_index++;
            userScore.push(pushScore($('#question input[name=choices]:checked')));
            console.log(userScore);

            moveOut();

            change_page_number();
            // $('#body').height(body);
        }
    });

    function pushScore(vals) {
        var curScore, checkScore = [], $this = vals;
        if($this.attr('type') == 'radio') {
            curScore = $this.val();
            // console.log(curScore);
            return curScore;
        } else {
            for(var i = 0; i < $this.length; i++) {
                if($this[i].value == "choices-other") {
                    break;
                }
                if($this[i].value == "choices-blank") {
                    break;
                }
                checkScore.push($this[i].value);
            }
            console.log(checkScore);
            return curScore = checkScore;
        }

    }

    // Add listener for the submit button
    $submit.on('click', function() {
        userScore.push(pushScore($('#question input[name=choices]:checked')));
        // console.log(userScore);
        var disp = resultsDisplay(occurrence(userScore));
        $('#quiz-results').prepend(disp);
        $('body').toggleClass('bodyResults');
        $('.questionsHold').toggleClass('btnHidden');
        $('.resultsFooter').toggleClass('btnHidden');
        $('#quiz-results').css('opacity', 0).slideDown(1000).animate({ opacity: 1}, { queue: false, duration: 2000});
        $('#quiz button').slideUp();
    });

    // Add a listener on the questions container to listen for user select changes
    // This is for determining whether we can submit answers of not.
    question_container.bind('user-select-change', function() {
        var all_questions_answered = true;
        for(var i = 0; i < self.questions.length; i++) {
            if(self.questions[i].user_choice_index === null) {
                all_questions_answered = false;
                break;
            }
            //console.log(self);
        }

        if(all_questions_answered && current_question_index == self.questions.length - 1) {
            //console.log('the end');
            $next.addClass('btnHidden');
            $submit.prop('enable')
            $submit.removeClass('btnHidden');
        } else {
            //console.log('where am I');
            $next.removeClass('btnHidden');
            $submit.prop('disabled');
            $submit.addClass('btnHidden');
        }
    });

    function moveOut() {
        var $container = $('#question').children();
        var setback = ($container.length) * 30;
        $container.each(function(index, element) {
            setTimeout(function() {
                $(element).removeClass(function (index, css) {
                    return (css.match (/\bmoveIn-\S+/g) || []).join(' ');
                });
                $(element).addClass('moveOut');
            }, ( index * 30 ));
        });

        setTimeout(function() {
            $('.buttonHolder').removeClass(function (index, css) {
                    return (css.match (/\bmoveIn-\S+/g) || []).join(' ');
                }).addClass('moveOut');
        }, (setback));
        setTimeout(function() {
            change_question();
        }, (setback + 445));
    }

    function moveIn() {
        var $container = $('#question').children();
        var timer = ($container.length) / 2 * 30;
        var setback = ($container.length + 1) / 2;

        $('#question h3').addClass('moveIn-header');
        setTimeout(function() {
            $('.buttonHolder').removeClass('moveOut');
            $('.buttonHolder button, .buttonHolder a').css({ opacity : 0.8 });
            $('.buttonHolder').addClass('moveIn-q' + setback);
        }, timer);
    }
}// end Quiz.prototype.render

var ScoreKeeper = function(title, score, answer) {
    this.title  = title,
    this.score  = score,
    this.answer = answer;
}
var userScore = [];
var occurrence = function(score) {
    var countedSpecs = function(choices) {
        var specNum = 0
        for(i = 0; i < choices.length; i++) {
            if(choices[i] == 'choices-spec') {
                specNum++;
            }
        }
        return specNum;
    }

    var result, item = [], tempCount = 0, suggestion, flat, level, autoRec;
    console.log(score);
    if(score[2].includes('choices-archpro') || score[2].includes('choices-devops') ) {
        level = 'pro';
    }
    if(score[2].includes('choices-spec') && score[2].includes('choices-archpro') || score[2].includes('choices-spec') && score[2].includes('choices-devops') ) {
        autoRec = 'specs';
    }

    if(countedSpecs(score[2]) == 3) {
        if(score[2].includes('choices-proSpec1') && score[2].includes('choices-proSpec2')) {
            autoRec = 'all';
        } else if(score[2].includes('choices-proSpec1')) {
            autoRec = 'choices-proSpec2';
        } else if(score[2].includes('choices-proSpec2')) {
            autoRec = 'choices-proSpec1';
        } else if(level == null) {
            autoRec = 'all';
        }
    }

    // if(score[2].includes('choices-spec') && score[2].includes('choices-proSpec1') && score[2].includes('choices-proSpec2')) {
    //     // if(countedSpecs(score[2]) == 5) {
    //         autoRec = 'all';
    //     // }
    // }
    // if(score[2].includes('choices-spec') && score[2].length >= 3 && level == null) {
    //     console.log(countedSpecs(score[2]));
    //     if(countedSpecs(score[2]) == 3) {
    //         autoRec = 'all';
    //     }
    // }
    // if(score[2].includes('choices-spec', 'choices-spec', 'choices-spec', 'choices-spec', 'choices-spec')) {
    //     autoRec = 'all';
    // }



    flat = [].concat.apply([], score);
    // console.log(flat);
    result = flat.reduce(function(prev, next) {
        prev[next] = (prev[next] + 1) || 1;
        return prev;
    },{});
    console.log("level: " + level);
    console.log("autoRec: " + autoRec);
    console.log(result);
    for(var k in result) {
        console.log('k: ' + k);
        if(k == 'choices-all') {
            // console.log('inside the if');
            item.pop();
            item.unshift(k);
            // console.log('item is: ' + item);
            break;
        }
        if(k == 'choices-devops' && item == 'choices-dev') {
            // console.log('inside DevOps');
            item.pop();
            item.unshift(k);
            // console.log('new k item is: ' + item);
        }
        if(result[k] > tempCount) {
            item.pop();
            tempCount = result[k];
            item.unshift(k);
        }
        console.log('item: ' + item);
    }
    suggestion = item[0];
    if(suggestion == 'choices-archassoc' && level == 'pro') {
        suggestion = 'choices-archpro';
    } else if(suggestion == 'choices-devops' || suggestion == 'choices-sysops' && level == 'pro') {
        suggestion = 'choices-devops';
    }
    if(autoRec == 'specs') {
        suggestion = 'choices-spec';
    }
    if(autoRec == 'all') {
        suggestion = 'choices-all';
    }
    if(autoRec == 'choices-proSpec1') {
        suggestion = 'choices-archpro';
    }
    if(autoRec == 'choices-proSpec2') {
        suggestion = 'choices-devops';
    }
    // console.log(suggestion);
    return suggestion;
};

var resultsDisplay = function(choice) {
    var classInfo = all_results, display = "", mainClass = "", otherClasses = "";
    // console.log(classInfo);
    // console.log('choice is: ' + choice);
    if(choice == 'choices-none') {
        choice = 'choices-cloud';
    }
    if(choice == 'choices-arch') {
        choice = 'choices-archassoc';
    }
    display += "<div class='resultsHeader'><span class='shapeHolderLeftResults shapeResults'></span>";
    mainClass += "<div class='innerResults'>";
    mainClass += "<div class='innerHolder'>";
    if(choice == "choices-spec") {
        mainClass += "<h2>AWS Specialty Certifications</h2>";
        mainClass += "<p>These advanced certifications validate your advanced skills in specific technical areas. They are for individuals who have achieved AWS Certified Cloud Practitioner or have an active Associate-level certification.</p>";
        // mainClass += "<h2>AWS Specialty Certifications</h2>";
        // mainClass += "<p>These certifications validate your advanced skills in specific technical areas. They are for individuals who have achieved AWS Certified Cloud Practitioner or have an active Associate-level certification.</p>";
    } else if(choice == "choices-all") {
        mainClass += "<h2>Continue your AWS certification journey</h2>";
        mainClass += "<p>You’ve validated your AWS skills with advanced AWS certifications; now it’s time to continue expanding your technical knowledge. Keep an eye out for new specialty certifications and watch for reminders to get recertified every 24 months.</p>";
    } else {
        mainClass += "<h2>Your certification path</h2>";
        mainClass += "<p>Based on your answers, we recommend focusing on the following:</p>";
    }
    mainClass += "<span class='shapeHolderRightResults shapeResults'></span></div></div></div>";
    for(var i = 0; i < classInfo.length; i++) {
        if(choice == 'choices-all') {
            mainClass += "<div class='mainClass thanksResults'><div class='innerResults'>";
            break;
        }
        if(choice == 'choices-spec') {
            mainClass += "<div class='mainClass specResults'><div class='innerResults'>";
            mainClass += "<article>"
            mainClass += "<img src='./img/icons/aws-certificate-icon-" + classInfo[6].type + ".svg' />";
            mainClass += "<div class='mainText'>"
            mainClass += "<h3>" + classInfo[6].text.headline + "</h3>";
            mainClass += "<p>" + classInfo[6].text.long + "</p>";
            mainClass += "</article>";
            mainClass += "<article>"
            mainClass += "<img src='./img/icons/aws-certificate-icon-" + classInfo[7].type + ".svg' />";
            mainClass += "<div class='mainText'>"
            mainClass += "<h3>" + classInfo[7].text.headline + "</h3>";
            mainClass += "<p>" + classInfo[7].text.long + "</p>";
            mainClass += "</article>";
            mainClass += "<article>"
            mainClass += "<img src='./img/icons/aws-certificate-icon-" + classInfo[8].type + ".svg' />";
            mainClass += "<div class='mainText'>"
            mainClass += "<h3>" + classInfo[8].text.headline + "</h3>";
            mainClass += "<p>" + classInfo[8].text.long + "</p>";
            mainClass += "</article>";
            mainClass += "<p class='specLearnMore'>Keep your eyes out for new Specialty Certifications as they become available.</p>";
            mainClass += "<a class='ctaButton' href='" + classInfo[6].text.link + "' target='_blank'>Learn more</a>";
            mainClass += "</div>";
            break;
        }
        else if(choice == classInfo[i].type) {
            mainClass += "<div class='mainClass'><div class='innerResults'>";
            mainClass += "<img src='./img/icons/aws-certificate-icon-" + classInfo[i].type + ".svg' />";
            mainClass += "<div class='mainText'>"
            mainClass += "<h3>" + classInfo[i].text.headline + "</h3>";
            mainClass += "<p>" + classInfo[i].text.long + "</p>";
            mainClass += "<a class='ctaButton' href='" + classInfo[i].text.link + "' target='_blank'>Learn more</a>";
            mainClass += "</div>";
        }
    }
     mainClass += "</div></div>";
    display += mainClass;
    display += "<div class='otherClassesContainer'><p>We offer four role-based certification paths with advancing levels of expertise, as well as Specialty certifications. <a href='https://aws.amazon.com/certification/'>See all your AWS Certification options.</a></p></div>";
    return display;
};

// object for a Question. Constructor
var Question = function(question_string, choices, type) {
    this.question_string   = question_string;
    this.choices           = [];
    this.user_choice_index = null; //Index of the user's choice selection
    this.type              = type;

    // TODO - fill choices[] with choices and do something with it
    var number_of_choices = choices;
    for(var key in number_of_choices) {
        var obj = number_of_choices[key];
        for(var prop in obj) {
            var quest = new ScoreKeeper(key, prop, obj[prop]);
            this.choices.push(quest);
        }
    }
}// end Question

var makeRadio = function(curRadio, appendTo, index) {
    var choice_radio_button = $('<input>')
        .attr('id', 'choices-' + curRadio.choices[index].title)
        .attr('type', 'radio')
        .attr('name', 'choices')
        .attr('value', 'choices-' + curRadio.choices[index].score)
        .attr('checked', index === (curRadio.user_choice_index - 1))
        .appendTo(appendTo);
}

var makeLabel = function(curRadio, appendTo, index, size) {
    var choice_label = $('<label>')
        .text(curRadio.choices[index].answer)
        .attr('for', 'choices-' + curRadio.choices[index].title)
        .attr('class', 'moveIn-' + curRadio.choices[index].title + ' ' + size)
        .appendTo(appendTo);
}

// function that will render the questions inside the container
Question.prototype.render = function(container, direction) {
    var self = this;
    console.log(self);
    var dir = direction;
    // Fill out the question label
    var question_string_h3;
    var question_string_hint;
    if(container.children('h3').length === 0) {
        question_string_h3 = $('<h3>').appendTo(container);
        // console.log('inside the if');
    } else {
        question_string_h3 = container.children('h3').first();
        // console.log('inside the else');
    }
    question_string_h3.text(this.question_string);
    question_string_hint = $('<span class="hintText">').appendTo(question_string_h3);
    if(this.type == "checkbox") {
        question_string_hint.text('(Choose all that apply)');
    } else if(this.type == "radio") {
        question_string_hint.text('(Choose one)');
    }
    // Clear any radio buttons and create new ones
    if(container.children('input[type=radio]').length > 0) {
        container.children('input[type=radio]').each(function() {
            var radio_button_id = $(this).attr('id');
            $(this).remove();
            container.children('label[for=' + radio_button_id + ']').remove();
        });
    }
    if(container.children('input[type=checkbox]').length > 0 ) {
        container.children('input[type=checkbox]').each(function() {
            var checkbox_id = $(this).attr('id');
            $(this).remove();
            container.children('label[for=' + checkbox_id + ']').remove();
        });
    }
    // console.log(this.choices.length);
    for(var i = 0; i < this.choices.length; i++) {
        //console.log("i: " + i);
        if(this.type == "checkbox") {
            // Create the checkbox
            var choice_radio_button = $('<input>')
                .attr('id', 'choices-' + this.choices[i].title)
                .attr('type', 'checkbox')
                .attr('name', 'choices')
                .attr('value', 'choices-' + this.choices[i].score);
                //.appendTo(container);
            if(this.user_choice_index !== null) {
                // console.log("i : " + i);
                // console.log("userchoiceindex: " + this.user_choice_index);
                choice_radio_button.attr('checked', this.user_choice_index.includes(i + 1))
                    .appendTo(container);
            } else {
                choice_radio_button.appendTo(container);
            }
            if(this.choices[i].score == 'all') {
                choice_radio_button.addClass('preventMore');
            }
        // } else if(this.type == "radio" && this.choices.length > 9) {
        //         // Create the radio button
        //         makeRadio(this, container, i);
        } else {
            // Create the radio button
            makeRadio(this, container, i);
        }
        // Create the label
        if(dir == 'prev') {
            if(this.type == "checkbox" && this.choices.length > 9) {
                var choice_label = $('<label>')
                    .text(this.choices[i].answer)
                    .attr('for', 'choices-' + this.choices[i].title)
                    .attr('class', 'moveIn-place halfSize')
                    .appendTo(container);
            } else {
                var choice_label = $('<label>')
                    .text(this.choices[i].answer)
                    .attr('for', 'choices-' + this.choices[i].title)
                    .attr('class', 'moveIn-place')
                    .appendTo(container);
            }

        } else if(this.type == "checkbox" && this.choices.length > 9) {
                makeLabel(this, container, i, 'halfSize');
                // makeLabel(this, container, i, 'fullSize');
        } else {
            makeLabel(this, container, i, 'fullSize');
        }
    } // end for loop

    var checkboxAnswerIndex = [];
    // Add a listener for the radio button to change which on the user has clicked on
    $('input[name=choices]').change(function(index) {
        //var selected_radio_button_value = $('input[name=choices]:checked').val();
        // var answerIndex = $('input[name=choices]:checked').attr('id');
        var answerIndex = $(this).attr('id');
        // console.log(checkboxAnswerIndex);
        if($(this).attr('type') == 'checkbox') {
            // console.log("answerIndex : " + answerIndex);
            // console.log($(this).prop('checked'));
            // console.log($(this));
            // console.log($(this)[0].value);
            if($(this)[0].value == 'choices-all' || $(this)[0].value == 'choices-none' || $(this)[0].value == 'choices-blank') {
                // console.log(this.id);
                var $ignore = $("label[for='" + this.id + "']");
                $('input[name=choices] + label').not($ignore).toggleClass('preventClick');
                $('input[name=choices]').not(this).prop('checked', false);
            }
            if(checkboxAnswerIndex.includes(parseInt(answerIndex.substring(9)))) {
                var i = checkboxAnswerIndex.indexOf(parseInt(answerIndex.substring(9)));
                if(i != -1) {
                    checkboxAnswerIndex.splice(i, 1);
                }
                // checkboxAnswerIndex = checkboxAnswerIndex.filter(function(item) {
                //     return item !== answerIndex;
                // });
            } else {
                checkboxAnswerIndex.push(parseInt(answerIndex.substr(9)));
            }
            //console.log('checkboxindes: ' + checkboxAnswerIndex);
        }
        // console.log(checkboxAnswerIndex);
        // console.log($(this).attr('id'));
        // console.log(answerIndex);
        if($('#next-question-button').prop('disabled')) {
            $('#next-question-button').prop('disabled', false);
        }
        // Change the user choice index
        if($(this).attr('type') == 'checkbox') {
            self.user_choice_index = checkboxAnswerIndex;
        } else {
            self.user_choice_index = parseInt(answerIndex.substr(answerIndex.length - 1, 1));
        }


        // Trigger a user-select-change
        container.trigger('user-select-change');
    });
}// end Question.prototype.render


$(document).ready(function() {
    // Create an instance of the Quiz object
    var quiz = new Quiz('Modality');
    // Create the Question objects from all_questions
    for(var i = 0; i < all_questions.length; i++) {
        var question = new Question(all_questions[i].question_string, all_questions[i].choices, all_questions[i].type);
        // Add the question to the instance of the Quiz object
        quiz.add_question(question);
    }

    // Render the quiz
    var quiz_container = $('.quizContainer');
    quiz.render(quiz_container);

    $('.pageStart').on('click', 'a', function() {
        $('.pageStart').addClass('noHeight');
        var $container = $('.pageStart').children();
        //$('#pageNumber').fadeToggle();
        $container.each(function(index, element) {
            setTimeout(function() {
                $(element).addClass('moveOut');
            }, 100 + ( index * 100 ));
        });

        $('.quizContainer').toggleClass('btnHidden');
        $('.questionsHold').addClass('show');
    });
});


})(jQuery);
























