(function($) {

// answers
// cloud - intro
// arch - assoc or pro
// archassoc - assoc
// archpro - pro
// dev - assoc
// sysops - assoc
// devops - pro
// pro - only recommend pro level
// assoc - only recommend assoc level
// none - auto cloud rec
// all - special message
// spec - one of the add ons
// data
// security
// networking


var all_results = [{
    type: "choices-cloud",
    text: {
        headline: "AWS 공인 클라우드 종사자",
        link: "https://aws.amazon.com/certification/certified-cloud-practitioner/",
        desc: "이 기본 자격증은 AWS 클라우드 사례, 원칙 및 특성의 핵심을 이해하고 있음을 검증합니다. 특정 역할과 상관 없이 최소 6개월 이상의 AWS 경험이 있는 개인에게 권장됩니다.",
        long: "이 기본 자격증은 AWS 클라우드 사례, 원칙 및 특성의 핵심을 이해하고 있음을 검증합니다. 특정 역할과 상관 없이 최소 6개월 이상의 AWS 경험이 있는 개인에게 권장됩니다."
    }
}, {
    type: "choices-archassoc",
    text: {
        headline: "AWS 공인 솔루션스 아키텍트 - 어소시에이트",
        link: "https://aws.amazon.com/certification/certified-solutions-architect-associate/",
        desc: "이 어소시에이트 자격증은 AWS에서 분산 애플리케이션 및 시스템을 설계하는 데 필요한 지식과 기술을 검증합니다. 1년 이상의 AWS 설계 경험이 있는 전문가에게 권장됩니다.",
        long: "이 어소시에이트 자격증은 AWS에서 분산 애플리케이션 및 시스템을 설계하는 데 필요한 지식과 기술을 검증합니다. 1년 이상의 AWS 설계 경험이 있는 전문가에게 권장됩니다."
    }
}, {
    type: "choices-archpro",
    text: {
        headline: "AWS 공인 솔루션스 아키텍트 – 프로페셔널",
        link: "https://aws.amazon.com/certification/certified-solutions-architect-professional/",
        desc: "이 전문가 자격증은 AWS에서 분산 애플리케이션 및 시스템을 설계하는 데 필요한 고급 기술 역량과 경험을 검증합니다.",
        long: "이 전문가 자격증은 AWS에서 분산 애플리케이션 및 시스템을 설계하는 데 필요한 고급 기술 역량과 경험을 검증합니다."
    }
}, {
    type: "choices-dev",
    text: {
        headline: "AWS 공인 개발자 – 어소시에이트",
        link: "https://aws.amazon.com/certification/certified-developer-associate/",
        desc: "이 어소시에이트 자격증은 AWS에서 애플리케이션을 개발하고 유지 및 관리하는 데 필요한 기술 전문성을 검증합니다. AWS 기반 애플리케이션 설계, 유지 및 관리와 관련된 실무 경험이 1년 이상 있는 개발자에게 권장됩니다.",
        long: "이 어소시에이트 자격증은 AWS에서 애플리케이션을 개발하고 유지 및 관리하는 데 필요한 기술 전문성을 검증합니다. AWS 기반 애플리케이션 설계, 유지 및 관리와 관련된 실무 경험이 1년 이상 있는 개발자에게 권장됩니다."
    }
}, {
    type: "choices-sysops",
    text: {
        headline: "AWS 공인 시스템 운영 관리자 – 어소시에이트",
        link: "https://aws.amazon.com/certification/certified-sysops-admin-associate/",
        desc: "이 어소시에이트 자격증은 AWS에서의 배포, 관리 및 운영에 대한 기술 전문성을 검증합니다. AWS 기반 애플리케이션을 운영한 1년 이상의 실무 경험이 있는 관리자에게 권장됩니다.",
        long: "이 어소시에이트 자격증은 AWS에서의 배포, 관리 및 운영에 대한 기술 전문성을 검증합니다. AWS 기반 애플리케이션을 운영한 1년 이상의 실무 경험이 있는 관리자에게 권장됩니다."
    }
}, {
    type: "choices-devops",
    text: {
        headline: "AWS 공인 데브옵스 엔지니어 – 프로페셔널",
        link: "https://aws.amazon.com/certification/certified-devops-engineer-professional/",
        desc: "이 프로페셔널 자격증은 AWS에서 분산 애플리케이션 시스템을 프로비저닝, 운영 및 관리하는 데 필요한 기술 전문성을 검증합니다.",
        long: "이 프로페셔널 자격증은 AWS에서 분산 애플리케이션 시스템을 프로비저닝, 운영 및 관리하는 데 필요한 기술 전문성을 검증합니다."
    }
}, {
    type: "choices-networking",
    text: {
        headline: "AWS 공인 고급 네트워킹 – 전문 분야",
        link: "https://aws.amazon.com/certification/",
        desc: "이 전문 분야 자격증은 대규모로 AWS 및 하이브리드 IT 네트워크 아키텍처를 설계 및 구현하는 데 필요한 고급 기술 역량을 검증합니다.",
        long: "이 전문 분야 자격증은 대규모로 AWS 및 하이브리드 IT 네트워크 아키텍처를 설계 및 구현하는 데 필요한 고급 기술 역량을 검증합니다."
    }
}, {
    type: "choices-data",
    text: {
        headline: "AWS 공인 빅 데이터 – 전문 분야",
        link: "https://aws.amazon.com/certification/",
        desc: "이 전문 분야 자격증은 데이터로부터 가치를 도출하도록 AWS 서비스를 설계하고 구현하는 데 필요한 기술 역량과 경험을 검증합니다.",
        long: "이 전문 분야 자격증은 데이터로부터 가치를 도출하도록 AWS 서비스를 설계하고 구현하는 데 필요한 기술 역량과 경험을 검증합니다."
    }
}, {
    type: "choices-security",
    text: {
        headline: "AWS 공인 보안 – 전문 분야",
        link: "https://aws.amazon.com/certification/",
        desc: "이 전문 분야 자격증은 인시던트 대응, 로깅 및 모니터링, 인프라 보안, 자격 증명 및 액세스 관리, 데이터 보호와 관련된 기술 역량과 AWS 보안에 대한 지식을 검증합니다.",
        long: "이 전문 분야 자격증은 인시던트 대응, 로깅 및 모니터링, 인프라 보안, 자격 증명 및 액세스 관리, 데이터 보호와 관련된 기술 역량과 AWS 보안에 대한 지식을 검증합니다."
    }
}];

var all_questions = [{

    question_string: "AWS 경험에 대해 알려주십시오.",
    choices: {
        q1: {
            cloud: "관리직이나 임원직, 테스트 업무 또는 코딩과 직접 관련이 없는 업무에서 1년 미만 동안 AWS 프로젝트에 기여했습니다."
        },
        q2: {
            archassoc: "여러 AWS 프로젝트에 아키텍트/디자이너로 기여했습니다."
        },
        q3: {
            sysops: "AWS에 여러 프로젝트를 배포 하는 일에 기여했습니다."
        },
        q4: {
            dev: "AWS에서 여러 프로젝트를 구축하고 개발하는 데 기여했습니다."
        },
        q5: {
            none: "AWS와 관련된 업무를 한 경험이 없습니다."
        }
    },
    type: "checkbox"

}, {
    question_string: "앞으로 어떤 직무를 원하십니까?",
    choices: {
        q1: {
            archassoc: "클라우드 보안 엔지니어"
        },
        q2: {
            dev: "네트워크 엔지니어"
        },
        q3: {
            cloud: "프로젝트 관리자"
        },
        q4: {
            archassoc: "데이터 사이언티스트"
        },
        q5: {
            dev: "IT 입문자"
        },
        q6: {
            cloud: "임원/관리직"
        },
        q7: {
            archassoc: "솔루션스 아키텍트"
        },
        q8: {
            archassoc: "엔터프라이즈 아키텍트"
        },
        q9: {
            archassoc: "테크니컬 아키텍트"
        },
        q10: {
            archassoc: "애플리케이션 아키텍트"
        },
        q11: {
            dev: "개발자"
        },
        q12: {
            dev: "소프트웨어 엔지니어"
        },
        q13: {
            dev: "코딩 관련 직무"
        },
        q14: {
            sysops: "시스템 운영"
        },
        q15: {
            sysops: "보안 운영"
        },
        q16: {
            devops: "데브옵스"
        },
        q17: {
            sysops: "시스템 운영자"
        },
        q18: {
            sysops: "시스템 관리자"
        }
    },
    type: "checkbox"
}, {
    question_string: "어떤 AWS 자격증을 보유하고 계십니까?",
    choices: {
        q1: {
            archassoc: "AWS 공인 클라우드 종사자"
        },
        q2: {
            archpro: "AWS 공인 솔루션스 아키텍트 – 어소시에이트"
        },
        q3: {
            devops: "AWS 공인 개발자 – 어소시에이트"
        },
        q4: {
            devops: "AWS 공인 시스템 운영 관리자 – 어소시에이트"
        },
        q5: {
            proSpec1: "AWS 공인 솔루션스 아키텍트 – 프로페셔널"
        },
        q6: {
            proSpec2: "AWS 공인 데브옵스 엔지니어 – 프로페셔널"
        },
        q7: {
            spec: "AWS 공인 고급 네트워킹 – 전문 분야"
        },
        q8: {
            spec: "AWS 공인 빅 데이터 – 전문 분야"
        },
        q9: {
            spec: "AWS 공인 보안 – 전문 분야"
        },
        q10: {
            all: "위 항목 모두 해당"
        },
        q11: {
            none: "없음"
        }
    },
    type: "checkbox"
}, {
    question_string: "클라우드 지식 습득을 통해 달성하고자 하는 목표는 무엇입니까?",
    choices: {
        q1: {
            dev: "클라우드에서 강력하고 혁신적인 애플리케이션을 구축하고 싶습니다."
        },
        q2: {
            archassoc: "변동적인 로드 및 고객 수요에 맞춰 확장 가능한 시스템을 구축 및 배포하고 싶습니다."
        },
        q3: {
            archassoc: "구축 및 배포하는 시스템에 장애가 발생하지 않도록 하고 싶습니다."
        },
        q4: {
            archassoc: "리던던트 프로덕션 애플리케이션을 배포하는 방법을 알고 싶습니다."
        },
        q5: {
            archassoc: "조직 내 보안 전문가가 되고 싶습니다."
        },
        q6: {
            spec: "데이터에서 정보에 입각한 결론을 도출하는 방법을 배우고 싶습니다."
        },
        q7: {
            spec: "액세스 로그를 해석하고, 민감한 데이터에 대한 액세스를 좀 더 효과적으로 제어하는 방법을 배우고 싶습니다."
        },
        q8: {
            sysops: "온프레미스 애플리케이션을 클라우드로 이전할 수 있기를 원합니다."
        },
        q9: {
            sysops: "CI/CD 개발 환경을 효과적으로 구축하고 싶습니다."
        },
        q10: {
            spec: "DDoS 및 EDoS 보호를 제공하는 시스템을 구성하고 싶습니다."
        },
        q11: {
            archassoc: "기본적인 클라우드 아키텍처와 핵심 서비스를 이해하여 더 나은 비즈니스 의사 결정을 내리고 싶습니다."
        },
        q12: {
            other: "기타"
        }
    },
    type: "checkbox"
}, {
    question_string: "관심 있는 AWS 관련 주제는 무엇입니까?",
    choices: {
        q1: {
            spec: "온프레미스 데이터 센터 요소와 AWS 구성 요소를 결합하는 하이브리드 시스템을 배포하고 싶습니다."
        },
        q2: {
            archassoc: "AWS 데이터베이스, 알림, 스토리지 및 워크플로 서비스, 변경 관리 서비스와 같은 주요 AWS 서비스를 사용하는 애플리케이션에 대한 실무 지식을 쌓고 싶습니다."
        },
        q3: {
            dev: "AWS에서 안전하고 안정적인 애플리케이션을 설계, 개발, 배포 및 유지 관리하는 방법을 이해하고 싶습니다."
        },
        q4: {
            spec: "NACL/ACL, 방화벽, 네트워크 게이트웨이, VPN, 키 관리, SSL, HTTPS, SAML, 토큰 및 인증서를 구성하는 방법을 배우고 싶습니다."
        },
        q5: {
            blank: "위 항목 모두 해당하지 않음."
        }
    },
    type: "checkbox"
}];

var Quiz = function(quiz_name) {
    this.quiz_name = quiz_name;
    this.questions = [];
}

// Function to add a question to the Quiz object
Quiz.prototype.add_question = function(question) {
    // Randomly choose where to add question
    // var index_to_add_question = Math.floor(Math.random() * this.questions.length);

    var index_to_add_question = this.questions.length;
    this.questions.splice(index_to_add_question, 0, question);
}

Quiz.prototype.render = function(container) {
    var self = this;
    var $submit = $('#submit-button'), $prev = $('#prev-question-button'), $next = $('#next-question-button');

    $('#quiz-results').hide();

    var question_container = $('<div>').attr('id', 'question').insertAfter('#quiz-name');
    function change_question(direction) {
        var dir = direction;
        self.questions[current_question_index].render(question_container, dir);
        // console.log(self);
        moveIn();
        $prev.prop('disabled', current_question_index === 0)
        $prev.toggleClass('btnHidden', current_question_index === 0);
        $next.prop('disabled', self.questions[current_question_index].user_choice_index === null);

        // Determine if all questions have been answered
        var all_questions_answered = true;
        for(var i = 0; i < self.questions.length; i++) {
            if(self.questions[i].user_choice_index === null) {
                all_questions_answered = false;
                break;
            }
        }
        if(all_questions_answered && current_question_index == self.questions.length - 1) {
            $next.toggleClass('btnHidden');
            $submit.prop('enable')
            $submit.toggleClass('btnHidden');
        } else {
            $next.removeClass('btnHidden');
            $submit.prop('disabled');
            $submit.addClass('btnHidden');
        }
    }

    function change_page_number() {
        var curPage = parseInt(self.questions.indexOf(self.questions[current_question_index])) + 1;
        var totalPages = self.questions.length;
        var numbers = curPage + "/" + totalPages;
        $('#pageNumber').text(numbers);
    }

    // Render the first question
    var current_question_index = 0;
    change_question();
    change_page_number();
    // Add listener for the previous question button
    $prev.on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        if(current_question_index > 0) {
            current_question_index--;
            userScore.pop();
            change_question('prev');
            change_page_number();
        }
    });
    // Add listener for the next question button
    $next.on('click', function() {
        var body = $(document).height();

        //console.log(body);
        if(current_question_index < self.questions.length - 1 && current_question_index !== undefined) {
            current_question_index++;
            userScore.push(pushScore($('#question input[name=choices]:checked')));
            console.log(userScore);

            moveOut();

            change_page_number();
            // $('#body').height(body);
        }
    });

    function pushScore(vals) {
        var curScore, checkScore = [], $this = vals;
        if($this.attr('type') == 'radio') {
            curScore = $this.val();
            // console.log(curScore);
            return curScore;
        } else {
            for(var i = 0; i < $this.length; i++) {
                if($this[i].value == "choices-other") {
                    break;
                }
                if($this[i].value == "choices-blank") {
                    break;
                }
                checkScore.push($this[i].value);
            }
            console.log(checkScore);
            return curScore = checkScore;
        }

    }

    // function submitMkto() {
    //     MktoForms2.whenReady(function(form) {
    //         form.submit();
    //     });
    // }

    // Add listener for the submit button
    $submit.on('click', function() {
        userScore.push(pushScore($('#question input[name=choices]:checked')));
        // console.log(userScore);
        var disp = resultsDisplay(occurrence(userScore));
        $('#quiz-results').prepend(disp);
        // submitMkto();
        $('body').toggleClass('bodyResults');
        $('.questionsHold').toggleClass('btnHidden');
        $('.resultsFooter').toggleClass('btnHidden');
        $('#quiz-results').css('opacity', 0).slideDown(1000).animate({ opacity: 1}, { queue: false, duration: 2000});
        $('#quiz button').slideUp();
    });

    // Add a listener on the questions container to listen for user select changes
    // This is for determining whether we can submit answers of not.
    question_container.bind('user-select-change', function() {
        var all_questions_answered = true;
        for(var i = 0; i < self.questions.length; i++) {
            if(self.questions[i].user_choice_index === null) {
                all_questions_answered = false;
                break;
            }
            //console.log(self);
        }

        if(all_questions_answered && current_question_index == self.questions.length - 1) {
            //console.log('the end');
            $next.addClass('btnHidden');
            $submit.prop('enable')
            $submit.removeClass('btnHidden');
        } else {
            //console.log('where am I');
            $next.removeClass('btnHidden');
            $submit.prop('disabled');
            $submit.addClass('btnHidden');
        }
    });

    function moveOut() {
        var $container = $('#question').children();
        var setback = ($container.length) * 30;
        $container.each(function(index, element) {
            setTimeout(function() {
                $(element).removeClass(function (index, css) {
                    return (css.match (/\bmoveIn-\S+/g) || []).join(' ');
                });
                $(element).addClass('moveOut');
            }, ( index * 30 ));
        });

        setTimeout(function() {
            $('.buttonHolder').removeClass(function (index, css) {
                    return (css.match (/\bmoveIn-\S+/g) || []).join(' ');
                }).addClass('moveOut');
        }, (setback));
        setTimeout(function() {
            change_question();
        }, (setback + 445));
    }

    function moveIn() {
        var $container = $('#question').children();
        var timer = ($container.length) / 2 * 30;
        var setback = ($container.length + 1) / 2;

        $('#question h3').addClass('moveIn-header');
        setTimeout(function() {
            $('.buttonHolder').removeClass('moveOut');
            $('.buttonHolder button, .buttonHolder a').css({ opacity : 0.8 });
            $('.buttonHolder').addClass('moveIn-q' + setback);
        }, timer);
    }
}// end Quiz.prototype.render

var ScoreKeeper = function(title, score, answer) {
    this.title  = title,
    this.score  = score,
    this.answer = answer;
}
var userScore = [];
var occurrence = function(score) {
    var countedSpecs = function(choices) {
        var specNum = 0
        for(i = 0; i < choices.length; i++) {
            if(choices[i] == 'choices-spec') {
                specNum++;
            }
        }
        return specNum;
    }

    var result, item = [], tempCount = 0, suggestion, flat, level, autoRec;
    console.log(score);
    if(score[2].includes('choices-archpro') || score[2].includes('choices-devops') ) {
        level = 'pro';
    }
    if(score[2].includes('choices-spec') && score[2].includes('choices-archpro')
        || score[2].includes('choices-spec') && score[2].includes('choices-devops')
        || score[2].includes('choices-proSpec1') && level == 'pro'
        || score[2].includes('choices-proSpec2') && level == 'pro' ) {
        autoRec = 'specs';
    }
    if(countedSpecs(score[2]) == 3) {
        if(score[2].includes('choices-proSpec1') && score[2].includes('choices-proSpec2')) {
            autoRec = 'all';
        } else if(score[2].includes('choices-proSpec1')) {
            autoRec = 'choices-proSpec2';
        } else if(score[2].includes('choices-proSpec2')) {
            autoRec = 'choices-proSpec1';
        } else if(level == null) {
            autoRec = 'all';
        }
    }

    // if(score[2].includes('choices-spec') && score[2].includes('choices-proSpec1') && score[2].includes('choices-proSpec2')) {
    //     // if(countedSpecs(score[2]) == 5) {
    //         autoRec = 'all';
    //     // }
    // }
    // if(score[2].includes('choices-spec') && score[2].length >= 3 && level == null) {
    //     console.log(countedSpecs(score[2]));
    //     if(countedSpecs(score[2]) == 3) {
    //         autoRec = 'all';
    //     }
    // }
    // if(score[2].includes('choices-spec', 'choices-spec', 'choices-spec', 'choices-spec', 'choices-spec')) {
    //     autoRec = 'all';
    // }



    flat = [].concat.apply([], score);
    // console.log(flat);
    result = flat.reduce(function(prev, next) {
        prev[next] = (prev[next] + 1) || 1;
        return prev;
    },{});
    console.log("level: " + level);
    console.log("autoRec: " + autoRec);
    console.log(result);
    for(var k in result) {
        console.log('k: ' + k);
        if(k == 'choices-all') {
            // console.log('inside the if');
            item.pop();
            item.unshift(k);
            // console.log('item is: ' + item);
            break;
        }
        if(k == 'choices-devops' && item == 'choices-dev') {
            // console.log('inside DevOps');
            item.pop();
            item.unshift(k);
            // console.log('new k item is: ' + item);
        }
        if(result[k] > tempCount) {
            item.pop();
            tempCount = result[k];
            item.unshift(k);
        }
        console.log('item: ' + item);
    }
    suggestion = item[0];
    if(suggestion == 'choices-archassoc' && level == 'pro') {
        suggestion = 'choices-archpro';
    } else if(suggestion == 'choices-devops' || suggestion == 'choices-sysops' && level == 'pro') {
        suggestion = 'choices-devops';
    }
    if(autoRec == 'specs') {
        suggestion = 'choices-spec';
    }
    if(autoRec == 'all') {
        suggestion = 'choices-all';
    }
    if(autoRec == 'choices-proSpec1') {
        suggestion = 'choices-archpro';
    }
    if(autoRec == 'choices-proSpec2') {
        suggestion = 'choices-devops';
    }
    console.log(suggestion);
    return suggestion;
};

var resultsDisplay = function(choice) {
    var classInfo = all_results, display = "", mainClass = "", otherClasses = "";
    // console.log(classInfo);
    // console.log('choice is: ' + choice);
    if(choice == 'choices-none') {
        choice = 'choices-cloud';
    }
    if(choice == 'choices-arch') {
        choice = 'choices-archassoc';
    }
    display += "<div class='resultsHeader'><span class='shapeHolderLeftResults shapeResults'></span>";
    mainClass += "<div class='innerResults'>";
    mainClass += "<div class='innerHolder'>";
    if(choice == "choices-spec") {
        mainClass += "<h2>AWS 전문 분야 자격증</h2>";
        mainClass += "<p>전문분야 자격증은 특정 기술 분야의 고급 기술을 검증합니다.</p>";
        // mainClass += "<h2>AWS Specialty Certifications</h2>";
        // mainClass += "<p>These certifications validate your advanced skills in specific technical areas. They are for individuals who have achieved AWS Certified Cloud Practitioner or have an active Associate-level certification.</p>";
    } else if(choice == "choices-all") {
        mainClass += "<h2>AWS 자격증 여정 계속하기</h2>";
        mainClass += "<p>고급 AWS 자격증을 통해 AWS 기술을 검증 했다면, 이제 기술 지식을 계속 확장해 나갈 때입니다. 새로운 전문 분야 자격증을 눈여겨 살펴보시고, 미리 알림을 확인하여 24개월마다 자격증을 갱신하시기 바랍니다.</p>";
    } else {
        mainClass += "<h2>귀하의 자격증 경로</h2>";
        mainClass += "<p>귀하의 답변에 따라 다음 자격증을 추천드립니다.</p>";
    }
    mainClass += "<span class='shapeHolderRightResults shapeResults'></span></div></div></div>";
    for(var i = 0; i < classInfo.length; i++) {
        if(choice == 'choices-all') {
            mainClass += "<div class='mainClass thanksResults'><div class='innerResults'>";
            break;
        }
        if(choice == 'choices-spec') {
            mainClass += "<div class='mainClass specResults'><div class='innerResults'>";
            mainClass += "<article>"
            mainClass += "<img src='./img/icons/aws-certificate-icon-" + classInfo[6].type + ".svg' />";
            mainClass += "<div class='mainText'>"
            mainClass += "<h3>" + classInfo[6].text.headline + "</h3>";
            mainClass += "<p>" + classInfo[6].text.long + "</p>";
            mainClass += "</article>";
            mainClass += "<article>"
            mainClass += "<img src='./img/icons/aws-certificate-icon-" + classInfo[7].type + ".svg' />";
            mainClass += "<div class='mainText'>"
            mainClass += "<h3>" + classInfo[7].text.headline + "</h3>";
            mainClass += "<p>" + classInfo[7].text.long + "</p>";
            mainClass += "</article>";
            mainClass += "<article>"
            mainClass += "<img src='./img/icons/aws-certificate-icon-" + classInfo[8].type + ".svg' />";
            mainClass += "<div class='mainText'>"
            mainClass += "<h3>" + classInfo[8].text.headline + "</h3>";
            mainClass += "<p>" + classInfo[8].text.long + "</p>";
            mainClass += "</article>";
            mainClass += "<p class='specLearnMore'>계속해서 출시되는 새로운 전문 분야 자격증을 눈여겨 살펴보시기 바랍니다.</p>";
            mainClass += "<a class='ctaButton' href='" + classInfo[6].text.link + "' target='_blank'>자세히 알아보기</a>";
            mainClass += "</div>";
            break;
        }
        else if(choice == classInfo[i].type) {
            mainClass += "<div class='mainClass'><div class='innerResults'>";
            mainClass += "<img src='./img/icons/aws-certificate-icon-" + classInfo[i].type + ".svg' />";
            mainClass += "<div class='mainText'>"
            mainClass += "<h3>" + classInfo[i].text.headline + "</h3>";
            mainClass += "<p>" + classInfo[i].text.long + "</p>";
            mainClass += "<a class='ctaButton' href='" + classInfo[i].text.link + "' target='_blank'>자세히 알아보기</a>";
            mainClass += "</div>";
        }
    }
     mainClass += "</div></div>";
    display += mainClass;
    display += "<div class='otherClassesContainer'><p>AWS에서는 차등화된 전문 지식 등급으로 구성된 네 가지 역할 기반 자격증 경로와 전문 분야 자격증을 제공합니다. <a href='https://aws.amazon.com/certification/'>모든 AWS 자격증 옵션 보기</a></p></div>";
    return display;
};

// object for a Question. Constructor
var Question = function(question_string, choices, type) {
    this.question_string   = question_string;
    this.choices           = [];
    this.user_choice_index = null; //Index of the user's choice selection
    this.type              = type;

    // TODO - fill choices[] with choices and do something with it
    var number_of_choices = choices;
    for(var key in number_of_choices) {
        var obj = number_of_choices[key];
        for(var prop in obj) {
            var quest = new ScoreKeeper(key, prop, obj[prop]);
            this.choices.push(quest);
        }
    }
}// end Question

var makeRadio = function(curRadio, appendTo, index) {
    var choice_radio_button = $('<input>')
        .attr('id', 'choices-' + curRadio.choices[index].title)
        .attr('type', 'radio')
        .attr('name', 'choices')
        .attr('data-answer', this.choices[i].answer)
        .attr('value', 'choices-' + curRadio.choices[index].score)
        .attr('checked', index === (curRadio.user_choice_index - 1))
        .appendTo(appendTo);
}

var makeLabel = function(curRadio, appendTo, index, size) {
    var choice_label = $('<label>')
        .text(curRadio.choices[index].answer)
        .attr('for', 'choices-' + curRadio.choices[index].title)
        .attr('class', 'moveIn-' + curRadio.choices[index].title + ' ' + size)
        .appendTo(appendTo);
}

// function that will render the questions inside the container
Question.prototype.render = function(container, direction) {
    var self = this;
    console.log(self);
    var dir = direction;
    // Fill out the question label
    var question_string_h3;
    var question_string_hint;
    if(container.children('h3').length === 0) {
        question_string_h3 = $('<h3>').appendTo(container);
        // console.log('inside the if');
    } else {
        question_string_h3 = container.children('h3').first();
        // console.log('inside the else');
    }
    question_string_h3.text(this.question_string);
    question_string_hint = $('<span class="hintText">').appendTo(question_string_h3);
    if(this.type == "checkbox") {
        question_string_hint.text('(해당 사항 모두 선택)');
    } else if(this.type == "radio") {
        question_string_hint.text('(Choose one)');
    }
    // Clear any radio buttons and create new ones
    if(container.children('input[type=radio]').length > 0) {
        container.children('input[type=radio]').each(function() {
            var radio_button_id = $(this).attr('id');
            $(this).remove();
            container.children('label[for=' + radio_button_id + ']').remove();
        });
    }
    if(container.children('input[type=checkbox]').length > 0 ) {
        container.children('input[type=checkbox]').each(function() {
            var checkbox_id = $(this).attr('id');
            $(this).remove();
            container.children('label[for=' + checkbox_id + ']').remove();
        });
    }
    // console.log(this.choices.length);
    for(var i = 0; i < this.choices.length; i++) {
        //console.log("i: " + i);
        if(this.type == "checkbox") {
            // Create the checkbox
            var choice_radio_button = $('<input>')
                .attr('id', 'choices-' + this.choices[i].title)
                .attr('type', 'checkbox')
                .attr('name', 'choices')
                .attr('data-answer', this.choices[i].answer)
                .attr('value', 'choices-' + this.choices[i].score);
                //.appendTo(container);
            if(this.user_choice_index !== null) {
                // console.log("i : " + i);
                // console.log("userchoiceindex: " + this.user_choice_index);
                choice_radio_button.attr('checked', this.user_choice_index.includes(i + 1))
                    .appendTo(container);
            } else {
                choice_radio_button.appendTo(container);
            }
            if(this.choices[i].score == 'all') {
                choice_radio_button.addClass('preventMore');
            }
        // } else if(this.type == "radio" && this.choices.length > 9) {
        //         // Create the radio button
        //         makeRadio(this, container, i);
        } else {
            // Create the radio button
            makeRadio(this, container, i);
        }
        // Create the label
        if(dir == 'prev') {
            if(this.type == "checkbox" && this.choices.length > 9) {
                var choice_label = $('<label>')
                    .text(this.choices[i].answer)
                    .attr('for', 'choices-' + this.choices[i].title)
                    .attr('class', 'moveIn-place halfSize')
                    .appendTo(container);
            } else {
                var choice_label = $('<label>')
                    .text(this.choices[i].answer)
                    .attr('for', 'choices-' + this.choices[i].title)
                    .attr('class', 'moveIn-place')
                    .appendTo(container);
            }

        } else if(this.type == "checkbox" && this.choices.length > 9) {
                makeLabel(this, container, i, 'halfSize');
                // makeLabel(this, container, i, 'fullSize');
        } else {
            makeLabel(this, container, i, 'fullSize');
        }
    } // end for loop

    var checkboxAnswerIndex = [];
    // Add a listener for the radio button to change which on the user has clicked on
    $('input[name=choices]').change(function(index) {
        //var selected_radio_button_value = $('input[name=choices]:checked').val();
        // var answerIndex = $('input[name=choices]:checked').attr('id');
        var answerIndex = $(this).attr('id');
        // console.log(checkboxAnswerIndex);
        if($(this).attr('type') == 'checkbox') {
            // console.log("answerIndex : " + answerIndex);
            // console.log($(this).prop('checked'));
            // console.log($(this));
            // console.log($(this)[0].value);
            if($(this)[0].value == 'choices-all' || $(this)[0].value == 'choices-none' || $(this)[0].value == 'choices-blank') {
                // console.log(this.id);
                var $ignore = $("label[for='" + this.id + "']");
                $('input[name=choices] + label').not($ignore).toggleClass('preventClick');
                $('input[name=choices]').not(this).prop('checked', false);
            }
            if(checkboxAnswerIndex.includes(parseInt(answerIndex.substring(9)))) {
                var i = checkboxAnswerIndex.indexOf(parseInt(answerIndex.substring(9)));
                if(i != -1) {
                    checkboxAnswerIndex.splice(i, 1);
                }
                // checkboxAnswerIndex = checkboxAnswerIndex.filter(function(item) {
                //     return item !== answerIndex;
                // });
            } else {
                checkboxAnswerIndex.push(parseInt(answerIndex.substr(9)));
            }
            //console.log('checkboxindes: ' + checkboxAnswerIndex);
        }
        // console.log(checkboxAnswerIndex);
        // console.log($(this).attr('id'));
        // console.log(answerIndex);
        if($('#next-question-button').prop('disabled')) {
            $('#next-question-button').prop('disabled', false);
        }
        // Change the user choice index
        if($(this).attr('type') == 'checkbox') {
            self.user_choice_index = checkboxAnswerIndex;
        } else {
            self.user_choice_index = parseInt(answerIndex.substr(answerIndex.length - 1, 1));
        }


        // Trigger a user-select-change
        container.trigger('user-select-change');
    });
}// end Question.prototype.render


$(document).ready(function() {
    // Create an instance of the Quiz object
    var quiz = new Quiz('Modality');
    // Create the Question objects from all_questions
    for(var i = 0; i < all_questions.length; i++) {
        var question = new Question(all_questions[i].question_string, all_questions[i].choices, all_questions[i].type);
        // Add the question to the instance of the Quiz object
        quiz.add_question(question);
    }

    // Render the quiz
    var quiz_container = $('.quizContainer');
    quiz.render(quiz_container);

    $('.pageStart').on('click', 'a', function() {
        $('.pageStart').addClass('noHeight');
        var $container = $('.pageStart').children();
        //$('#pageNumber').fadeToggle();
        $container.each(function(index, element) {
            setTimeout(function() {
                $(element).addClass('moveOut');
            }, 100 + ( index * 100 ));
        });

        $('.quizContainer').toggleClass('btnHidden');
        $('.questionsHold').addClass('show');
    });
});


})(jQuery);
























