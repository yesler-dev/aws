(function($) {

// new categories
// hands
// class
// digital
// exam

var all_results = [{
    type: "choices-class",
    text: {
        headline: "강의실 교육",
        link: "https://aws.amazon.com/training/course-descriptions/",
        desc: "기술 전문성을 갖춘 공인 강사가 진행하는 오프라인, 가상 및 프라이빗 온사이트 교육",
        long: "물리적 환경에서나, 가상 환경에서나, 강의식 수업은 기술 역량을 강화하고자 하는 분들을 위해 좀 더 심층적인 내용을 제공합니다. 강의식 수업은 해당 분야의 전문가가 진행하는 프레젠테이션, 실습 및 그룹 토론의 조합으로 이루어집니다."
    }
}, {
    type: "choices-digital",
    text: {
        headline: "디지털 교육",
        link: "https://aws.amazon.com/training/course-descriptions/",
        desc: "무료 온디맨드 온라인 과정을 통해 자신의 속도에 맞춰 심화 교육을 받을 수 있습니다.",
        long: "약간의 시간과 노력으로 온디맨드 온라인 교육을 통해 실용적인 클라우드 지식을 향상할 수 있습니다. 해당 온라인 교육 과정들은 1시간에서 1일까지 소요 시간이 다양하며, 특정 주제에 대한 이해를 넓히는 데 도움이 됩니다."
    }
}, {
    type: "choices-hands",
    text: {
        headline: "핸즈온 경험",
        link: "https://aws.amazon.com/training/course-descriptions/cloud-practitioner-essentials/",
        desc: "교육의 기반으로, 실제 환경에서 3~6개월 동안 클라우드를 직접 체험해보는 것이 좋습니다.",
        long: "가장 좋은 유형의 교육은 팀 내에서 이루어집니다. 팀에서는 실제 클라우드 환경에서 개발하고 운영하는 데 필요한 실질적인 지식을 얻을 수 있습니다. 3~6개월 동안 직접 체험해 보면 실무 지식의 기초를 쌓을 수 있습니다."
    }
}, {
    type: "choices-exam",
    text: {
        headline: "자기 주도적 학습",
        link: "https://www.youtube.com/user/AWSwebinars/featured",
        desc: "독립적인 학습을 통해 지식의 틈을 메우고, 자신만의 속도에 맞춰 새로운 주제를 배울 수 있습니다.",
        long: "독립적인 학습을 통해 지식의 틈을 메우고, 자신만의 속도에 맞춰 새로운 주제를 배울 수 있습니다. 특정 기술 주제를 깊이 있게 알아보려는 IT 전문가를 위해 다양한 백서, 블로그 게시물, 동영상, 웹 세미나, 사용 사례 등이 준비되어 있습니다."
    }
}];

var all_questions = [{

    question_string: "클라우드 여정 중 현재 어느 단계에 있습니까? 해당 사항을 모두 선택하십시오.",
    choices: {
        q1: {
            class: "AWS 클라우드에 대한 경험이 있습니다."
        },
        q2: {
            digital: "온프레미스에 대한 경험이 있는 IT 전문가입니다."
        },
        q3: {
            hands: "IT 전문가가 아니며, 클라우드에 대한 경험이 없습니다."
        },
        q4: {
            class: "다른 클라우드 공급자에 대한 경험이 있습니다."
        },
        q5: {
            digital: "IT 전문가는 아니지만 클라우드에 대한 경험이 있습니다."
        },
        q6: {
            digital: "이제 학교를 졸업했으며, 처음으로 취업 시장에 진입했습니다."
        }
    },
    type: "checkbox"

}, {
    question_string: "업무 목표 달성을 위해 교육이 바로 필요한 상황입니까, 아니면 개인의 상황에 따라 유연적으로 교육을 받고 계신가요?",
    choices: {
        q1: {
            digital: "바로 교육이 필요한 상황입니다."
        },
        q2: {
            digital: "일정이 유연한 편입니다."
        }
    },
    type: "radio"
}, {
    question_string: "본인에게 맞는 교육 스타일은 무엇입니까? 해당 사항을 모두 선택하십시오.",
    choices: {
        q1: {
            class: "체계적인 수업에서 가장 잘 배웁니다."
        },
        q2: {
            class: "AWS 클라우드 전문가에게 직접 질문을 하고 싶습니다."
        },
        q3: {
            digital: "새로운 시도를 하고 싶습니다. 시도하고 실패하는 것은 저에게는 큰 문제가 되지 않습니다."
        },
        q4: {
            digital: "선택의 폭이 넓을 때 가장 잘 배우는 자기 주도적 학생입니다."
        },
        q5: {
            hands: "독서를 통해 가장 잘 배웁니다."
        },
        q6: {
            digital: "동영상 및 시각적 프레젠테이션을 선호합니다."
        },
        q7: {
            hands: "실제 클라우드 경험이 필요한 실습형 학습자입니다."
        }
    },
    type: "checkbox"
}, {
    question_string: "혼자 받는 교육을 원하십니까, 아니면 그룹으로 받는 교육을 선호하십니까?",
    choices: {
        q1: {
            class: "소그룹 또는 팀과 함께 교육을 받고 있습니다."
        },
        q2: {
            class: "전체 팀(또는 부서나 대규모 그룹)과 함께 교육을 받고 있습니다."
        },
        q3: {
            digital: "개인의 발전에 중점을 둡니다."
        }
    },
    type: "radio"
}, {
    question_string: "교육에 대한 비용은 누가 부담하시나요?",
    choices: {
        q1: {
            class: "회사에서 교육 비용을 지원해 줍니다."
        },
        q2: {
            digital: "교육을 위해 투자할 비용 여유가 거의 없습니다."
        },
        q3: {
            class: "제 교육비는 모두 제가 직접 부담합니다."
        },
        q4: {
            digital: "제 교육비의 일부를 제가 부담합니다."
        },
        q5: {
            hands: "사전에 승인된 예산은 없지만, 제가 속한 조직이 추가 교육에 대한 관심이 있을 수 있습니다."
        },
        q6: {
            class: "사전에 승인된 교육 예산이 있습니다."
        }
    },
    type: "radio"
}];

var Quiz = function(quiz_name) {
    this.quiz_name = quiz_name;
    this.questions = [];
}

// Function to add a question to the Quiz object
Quiz.prototype.add_question = function(question) {
    // Randomly choose where to add question
    // var index_to_add_question = Math.floor(Math.random() * this.questions.length);

    var index_to_add_question = this.questions.length;
    this.questions.splice(index_to_add_question, 0, question);
}

Quiz.prototype.render = function(container) {
    var self = this;
    var $submit = $('#submit-button'), $prev = $('#prev-question-button'), $next = $('#next-question-button');

    $('#quiz-results').hide();

    var question_container = $('<div>').attr('id', 'question').insertAfter('#quiz-name');
    function change_question(direction) {
        var dir = direction;
        self.questions[current_question_index].render(question_container, dir);
        moveIn();
        $prev.prop('disabled', current_question_index === 0)
        $prev.toggleClass('btnHidden', current_question_index === 0);
        $next.prop('disabled', self.questions[current_question_index].user_choice_index === null);

        // Determine if all questions have been answered
        var all_questions_answered = true;
        for(var i = 0; i < self.questions.length; i++) {
            if(self.questions[i].user_choice_index === null) {
                all_questions_answered = false;
                break;
            }
        }
        if(all_questions_answered && current_question_index == self.questions.length - 1) {
            $next.toggleClass('btnHidden');
            $submit.prop('enable')
            $submit.toggleClass('btnHidden');
        } else {
            $next.removeClass('btnHidden');
            $submit.prop('disabled');
            $submit.addClass('btnHidden');
        }
    }

    function change_page_number() {
        var curPage = parseInt(self.questions.indexOf(self.questions[current_question_index])) + 1;
        var totalPages = self.questions.length;
        var numbers = curPage + "/" + totalPages;
        $('#pageNumber').text(numbers);
    }

    // Render the first question
    var current_question_index = 0;
    change_question();
    change_page_number();
    // Add listener for the previous question button
    $prev.on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        if(current_question_index > 0) {
            current_question_index--;
            userScore.pop();
            change_question('prev');
            change_page_number();
        }
    });
    // Add listener for the next question button
    $next.on('click', function() {
        if(current_question_index < self.questions.length - 1 && current_question_index !== undefined) {
            current_question_index++;
            userScore.push(pushScore($('#question input[name=choices]:checked')));
            // console.log(userScore);
            moveOut();
            change_page_number();
        }
    });

    function pushScore(vals) {
        var curScore, checkScore = [], $this = vals;
        if($this.attr('type') == 'radio') {
            curScore = $this.val();
            //console.log(curScore);
            return curScore;
        } else {
            for(var i = 0; i < $this.length; i++) {
                checkScore.push($this[i].value);
            }
            //console.log(checkScore);
            return curScore = checkScore;
        }

    }

    function submitMkto() {
        MktoForms2.whenReady(function(form) {
            form.submit();
        });
    }

    // Add listener for the submit button
    $submit.on('click', function() {
        userScore.push(pushScore($('#question input[name=choices]:checked')));
        var disp = resultsDisplay(occurrence(userScore));
        $('#quiz-results').prepend(disp);
        // submitMkto();
        $('body').toggleClass('bodyResults');
        $('.questionsHold').toggleClass('btnHidden');
        $('.resultsFooter').toggleClass('btnHidden');
        $('#quiz-results').css('opacity', 0).slideDown(1000).animate({ opacity: 1}, { queue: false, duration: 2000});
        $('#quiz button').slideUp();
    });

    // Add a listener on the questions container to listen for user select changes
    // This is for determining whether we can submit answers of not.
    question_container.bind('user-select-change', function() {
        var all_questions_answered = true;
        for(var i = 0; i < self.questions.length; i++) {
            if(self.questions[i].user_choice_index === null) {
                all_questions_answered = false;
                break;
            }
        }

        if(all_questions_answered && current_question_index == self.questions.length - 1) {
            //console.log('the end');
            $next.addClass('btnHidden');
            $submit.prop('enable')
            $submit.removeClass('btnHidden');
        } else {
            //console.log('where am I');
            $next.removeClass('btnHidden');
            $submit.prop('disabled');
            $submit.addClass('btnHidden');
        }
    });

    function moveOut() {
        var $container = $('#question').children();
        var setback = ($container.length) * 30;
        $container.each(function(index, element) {
            setTimeout(function() {
                $(element).removeClass(function (index, css) {
                    return (css.match (/\bmoveIn-\S+/g) || []).join(' ');
                });
                $(element).addClass('moveOut');
            }, ( index * 30 ));
        });

        setTimeout(function() {
            $('.buttonHolder').removeClass(function (index, css) {
                    return (css.match (/\bmoveIn-\S+/g) || []).join(' ');
                }).addClass('moveOut');
        }, (setback));
        setTimeout(function() {
            change_question();
        }, (setback + 445));
    }

    function moveIn() {
        var $container = $('#question').children();
        var timer = ($container.length) / 2 * 30;
        var setback = ($container.length + 1) / 2;

        $('#question h3').addClass('moveIn-header');
        setTimeout(function() {
            $('.buttonHolder').removeClass('moveOut');
            $('.buttonHolder button, .buttonHolder a').css({ opacity : 1 });
            $('.buttonHolder').addClass('moveIn-q' + setback);
        }, timer);
    }
}// end Quiz.prototype.render

var ScoreKeeper = function(title, score, answer) {
    this.title  = title,
    this.score  = score,
    this.answer = answer
}
var userScore = [];
var occurrence = function(score) {
    var result, item = [], tempCount = 0, suggestion, flat;
    flat = [].concat.apply([], score);

    result = flat.reduce(function(prev, next) {
        prev[next] = (prev[next] + 1) || 1;
        return prev;
    },{});
    for(var k in result) {
        if(result[k] > tempCount) {
            item.pop();
            tempCount = result[k];
            item.unshift(k);
        }
    }
    suggestion = item[0];
    return suggestion;
};

var resultsDisplay = function(choice) {
    var classInfo = all_results, display = "", mainClass = "", otherClasses = "";
    //console.log('choice is: ' + choice);

    for(var i = 0; i < classInfo.length; i++) {
        //console.log(classInfo[i].type);
        if(choice == classInfo[i].type) {
            display += "<h2>귀하만의 고유한 선호와 상황을 바탕으로 다음과 같은 교육 유형을 추천합니다. <span>" + classInfo[i].text.headline + "</span></h2>";
            mainClass += "<div class='mainClass'>";
            mainClass += "<img src='./img/aws-training-icon-" + classInfo[i].type + ".svg' />";
            mainClass += "<div class='mainText'>"
            mainClass += "<h3>" + classInfo[i].text.headline + "</h3>";
            mainClass += "<p>" + classInfo[i].text.long + "</p>";
            mainClass += "<a href='" + classInfo[i].text.link + "' target='_blank'>자세히 알아보기</a>";
            mainClass += "</div>";
            mainClass += "</div>";
        } else {
            otherClasses += "<article class='otherClass'>";
            otherClasses += "<img src='./img/aws-training-icon-" + classInfo[i].type + ".svg' />";
            otherClasses += "<div><h5>" + classInfo[i].text.headline + "</h5><p>" + classInfo[i].text.desc + "</p><a href='" + classInfo[i].text.link + "' target='_blank'>자세히 알아보기</a>";
            otherClasses += "</article>";
        }
    }
    display += mainClass;
    display += "<div class='otherClassesContainer'><h4>다음과 같은 다른 형태의 교육으로 마무리하십시오.</h4>" + otherClasses + "</div>";
    return display;
};

// object for a Question. Constructor
var Question = function(question_string, choices, type) {
    this.question_string   = question_string;
    this.choices           = [];
    this.user_choice_index = null; //Index of the user's choice selection
    this.type              = type;


    // TODO - fill choices[] with choices and do something with it
    var number_of_choices = choices;
    for(var key in number_of_choices) {
        var obj = number_of_choices[key];
        for(var prop in obj) {
            var quest = new ScoreKeeper(key, prop, obj[prop]);
            this.choices.push(quest);
        }
    }
}// end Question

// function that will render the questions inside the container
Question.prototype.render = function(container, direction) {
    var self = this;
    var dir = direction;
    // Fill out the question label
    var question_string_h3;
    if(container.children('h3').length === 0) {
        question_string_h3 = $('<h3>').appendTo(container);
    } else {
        question_string_h3 = container.children('h3').first();
    }
    question_string_h3.text(this.question_string);
    // console.log(question_string_h3);
    // Clear any radio buttons and create new ones
    if(container.children('input[type=radio]').length > 0) {
        container.children('input[type=radio]').each(function() {
            var radio_button_id = $(this).attr('id');
            $(this).remove();
            container.children('label[for=' + radio_button_id + ']').remove();
        });
    }
    if(container.children('input[type=checkbox]').length > 0 ) {
        container.children('input[type=checkbox]').each(function() {
            var checkbox_id = $(this).attr('id');
            $(this).remove();
            container.children('label[for=' + checkbox_id + ']').remove();
        });
    }
    for(var i = 0; i < this.choices.length; i++) {
        //console.log(this.user_choice_index);
        // console.log(this.choices[i].answer);
        if(this.type == "checkbox") {
            // Create the checkbox
            var choice_radio_button = $('<input>')
                .attr('id', 'choices-' + this.choices[i].title)
                .attr('type', 'checkbox')
                .attr('name', 'choices')
                .attr('data-answer', this.choices[i].answer)
                .attr('value', 'choices-' + this.choices[i].score);

                //.appendTo(container);
            if(this.user_choice_index !== null) {
                // console.log("i : " + i);
                // console.log("userchoiceindex: " + this.user_choice_index);
                choice_radio_button.attr('checked', this.user_choice_index.includes(i + 1))
                    .appendTo(container);
            } else {
                choice_radio_button.appendTo(container);
            }
        } else {
            // Create the radio button
            var choice_radio_button = $('<input>')
                .attr('id', 'choices-' + this.choices[i].title)
                .attr('type', 'radio')
                .attr('name', 'choices')
                .attr('data-answer', this.choices[i].answer)
                .attr('value', 'choices-' + this.choices[i].score)
                .attr('checked', i === (this.user_choice_index - 1))
                .appendTo(container);
        }
        // Create the lable
        if(dir == 'prev') {
            var choice_label = $('<label>')
                .text(this.choices[i].answer)
                .attr('for', 'choices-' + this.choices[i].title)
                .attr('class', 'moveIn-place')
                .appendTo(container);
        } else {
            var choice_label = $('<label>')
                .text(this.choices[i].answer)
                .attr('for', 'choices-' + this.choices[i].title)
                .attr('class', 'moveIn-' + this.choices[i].title)
                .appendTo(container);
        }

    }
    var checkboxAnswerIndex = [];
    // Add a listener for the radio button to change which on the user has clicked on
    $('input[name=choices]').change(function(index) {
        //var selected_radio_button_value = $('input[name=choices]:checked').val();
        // var answerIndex = $('input[name=choices]:checked').attr('id');
        var answerIndex = $(this).attr('id');
        if($(this).attr('type') == 'checkbox') {
            //console.log("answerIndex : " + answerIndex);
            // console.log($(this).prop('checked'));
            if(checkboxAnswerIndex.includes(parseInt(answerIndex.substr(answerIndex.length - 1, 1)))) {

                var i = checkboxAnswerIndex.indexOf(parseInt(answerIndex.substr(answerIndex.length - 1, 1)));
                if(i != -1) {
                    checkboxAnswerIndex.splice(i, 1);
                }
                // checkboxAnswerIndex = checkboxAnswerIndex.filter(function(item) {
                //     return item !== answerIndex;
                // });
            } else {
                checkboxAnswerIndex.push(parseInt(answerIndex.substr(answerIndex.length - 1, 1)));
            }
            //console.log('checkboxindes: ' + checkboxAnswerIndex);
        }
        // console.log(checkboxAnswerIndex);
        // console.log($(this).attr('id'));
        // console.log(answerIndex);
        if($('#next-question-button').prop('disabled')) {
            $('#next-question-button').prop('disabled', false);
        }
        // Change the user choice index
        if($(this).attr('type') == 'checkbox') {
            self.user_choice_index = checkboxAnswerIndex;
        } else {
            self.user_choice_index = parseInt(answerIndex.substr(answerIndex.length - 1, 1));
        }


        // Trigger a user-select-change
        container.trigger('user-select-change');
    });
}// end Question.prototype.render


$(document).ready(function() {
    // Create an instance of the Quiz object
    var quiz = new Quiz('Modality');
    // Create the Question objects from all_questions
    for(var i = 0; i < all_questions.length; i++) {
        var question = new Question(all_questions[i].question_string, all_questions[i].choices, all_questions[i].type);
        // Add the question to the instance of the Quiz object
        quiz.add_question(question);
    }

    // Render the quiz
    var quiz_container = $('.quizContainer');
    quiz.render(quiz_container);

    $('.pageStart').on('click', 'a', function() {
        $('.pageStart').addClass('noHeight');
        var $container = $('.pageStart').children();
        //$('#pageNumber').fadeToggle();
        $container.each(function(index, element) {
            setTimeout(function() {
                $(element).addClass('moveOut');
            }, 100 + ( index * 100 ));
        });

        $('.quizContainer').toggleClass('btnHidden');
        $('.questionsHold').addClass('show');
    });
});


})(jQuery);
























