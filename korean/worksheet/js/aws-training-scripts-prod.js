(function($) {

// new categories
// hands
// class
// digital
// exam

var all_results = [{
    type: "choices-class",
    text: {
        headline: "Classroom Training",
        link: "https://aws.amazon.com/training/course-descriptions/",
        desc: "In-person, virtual, and private onsite training led by accredited instructors with deep technical expertise",
        long: "Whether it’s a physical or virtual setting, classes offer more in-depth training for those who want to deepen their technical skills. Classes are a mix of presentations, hands-on labs, and group discussions led by experts in their fields."
    }
}, {
    type: "choices-digital",
    text: {
        headline: "Digital Training",
        link: "https://aws.amazon.com/training/course-descriptions/",
        desc: "Free on-demand digital courses help you deepen your knowledge at your own pace",
        long: "With a little time and initiative, you can enhance your practical cloud knowledge through on-demand, online training. These e-learning courses, which vary in length from one hour to one day, help you broaden your understanding of specific subjects."
    }
}, {
    type: "choices-hands",
    text: {
        headline: "Hands-on Experience",
        link: "https://aws.amazon.com/training/course-descriptions/cloud-practitioner-essentials/",
        desc: "We recommend 3-6 months of hands-on work in a live cloud environment as a foundation of your education",
        long: "The best type of training actually happens on teams, where you gain the practical knowledge you need to develop and operate in real cloud environments. Three to six months of hands-on experience will give you a good baseline of working knowledge."
    }
}, {
    type: "choices-exam",
    text: {
        headline: "Self-guided Study",
        link: "https://www.youtube.com/user/AWSwebinars/featured",
        desc: "Independent learning allows you to fill in gaps in your knowledge and learn new topics at your own pace",
        long: "Independent learning allows you to fill in gaps in your knowledge and learn new topics at your own pace. There’s a wide range of white papers, blog posts, videos, webinars, use cases, and peer resources available for IT professionals who want to dive deep into specific technical topics."
    }
}];

var all_questions = [{

    question_string: "How far along are you in your cloud journey? Choose all that apply.",
    choices: {
        q1: {
            class: "I’m experienced with AWS Cloud"
        },
        q2: {
            digital: "I’m an IT pro with on-premises experience"
        },
        q3: {
            hands: "I’m not an IT pro and have no cloud experience"
        },
        q4: {
            class: "I’m experienced with other cloud providers"
        },
        q5: {
            digital: "I’m not an IT pro, but I have cloud experience"
        },
        q6: {
            digital: "I just finished school and am entering the job market for the first time"
        }
    },
    type: "checkbox"

}, {
    question_string: "Do you need to get trained quickly to meet work goals, or are you on a self-paced professional development journey?",
    choices: {
        q1: {
            digital: "I need to move quickly"
        },
        q2: {
            digital: "My timing is flexible"
        }
    },
    type: "radio"
}, {
    question_string: "We’re not all built the same. How do you like to learn? Choose all that apply.",
    choices: {
        q1: {
            class: "I learn best in structured classes"
        },
        q2: {
            class: "I’d like to ask questions of AWS Cloud experts"
        },
        q3: {
            digital: "I like to experiment; trying and failing isn’t a problem for me"
        },
        q4: {
            digital: "I’m a self-guided student who learns best from a wide range of choices"
        },
        q5: {
            hands: "I learn best by reading"
        },
        q6: {
            digital: "I prefer videos and visual presentations"
        },
        q7: {
            hands: "I’m a hands-on learner who needs actual cloud experience"
        }
    },
    type: "checkbox"
}, {
    question_string: "Is your training a solo endeavor or a group effort?",
    choices: {
        q1: {
            class: "I’m training with a small group or team"
        },
        q2: {
            class: "I’m training with an entire team (or department or large group)"
        },
        q3: {
            digital: "My focus is individual development"
        }
    },
    type: "radio"
}, {
    question_string: "Who’s paying for your training?",
    choices: {
        q1: {
            class: "My work is paying for training"
        },
        q2: {
            digital: "I have little to no budget"
        },
        q3: {
            class: "I’m paying for all my own training"
        },
        q4: {
            digital: "I’m paying for part of my training"
        },
        q5: {
            hands: "I don’t have a preapproved budget, but my group may be interested in further training"
        },
        q6: {
            class: "I have a preapproved budget"
        }
    },
    type: "radio"
}];

var Quiz = function(quiz_name) {
    this.quiz_name = quiz_name;
    this.questions = [];
}

// Function to add a question to the Quiz object
Quiz.prototype.add_question = function(question) {
    // Randomly choose where to add question
    // var index_to_add_question = Math.floor(Math.random() * this.questions.length);

    var index_to_add_question = this.questions.length;
    this.questions.splice(index_to_add_question, 0, question);
}

Quiz.prototype.render = function(container) {
    var self = this;
    var $submit = $('#submit-button'), $prev = $('#prev-question-button'), $next = $('#next-question-button');

    $('#quiz-results').hide();

    var question_container = $('<div>').attr('id', 'question').insertAfter('#quiz-name');
    function change_question(direction) {
        var dir = direction;
        self.questions[current_question_index].render(question_container, dir);
        moveIn();
        $prev.prop('disabled', current_question_index === 0)
        $prev.toggleClass('btnHidden', current_question_index === 0);
        $next.prop('disabled', self.questions[current_question_index].user_choice_index === null);

        // Determine if all questions have been answered
        var all_questions_answered = true;
        for(var i = 0; i < self.questions.length; i++) {
            if(self.questions[i].user_choice_index === null) {
                all_questions_answered = false;
                break;
            }
        }
        if(all_questions_answered && current_question_index == self.questions.length - 1) {
            $next.toggleClass('btnHidden');
            $submit.prop('enable')
            $submit.toggleClass('btnHidden');
        } else {
            $next.removeClass('btnHidden');
            $submit.prop('disabled');
            $submit.addClass('btnHidden');
        }
    }

    function change_page_number() {
        var curPage = parseInt(self.questions.indexOf(self.questions[current_question_index])) + 1;
        var totalPages = self.questions.length;
        var numbers = curPage + "/" + totalPages;
        $('#pageNumber').text(numbers);
    }

    // Render the first question
    var current_question_index = 0;
    change_question();
    change_page_number();
    // Add listener for the previous question button
    $prev.on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        if(current_question_index > 0) {
            current_question_index--;
            userScore.pop();
            change_question('prev');
            change_page_number();
        }
    });
    // Add listener for the next question button
    $next.on('click', function() {
        if(current_question_index < self.questions.length - 1 && current_question_index !== undefined) {
            current_question_index++;
            userScore.push(pushScore($('#question input[name=choices]:checked')));
            // console.log(userScore);
            moveOut();
            change_page_number();
        }
    });

    function pushScore(vals) {
        var curScore, checkScore = [], $this = vals;
        if($this.attr('type') == 'radio') {
            curScore = $this.val();
            //console.log(curScore);
            return curScore;
        } else {
            for(var i = 0; i < $this.length; i++) {
                checkScore.push($this[i].value);
            }
            //console.log(checkScore);
            return curScore = checkScore;
        }

    }

    function submitMkto() {
        MktoForms2.whenReady(function(form) {
            form.submit();
        });
    }

    // Add listener for the submit button
    $submit.on('click', function() {
        userScore.push(pushScore($('#question input[name=choices]:checked')));
        var disp = resultsDisplay(occurrence(userScore));
        $('#quiz-results').prepend(disp);
        submitMkto();
        $('body').toggleClass('bodyResults');
        $('.questionsHold').toggleClass('btnHidden');
        $('.resultsFooter').toggleClass('btnHidden');
        $('#quiz-results').css('opacity', 0).slideDown(1000).animate({ opacity: 1}, { queue: false, duration: 2000});
        $('#quiz button').slideUp();
    });

    // Add a listener on the questions container to listen for user select changes
    // This is for determining whether we can submit answers of not.
    question_container.bind('user-select-change', function() {
        var all_questions_answered = true;
        for(var i = 0; i < self.questions.length; i++) {
            if(self.questions[i].user_choice_index === null) {
                all_questions_answered = false;
                break;
            }
        }

        if(all_questions_answered && current_question_index == self.questions.length - 1) {
            //console.log('the end');
            $next.addClass('btnHidden');
            $submit.prop('enable')
            $submit.removeClass('btnHidden');
        } else {
            //console.log('where am I');
            $next.removeClass('btnHidden');
            $submit.prop('disabled');
            $submit.addClass('btnHidden');
        }
    });

    function moveOut() {
        var $container = $('#question').children();
        var setback = ($container.length) * 30;
        $container.each(function(index, element) {
            setTimeout(function() {
                $(element).removeClass(function (index, css) {
                    return (css.match (/\bmoveIn-\S+/g) || []).join(' ');
                });
                $(element).addClass('moveOut');
            }, ( index * 30 ));
        });

        setTimeout(function() {
            $('.buttonHolder').removeClass(function (index, css) {
                    return (css.match (/\bmoveIn-\S+/g) || []).join(' ');
                }).addClass('moveOut');
        }, (setback));
        setTimeout(function() {
            change_question();
        }, (setback + 445));
    }

    function moveIn() {
        var $container = $('#question').children();
        var timer = ($container.length) / 2 * 30;
        var setback = ($container.length + 1) / 2;

        $('#question h3').addClass('moveIn-header');
        setTimeout(function() {
            $('.buttonHolder').removeClass('moveOut');
            $('.buttonHolder button, .buttonHolder a').css({ opacity : 1 });
            $('.buttonHolder').addClass('moveIn-q' + setback);
        }, timer);
    }
}// end Quiz.prototype.render

var ScoreKeeper = function(title, score, answer) {
    this.title  = title,
    this.score  = score,
    this.answer = answer
}
var userScore = [];
var occurrence = function(score) {
    var result, item = [], tempCount = 0, suggestion, flat;
    flat = [].concat.apply([], score);

    result = flat.reduce(function(prev, next) {
        prev[next] = (prev[next] + 1) || 1;
        return prev;
    },{});
    for(var k in result) {
        if(result[k] > tempCount) {
            item.pop();
            tempCount = result[k];
            item.unshift(k);
        }
    }
    suggestion = item[0];
    return suggestion;
};

var resultsDisplay = function(choice) {
    var classInfo = all_results, display = "", mainClass = "", otherClasses = "";
    //console.log('choice is: ' + choice);

    for(var i = 0; i < classInfo.length; i++) {
        //console.log(classInfo[i].type);
        if(choice == classInfo[i].type) {
            display += "<h2>Based on your unique preferences and factors, we recommend the following types of training <span>" + classInfo[i].text.headline + "</span></h2>";
            mainClass += "<div class='mainClass'>";
            mainClass += "<img src='./img/aws-training-icon-" + classInfo[i].type + ".svg' />";
            mainClass += "<div class='mainText'>"
            mainClass += "<h3>" + classInfo[i].text.headline + "</h3>";
            mainClass += "<p>" + classInfo[i].text.long + "</p>";
            mainClass += "<a href='" + classInfo[i].text.link + "' target='_blank'>Learn more</a>";
            mainClass += "</div>";
            mainClass += "</div>";
        } else {
            otherClasses += "<article class='otherClass'>";
            otherClasses += "<img src='./img/aws-training-icon-" + classInfo[i].type + ".svg' />";
            otherClasses += "<div><h5>" + classInfo[i].text.headline + "</h5><p>" + classInfo[i].text.desc + "</p><a href='" + classInfo[i].text.link + "' target='_blank'>Learn more</a>";
            otherClasses += "</article>";
        }
    }
    display += mainClass;
    display += "<div class='otherClassesContainer'><h4>Round out your education with these other training types:</h4>" + otherClasses + "</div>";
    return display;
};

// object for a Question. Constructor
var Question = function(question_string, choices, type) {
    this.question_string   = question_string;
    this.choices           = [];
    this.user_choice_index = null; //Index of the user's choice selection
    this.type              = type;


    // TODO - fill choices[] with choices and do something with it
    var number_of_choices = choices;
    for(var key in number_of_choices) {
        var obj = number_of_choices[key];
        for(var prop in obj) {
            var quest = new ScoreKeeper(key, prop, obj[prop]);
            this.choices.push(quest);
        }
    }
}// end Question

// function that will render the questions inside the container
Question.prototype.render = function(container, direction) {
    var self = this;
    var dir = direction;
    // Fill out the question label
    var question_string_h3;
    if(container.children('h3').length === 0) {
        question_string_h3 = $('<h3>').appendTo(container);
    } else {
        question_string_h3 = container.children('h3').first();
    }
    question_string_h3.text(this.question_string);
    // console.log(question_string_h3);
    // Clear any radio buttons and create new ones
    if(container.children('input[type=radio]').length > 0) {
        container.children('input[type=radio]').each(function() {
            var radio_button_id = $(this).attr('id');
            $(this).remove();
            container.children('label[for=' + radio_button_id + ']').remove();
        });
    }
    if(container.children('input[type=checkbox]').length > 0 ) {
        container.children('input[type=checkbox]').each(function() {
            var checkbox_id = $(this).attr('id');
            $(this).remove();
            container.children('label[for=' + checkbox_id + ']').remove();
        });
    }
    for(var i = 0; i < this.choices.length; i++) {
        //console.log(this.user_choice_index);
        // console.log(this.choices[i].answer);
        if(this.type == "checkbox") {
            // Create the checkbox
            var choice_radio_button = $('<input>')
                .attr('id', 'choices-' + this.choices[i].title)
                .attr('type', 'checkbox')
                .attr('name', 'choices')
                .attr('data-answer', this.choices[i].answer)
                .attr('value', 'choices-' + this.choices[i].score);

                //.appendTo(container);
            if(this.user_choice_index !== null) {
                // console.log("i : " + i);
                // console.log("userchoiceindex: " + this.user_choice_index);
                choice_radio_button.attr('checked', this.user_choice_index.includes(i + 1))
                    .appendTo(container);
            } else {
                choice_radio_button.appendTo(container);
            }
        } else {
            // Create the radio button
            var choice_radio_button = $('<input>')
                .attr('id', 'choices-' + this.choices[i].title)
                .attr('type', 'radio')
                .attr('name', 'choices')
                .attr('data-answer', this.choices[i].answer)
                .attr('value', 'choices-' + this.choices[i].score)
                .attr('checked', i === (this.user_choice_index - 1))
                .appendTo(container);
        }
        // Create the lable
        if(dir == 'prev') {
            var choice_label = $('<label>')
                .text(this.choices[i].answer)
                .attr('for', 'choices-' + this.choices[i].title)
                .attr('class', 'moveIn-place')
                .appendTo(container);
        } else {
            var choice_label = $('<label>')
                .text(this.choices[i].answer)
                .attr('for', 'choices-' + this.choices[i].title)
                .attr('class', 'moveIn-' + this.choices[i].title)
                .appendTo(container);
        }

    }
    var checkboxAnswerIndex = [];
    // Add a listener for the radio button to change which on the user has clicked on
    $('input[name=choices]').change(function(index) {
        //var selected_radio_button_value = $('input[name=choices]:checked').val();
        // var answerIndex = $('input[name=choices]:checked').attr('id');
        var answerIndex = $(this).attr('id');
        if($(this).attr('type') == 'checkbox') {
            //console.log("answerIndex : " + answerIndex);
            // console.log($(this).prop('checked'));
            if(checkboxAnswerIndex.includes(parseInt(answerIndex.substr(answerIndex.length - 1, 1)))) {

                var i = checkboxAnswerIndex.indexOf(parseInt(answerIndex.substr(answerIndex.length - 1, 1)));
                if(i != -1) {
                    checkboxAnswerIndex.splice(i, 1);
                }
                // checkboxAnswerIndex = checkboxAnswerIndex.filter(function(item) {
                //     return item !== answerIndex;
                // });
            } else {
                checkboxAnswerIndex.push(parseInt(answerIndex.substr(answerIndex.length - 1, 1)));
            }
            //console.log('checkboxindes: ' + checkboxAnswerIndex);
        }
        // console.log(checkboxAnswerIndex);
        // console.log($(this).attr('id'));
        // console.log(answerIndex);
        if($('#next-question-button').prop('disabled')) {
            $('#next-question-button').prop('disabled', false);
        }
        // Change the user choice index
        if($(this).attr('type') == 'checkbox') {
            self.user_choice_index = checkboxAnswerIndex;
        } else {
            self.user_choice_index = parseInt(answerIndex.substr(answerIndex.length - 1, 1));
        }


        // Trigger a user-select-change
        container.trigger('user-select-change');
    });
}// end Question.prototype.render


$(document).ready(function() {
    // Create an instance of the Quiz object
    var quiz = new Quiz('Modality');
    // Create the Question objects from all_questions
    for(var i = 0; i < all_questions.length; i++) {
        var question = new Question(all_questions[i].question_string, all_questions[i].choices, all_questions[i].type);
        // Add the question to the instance of the Quiz object
        quiz.add_question(question);
    }

    // Render the quiz
    var quiz_container = $('.quizContainer');
    quiz.render(quiz_container);

    $('.pageStart').on('click', 'a', function() {
        $('.pageStart').addClass('noHeight');
        var $container = $('.pageStart').children();
        //$('#pageNumber').fadeToggle();
        $container.each(function(index, element) {
            setTimeout(function() {
                $(element).addClass('moveOut');
            }, 100 + ( index * 100 ));
        });

        $('.quizContainer').toggleClass('btnHidden');
        $('.questionsHold').addClass('show');
    });
});


})(jQuery);
























